

// Forms on Cached Data (FOCD)
// common functions for pick lists using tag table
// modified to work on new and legacy focd forms
// uses Tags.js as a data source for pick lists aka mtcc or multi column tag table
// freeform data entry fields do not use mctt are updated on focusout 

// how to configure fields to be editable
// class to use for the pick list feature, aka multi column tag table (mctt) is mcttUpdatable  
// data-formkey="1-doc-Business Advisors"   // legacy only doc is form name, Business Advisors is the group field in Tag Table
// data-formkey="sourcecalss-formname-taggroup"  // modified for v1 focd
// contenteditable="true"
// updatableStd   used in legacy not v1
// data-tbid is set then is pulled e.g. data-tbid="101" based on a id="mdFormAdvL1"



import { openIDBGetStore, updateOneMetadataField, updateOneDocumentField } from '../scripts/tbdatabase';
let storeName = ""
let indexName = ""
const message = $("#msgReporting"); /* green row that disappears after message displayed */

$(document).ready(function () {
    // init the tabs after dm component loads
    $('.tabs').tabs();
    setTimeout(() => {
        message.html("")
    }, 1000);
});



// green display message bar at top
export const displayMessage = (flag, msg) => {

    // console.log("msg: " + msg)
    if (flag) {
        message.removeClass('alert-danger');
        message.addClass('alert-success');
        message.html(msg);
        message.show();
    } else {
        message.removeClass('alert-success');
        message.addClass('alert-danger');
        message.html(msg);
        message.show();
    }
    setTimeout(() => {

        message.html("")
    }, 10000);
}




/* mctt (Pick List) Legacy and V1 
new forms must be added to this list and configured per FOCD  rules mainly the following:
data-formkey="people-mdAdv-Primary Contact"
*/

// V1 new make tag table by clicking on field adds support for multi forms by adding suffix to id
$(document).on('click', '.mcttUpdatablev1', function (ui) {

    // get form name, then we can get the tbid
    var formId = String($(this).attr('data-formkey')).split('-')
    var sourceclass = formId[0]
    var formName = formId[1]
    var tagGroup = formId[2]
    var fieldName = $(this).attr('Id')


    //  console.log("fieldName " + fieldName)

    // this is standard pattern how to get current bar id (data-tbid value is set when form opens)
    if (formName === 'tbform') {
        var mybar = document.getElementById('launchTimebarsForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'mdform') {
        var mybar = document.getElementById('launchMetadatasForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'resform') {
        var mybar = document.getElementById('launchResourcesForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'docform') {
        var mybar = document.getElementById('launchDocumentsForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'tagform') {
        var mybar = document.getElementById('launchTagsForm')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === 'statusForm') {
        var mybar = document.getElementById('statusFormIndicatorTable')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === '2colMdPup') {
        var mybar = document.getElementById('tblTaskMetaDataPopupOffBar')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === 'mdAdv') {
        var mybar = document.getElementById('mdFormAdvL1')
        var tbID = mybar.getAttribute('data-tbid')
    }

    // dont want modal mctt on mdAdv form (Inv Planner main form).
    if (formName === "mdAdv") {

        document.getElementById("mcttContainer").style.visibility = "visible";

        // use this for positioning mctt near point of click
        var leftpx = Number($(this).offset().left)
        var toppx = Number($(this).offset().top)
        console.log("leftpx " + leftpx)
        console.log("toppx " + toppx)

        // if user scrolls down, this will ensure the form pops up in viewable area.
        var lastScrollY = window.pageYOffset;
        var lastScrollX = window.pageXOffset;

        toppx = (toppx + 50) - Number(lastScrollY)
        leftpx = (leftpx) - Number(lastScrollX)
        leftpx = leftpx + "px"
        toppx = toppx + "px"

        //   console.log("lastScrollY " + lastScrollY)
        //  console.log("toppx " + toppx)

        makeMcttMetaDataOrRes(tagGroup, tbID, formName, fieldName, leftpx, toppx, sourceclass)

    } else {

        document.getElementById("mcttContainer").style.visibility = "visible";

        leftpx = leftpx + 100
        leftpx = leftpx + "px"
        toppx = ""
        makeMcttMetaDataOrRes(tagGroup, tbID, formName, fieldName, leftpx, toppx, sourceclass)

        // make the mctt a modal
        var elem = document.querySelector('#mcttContainer');
        var options = {
            dismissible: false
        }
        var instance = M.Modal.init(elem, options);
        instance.open();
    }

});



// step 2 draw or make mctt MultiColumnTagTable Table
function makeMcttMetaDataOrRes(tagGroup, tbID, formName, fieldName, leftpx, toppx, sourceclass) {

    console.log("makeMcttMetaDataOrRes " + tbID)
    // sourceclass = 'resource'
    // draw Datatable header    
    /*     var strVar = "";
        strVar += "    <div id=\"mcttHeader\"><h4>Tag Table <\/h4>";
        strVar += "    <div>";
        strVar += "        <input class=\"waves-effect waves-light btn-small blue darken-4 tbButtonSmall FloatRight\" type=\"button\" id=\"btnCloseMultiColumnTagTable\" value=\"Close\" \/>";
        strVar += "    <\/div>";
        strVar += "    <p>Tap to assign value!<\/p><\/div>";
        strVar += "    <div id=\"mcttTagListDiv\"><\/div>"; */

    let strVar = `
    <div id="mcttHeader">
        <div style="height: 34px;">
            <div class="nav-wrapper black" style="height: 34px;">
                <div title="Double click Name value!" style="font-size: 24px;"
                    class="brand-logo left white-text ml-1">
                    Pick List
                </div>
                <ul id="nav-mobile" class="right">
                    <li></li>
                    <li id="btnCloseMultiColumnTagTable"><a title="Close!" href="#!"><i
                                class="material-icons right">close</i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="mcttTagListDiv" class="left"></div>
`

    var mcttContainer = $('#mcttContainer');
    mcttContainer.empty()
    mcttContainer.append(strVar)
    strVar = ""

    // generate special id that we can split later to get tbid and field id
    var tagFormID = tbID + "-" + fieldName
    // console.log("tagFormID " + tagFormID)

    // draw datatable mctt
    var list = $('#mcttTagListDiv');
    list.empty();
    var $table = $('<table class="display" id="' + tagFormID + '">');
    // $table.append('<thead><tr"><th>Class</th><th>Name</th></tr></thead><tbody>') // uncomment for two columsn in tag table
    $table.append('<thead><tr"><th>Item</th><th>Field</th></tr></thead><tbody>')

    // console.log("$table " + JSON.stringify($table))

    // want to be able toget tag values from the tag table or the res table based on source class
    if (sourceclass == "md" || sourceclass == "metadata") {

        var table = 'tbTags'
        var indexName = 'tbTagGroup'
        var filterValue = tagGroup
        openIDBGetStore(table, 'readonly', function (store) {
            var index = store.index(indexName);
            var range = IDBKeyRange.only(filterValue);
            index.openCursor(range).onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor) {

                    $table.append('<tr><td id="mcttKey-' + tagFormID + '" class="mcttTapName" >' + cursor.value.tbTagName + '</td><td>' + cursor.value.tbTagGroup + '</td></tr>');
                    cursor.continue();

                } else {
                    $table.append('</tbody></table>');
                    list.append($table)

                    makeMcttMetaDataOrResDataTable(tagFormID)
                }
            }
        }); // end on success of db // end on success of db

    } else if (sourceclass == 'people' || sourceclass == 'resource') {
        var table = 'tbResources'
        var indexName = 'tbResResourceType'
        var filterValue = 'Human'

        openIDBGetStore(table, 'readonly', function (store) {
            var index = store.index(indexName);
            var range = IDBKeyRange.only(filterValue);
            index.openCursor(range).onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor) {

                    $table.append('<tr><td id="mcttKey-' + tagFormID + '" class="mcttTapName" >' + cursor.value.tbResName + '</td><td>' + cursor.value.tbResDepartment + '</td></tr>');

                    cursor.continue();

                } else {
                    $table.append('</tbody></table>');
                    list.append($table)

                    makeMcttMetaDataOrResDataTable(tagFormID)
                }
                // can add functions to run here after building objects
            }
        }); // end on success of db // end on success of db
    }
    // position the pick list near place clicked

    $('#mcttContainer').css('left', leftpx)
    $('#mcttContainer').css('top', toppx)

    $('#mcttContainer').draggable({}).resizable();

};


// step 3 mctt make it a datatable
function makeMcttMetaDataOrResDataTable(tagFormID) {

    //console.log("how many times")
    var tagFormID = "#" + tagFormID
    $(tagFormID).DataTable({
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "dom": '<"top"f>rt<"bottom"ilp>',

        /*         "paging": false,
                "ordering": true,
                "order": [
                    [1, "asc"]
                ],
                "info": false,
                "scrollY": "400px",
               "scrollCollapse": true, */
    });
    //"searching": false,
}

/* mctt Update UI and IDB */


// step 4b legacy same as 4a but dblclick to close mctt **** update UI and NOT IDB *****
$(document).on('dblclick', '.mcttTapName', function (e) {

    var val = $(this).attr('id');
    var arrval = val.split('-')
    var fieldName = arrval[2]
    var tagValue = e.target.innerHTML
    var fieldToUpate = "#" + fieldName

    $(fieldToUpate).val(tagValue);

    console.log("fieldToUpate " + fieldToUpate)
    console.log("tagValue " + tagValue)

    M.updateTextFields()
    closeMultiColumnTagTable()
});


// step 4 V1 update mctt tag table fields, update UI and IDB with vaulues fixed to deal with V1
$(document).on('click', '.mcttTapName', function (e) {
    // var tagValueOrig = document.getElementById("tbMDSize").innerText

    var val = $(this).attr('id');
    var arrval = val.split('-')
    var tbID = arrval[1]
    var fieldName = arrval[2]
    var tagValue = e.target.innerHTML

    // strip off the field name suffix so we can updated IDB
    fieldName = fieldName.split('_')
    let fieldNamesuffix = fieldName[1]
    fieldName = fieldName[0]

    console.log("val " + val)
    console.log("fieldName " + fieldName)
    console.log("tagValue " + tagValue)

    // logic to deal with updating md table, timebars table or document table
    if (fieldName.startsWith('tbMD') || fieldName.startsWith('tbmd')) {

        updateOneMetadataField(tbID, fieldName, tagValue)

    } else if (fieldName.startsWith('tbDoc') || fieldName.startsWith('tbdoc')) {

        //  console.log("updateOneDocumentField " + fieldName)

        updateOneDocumentField(tbID, fieldName, tagValue)

    } else {

        updateOneFieldTimebarIDB(tbID, fieldName, tagValue)

    }

    // mctt legacy has no "_" in field name, need logic to deal with it
    if (!fieldNamesuffix) {
        // console.log("fieldNamesuffix with no _: " + fieldNamesuffix)
        var fieldToUpate = "#" + fieldName
    } else {

        var fieldToUpate = "#" + fieldName + "_" + fieldNamesuffix
    }
    // console.log("fieldToUpate " + fieldToUpate)

    document.querySelector(fieldToUpate).innerHTML = tagValue

});


// update non tag table fields idb and form when user moves off editable field
$(document).on('focusout', '.updatableStd', function (e) {
    // get form name, then we can get the tbid
    var formId = String($(this).attr('data-formkey')).split('-')
    var formName = formId[1]
    var val = $(this).attr('id');
    console.log("val:" + val)

    // strip off the field name suffix so we can updated IDB
    let fieldName = val.split('_')
    let fieldNamesuffix = fieldName[1]
    fieldName = fieldName[0]
    let fieldid = "#" + fieldName + "_" + fieldNamesuffix
    // get the text from field
    var theText = $(fieldid).val();

    console.log("theText " + theText)

    UpdatableStd(formName, fieldName, theText)
});


// update non tag table fields idb and form when user moves off editable field
$(document).on('focusout', '.updatableTextArea', function (e) {
    // get form name, then we can get the tbid
    var formId = String($(this).attr('data-formkey')).split('-')
    var formName = formId[1]
    var val = $(this).attr('id');
    //    console.log("val:" + val)

    // strip off the field name suffix so we can updated IDB
    let fieldName = val.split('_')
    let fieldNamesuffix = fieldName[1]
    fieldName = fieldName[0]
    let fieldid = "#" + fieldName + "_" + fieldNamesuffix
    // get the text from field
    // var theText = $(fieldid).html() //.trim();
    let theText = $.trim($(fieldid).val())
    //theText = theText.trim()
    //  console.log("fieldid " + fieldid)
    //  console.log("theText " + theText)

    UpdatableStd(formName, fieldName, theText)


});

export function UpdatableStd(formName, fieldName, theText) {

    // this is standard pattern how to get current bar id (data-tbid value is set when form opens)
    if (formName === 'tbform') {
        var mybar = document.getElementById('launchTimebarsForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'mdform') {
        var mybar = document.getElementById('launchMetadatasForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'resform') {
        var mybar = document.getElementById('launchResourcesForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'docform') {
        var mybar = document.getElementById('launchDocumentsForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'tagform') {
        var mybar = document.getElementById('launchTagsForm')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === 'statusForm') {
        var mybar = document.getElementById('statusFormIndicatorTable')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === '2colMdPup') {
        var mybar = document.getElementById('tblTaskMetaDataPopupOffBar')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === 'mdAdv') {
        var mybar = document.getElementById('mdFormAdvL1')
        var tbID = mybar.getAttribute('data-tbid')
    }

    console.log("UpdatableStd tbID mdAdv " + tbID)

    if (fieldName.startsWith('tbMD') || fieldName.startsWith('tbmd')) {

        updateOneMetadataField(tbID, fieldName, theText)

        //   console.log("update one md field " + theText)

    } else if (fieldName.startsWith('tbDoc') || fieldName.startsWith('tbdoc')) {

        console.log("updateOneDocumentField " + fieldName)

        updateOneDocumentField(tbID, fieldName, theText)

    } else {

        // can remove this, was tb
    }
}




// V1 close modal mctt 
$(document).on('click', '#btnCloseMultiColumnTagTable', function (e) {

    closeMultiColumnTagTable()
});
function closeMultiColumnTagTable() {
    M.updateTextFields()
    var elem = document.querySelector('#mcttContainer');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.getInstance(elem, options);

    if (instance) {

        instance.close();

    } else {

        $("#multiColumnTagTable").remove()
        $("#mcttTagListDiv").remove()
        document.getElementById("mcttContainer").style.visibility = "hidden";
    }

}

