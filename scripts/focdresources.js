// was the todo app from noob coder
// was doc datatableuniversal
// refactored to use tb and idb


/* idb code */
import { displaySuccessMessageDB, handleJSONFileDropforUpload, openIDBGetStore, setTbidBasedOnKeyUniversal, deleteRowFromIDBUniversal, universalJsonDataPopulator, clearObjectStore, createJsonFileSimple } from './tbdatabase';
import { displayMessage } from './focdcommon';
import { tbBreadCrumbHTMLComponent } from './htmlcomponents';


// global variables
let displayDT = "" //$("#displayDtResources");
const storeName = 'tbResources'



// get and display resources in grid from menus
// replace this with pattern in focddocuments.js
$(document).on('click', '.launchTbResources', function (e) {

   e.preventDefault()

   $("#divTbPagesL1").css("visibility", "visible");
   // $("#divTbPagesL1").empty()
   emptyDivsForDynamicPages()
   console.log(" here launchTbResources click")
   let targetDiv = '#divTbPagesL1'
   let dropdiv = 'tbResourcesComponentL2'
   drawTbResources(targetDiv, dropdiv)
});

$('#resTab').on('click', function () {
   //  console.log("clicked Resource")
   getResources()
   setTimeout(() => {
      console.log("page load get Resource")
      initDtResources()

   }, 300);
});

export function drawTbResources(targetDiv, dropdiv) {

   setTimeout(() => {
      // let targetDiv = '#divTbPagesL1'
      dtResourceTableHeaderHTMLComponent(targetDiv)

   }, 50);

   setTimeout(() => {
      //   console.log("page load get Document")
      getResources()
   }, 200);

   setTimeout(() => {
      //   console.log("page load get Document")
      initDtResources()

   }, 300);
   setTimeout(() => {

      handleJSONFileDropforUpload(dropdiv)

   }, 400);
};


// set up the column names for data table
export function dtResourceTableHeaderHTMLComponent(targetDiv) {

   let td = $(targetDiv);
   let dtreshtml = `
               <!-- ------ START Resources core univ data table area -->
               <div id="tbResourcesComponentL2" class="dmDropArea">

                  <div style="margin-top: 5px; font-size: 24px;">People Profiles (Users)

                     <span style="margin-left: 300px; font-size: 12px;">Drop tag files here to bulk upload!</span>

                  </div>
                  <button id="btnLaunchEmptyResourcesForm"
                     class="btn-small waves-effect waves-light grey darken-4 right">New Item
                  </button>
                  <button id="btnBackupResource" style="margin-right: 5px;"
                     class="btn-small waves-effect waves-light grey darken-4 right"> Data
                     Backup
                  </button>

               </div>
               <!-- ------ START Resource data table area -->
               <table id="dataTableResources" class="displayzz" style="width:100%">
                  <thead>
                     <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Cost Code</th>
                        <th>Type</th>
                        <th>Class</th>
                        <th>Delete</th>
                     </tr>
                  </thead>
                  <tbody id="displayDtResources">
                  </tbody>
                  </tfoot>
               </table>
               <!-- -------- END Resource data table area -->
          `
   td.append(dtreshtml)

};




$(document).on('click', '#btnBackupResource', function () {
   createJsonFileSimple(storeName)
});


// appends to dataTableResources
const generateDatatableResources = (Item, ids) => {

   // console.log(" item: " + Item.tbResID)
   return `<tr id="${ids.listItemID}">
        <td><a title="Edit this Resource" id="${ids.editID}"><i class="material-icons tiny">edit</i></a></td>
        <td id="tbResID-${Item.tbResID}"><div>${Item.tbResID}</div></td>
        <td id="tbResName-${Item.tbResID}"><div>${Item.tbResName}</div></td>
        <td id="tbResEmail-${Item.tbResID}"><div>${Item.tbResEmail}</div></td>
        <td id="tbResStatus-${Item.tbResID}"><div>${Item.tbResStatus}</div></td>
        <td id="tbResCostCode-${Item.tbResID}"><div>${Item.tbResCostCode}</div></td>
        <td id="tbResResourceType-${Item.tbResID}"><div>${Item.tbResResourceType}</div></td>
        <td id="tbResResourceClass-${Item.tbResID}"><div>${Item.tbResResourceClass}</div></td>
         <td><button title="Delete this row" type="button" class="btn-flat" id="${ids.deleteID}"><i class="material-icons">delete_forever</i></button></td>
        </tr>`;
}



// new code get all items and display them
const getResources = () => {

   setTimeout(() => {
      returnAllResources(function (data) {

         displayResources(data);
      })
   }, 300);

}

// master very simple function to get all rows in a table store  // later send store name as var
function returnAllResources(callbackResources) {
   openIDBGetStore(storeName, 'readwrite', function (store) {
      var request = store.getAll();
      request.onsuccess = function (evt) {
         var tb = evt.target.result
         //   console.log(tb)
         callbackResources(tb)
      };
   })
};


// univ form materialize javascript
function launchResourcesForm(resourceid) {

   // set data-tbid on form for later actions
   var myform = document.getElementById('launchResourcesForm')
   var myform = myform.setAttribute('data-tbid', resourceid)

   var elem = document.querySelector('#launchResourcesForm');
   var options = {
      dismissible: false
   }
   var instance = M.Modal.init(elem, options);
   instance.open();

   //  console.log("launchEmptyResourcesForm finished")

}

export function resourceFormHTMLComponent(targetDiv) {
   let td = $(targetDiv);
   let tbBreadCrumbHTML = `
    <!-- ---- Start Resource Form move to component later -->
    <div id="launchResourcesForm" class="modal" data-tbid="">
       <div class="modal-content" data-backdrop="static" data-keyboard="false">
          <!-- Left Half -->
          <div class="containerzzz">
             <form class="zzz">
                <div class="row">
                   <!-- start header row -->
                   <div class="row">
                      <div class="input-field col s8">
                         <h4>People Profile (Users) Metadata </h4>
                      </div>
                      <div class="input-field col s2">
                         <input disabled id="tbResID" type="text" class="validate">
                         <label for="tbResID">Resource UID</label>
                      </div>
                      <div class="input-field col s2">
                         <a title="Close without saving!" id="closeEditorResourcesTop"
                            class="modal-close waves-effect waves-blue btn-flat">Close</a>
                         <a href="#!" title="Save to the database"
                            class="btnSaveFormDataResource waves-effect waves-light btn-small blue darken-4">Save</a>
                      </div>
                   </div>
                   <!-- end hdr row -->

                   <!-- start Common section -->

                   <div class="row">
                      <div class="input-field col s4">
                         <input id="tbResName" type="text" class="validate">
                         <label for="tbResName">Resource Name</label>
                      </div>

                      <div class="input-field col s4">
                         <input id="tbResTeam" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Responsible Teams">
                         <label for="tbResTeam">Team</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResSupervisor" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Resource Supervisor">
                         <label for="tbResSupervisor">Supervisor</label>
                      </div>
                   </div>
                   <div class="row">
                   <div class="input-field col s4">
                      <input id="tbResEmail" type="text" class="validate"
                         data-formkey="md-resform-Email">
                      <label for="tbResEmail">Email Address</label>
                   </div>
                   <div class="input-field col s4">
                      <input id="tbResStatus" type="text" class="validate mcttUpdatablev1"
                         data-formkey="md-resform-Resource Status">
                      <label for="tbResStatus">Status</label>
                   </div>
                   <div class="input-field col s4">
                      <input id="tbResPIN" type="text" class="validate"
                         data-formkey="md-resform-NA">
                      <label for="tbResPIN">PWD</label>
                  </div>
                </div>
                   <div class="row">
                      <div class="input-field col s4">
                         <input id="tbResManager" type="text" class="validate mcttUpdatablev1"
                            data-formkey="people-resform-Resource Manager">
                         <label for="tbResManager">Manager</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResDepartment" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Department">
                         <label for="tbResDepartment">Department</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResPasswordHash" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Parttime Fulltime">
                         <label for="tbResPasswordHash">Part or Full Time</label>
                      </div>
                   </div>

                   <div class="row">

                      <div class="input-field col s4">
                         <input id="tbResResourceCalendar" type="text" class="validate"
                            data-formkey="md-resform-Calendar">
                         <label for="tbResResourceCalendar">Work Day</label>
                      </div>

                      <div class="input-field col s4">
                         <input id="tbResPayRate" type="text" class="validate">
                         <label for="tbResPayRate">Pay Rate</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResLabourType" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Labour Type">
                         <label for="tbResLabourType">Labour Type</label>
                      </div>
                   </div>

                   <div class="row">
                      <div class="input-field col s4">
                         <input id="tbResResourceClass" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Resource Class">
                         <label for="tbResResourceClass">Resource Class</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResResourceType" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Resource Type">
                         <label for="tbResResourceType">Resource Type</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResCostCode" type="text" class="validate mcttUpdatablev1"
                            data-formkey="md-resform-Cost Code">
                         <label for="tbResCostCode">Cost Code</label>
                      </div>
                   </div>
                   <div class="row">
                      <div class="input-field col s4">
                         <input id="tbResSortOrder" type="text" class="validate">
                         <label for="tbResSortOrder">Sort Order</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResNameShort" type="text" class="validate" data-length="10">
                         <label for="tbResNameShort">Resource Short Name</label>
                      </div>
                      <div class="input-field col s4">
                         <input id="tbResLastModified" type="text" class="validate">
                         <label for="tbResLastModified">Last Modified</label>
                      </div>
                   </div>


                   <!-- end Common sect -->

                   <!-- Other system info -->
                   <div class="section">
                      <h5>Other System Informaton</h5>

                      <div class="row">
                         <div class="input-field col s4">
                            <input id="tbResCustomerID" type="text" class="validate">
                            <label for="tbResCustomerID">Customer ID</label>
                         </div>

                         <div class="input-field col s4">
                            <input id="tbResAzureID" type="text" class="validate">
                            <label for="tbResAzureID">External ID Other</label>
                         </div>
                         <div class="input-field col s4">
                            <input id="tbResExtSystemResID" type="text" class="validate">
                            <label for="tbResExtSystemResID">Ext System ResID</label>
                         </div>
                      </div>

                      <div class="row">
                         <div class="input-field col s4">
                            <input id="tbResCoordTop" type="text" class="validate">
                            <label for="tbResCoordTop">tbResCoordTop</label>
                         </div>
                         <div class="input-field col s4">
                            <input id="tbResCoordLeft" type="text" class="validate">
                            <label for="tbResCoordLeft">tbResCoordLeft</label>
                         </div>
                         <div class="input-field col s4">
                            <input id="tbResExtSystemResIDzz" type="text" class="validate">
                            <label for="tbResExtSystemResIDzz">Not Used</label>
                         </div>
                      </div>
                   </div> <!-- end other system info section -->


                   <!-- </div> -->
                </div>
             </form>
          </div>

          <div class="modal-footer">
             <a href="#!" title="Save to the database"
                class="btnSaveFormDataResource waves-effect waves-light btn-small blue darken-4">Save</a>
             <a id="btnCreateResource" href="#!" title="Create new Resource from data in this form!"
                class="waves-effect waves-blue btn-flat">Create New</a>
             <a id="closeEditorResources" title="Close without saving!"
                class="modal-close waves-effect waves-blue btn-flat">Close</a>

          </div>
       </div>
    </div>
    <!--  ----END Resource Form  -->
          `
   td.append(tbBreadCrumbHTML)
};


$(document).on('click', '#btnLaunchEmptyResourcesForm', function () {
   launchEmptyResourcesForm()
});

// univ form materialize javascript
function launchEmptyResourcesForm() {


   var elem = document.querySelector('#launchResourcesForm');
   var options = {
      dismissible: false
   }
   var instance = M.Modal.init(elem, options);
   instance.open();



   setTimeout(() => {
      resetResourcesInput()
   }, 300);
   //  console.log("launchEmptyResourcesForm finished")

}

// not used, we only create Resource rows from the app
//$('#btnCreateResource').on('click', function () {
$(document).on('click', '#btnCreateResource', function (e) {
   confirm("Are you sure, this will create a new Resource. Must be unique Resource Name")
   // remove any cached data in the form
   createResource()
});

// not used, we only create Resource rows from the app when creating new timebar
function createResource() {

   // let tbResID = $('#tbResID').val();  // is auto generated by idb
   let tbResCustomerID = $("#tbResCustomerID").val()
   let tbResAzureID = $("#tbResAzureID").val()
   let tbResName = $("#tbResName").val()
   let tbResNameShort = $("#tbResNameShort").val()
   let tbResEmail = $("#tbResEmail").val()
   let tbResStatus = $("#tbResStatus").val()
   let tbResDepartment = $("#tbResDepartment").val()
   let tbResManager = $("#tbResManager").val()
   let tbResPayRate = $("#tbResPayRate").val()
   let tbResCoordTop = $("#tbResCoordTop").val()
   let tbResCoordLeft = $("#tbResCoordLeft").val()
   let tbResTeam = $("#tbResTeam").val()
   let tbResLocation = $("#tbResLocation").val()
   let tbResCostCode = $("#tbResCostCode").val()
   let tbResTeamLeader = $("#tbResTeamLeader").val()
   let tbResSupervisor = $("#tbResSupervisor").val()
   let tbResPasswordHash = $("#tbResPasswordHash").val()
   let tbResResourceType = $("#tbResResourceType").val()
   let tbResResourceClass = $("#tbResResourceClass").val()
   let tbResResourceCalendar = $("#tbResResourceCalendar").val()
   let tbResPIN = $("#tbResPIN").val()
   let tbResExtSystemResID = $("#tbResExtSystemResID").val()
   let tbResSortOrder = $("#tbResSortOrder").val()
   let tbResLastModified = $("#tbResLastModified").val()
   let tbResLabourType = $("#tbResLabourType").val()

   let nameCheck = $("#tbResName").val()
   console.log("Name check " + nameCheck)
   if (nameCheck == '' || nameCheck == ' ') {
      alert("You must enter a Resource Name!")
      return
   }

   /* build up an object */
   var obj = {

      tbResCustomerID: tbResCustomerID,
      tbResAzureID: tbResAzureID,
      tbResName: tbResName,
      tbResNameShort: tbResNameShort,
      tbResEmail: tbResEmail,
      tbResStatus: tbResStatus,
      tbResDepartment: tbResDepartment,
      tbResManager: tbResManager,
      tbResPayRate: tbResPayRate,
      tbResCoordTop: tbResCoordTop,
      tbResCoordLeft: tbResCoordLeft,
      tbResTeam: tbResTeam,
      tbResLocation: tbResLocation,
      tbResCostCode: tbResCostCode,
      tbResTeamLeader: tbResTeamLeader,
      tbResSupervisor: tbResSupervisor,
      tbResPasswordHash: tbResPasswordHash,
      tbResResourceType: tbResResourceType,
      tbResResourceClass: tbResResourceClass,
      tbResResourceCalendar: tbResResourceCalendar,
      tbResPIN: tbResPIN,
      tbResExtSystemResID: tbResExtSystemResID,
      tbResSortOrder: tbResSortOrder,
      tbResLastModified: tbResLastModified,
      tbResLabourType: tbResLabourType
   };

   let newItemId = ""
   /*  add the Resource */
   openIDBGetStore(storeName, 'readwrite', function (store) {

      var request;
      try {
         request = store.add(obj);
      } catch (e) {
         displayFailureMessage("Some Problem While adding");
         throw e;
      }
      request.onsuccess = function (e) {

         // return the id of the newly added item
         newItemId = request.result

         // now fetch the new record, and generate template stuff
         var request2 = store.get(newItemId);
         request2.onsuccess = function (e) {

            var data = e.target.result;

            console.log("data " + JSON.stringify(data))

            let ids = buildIdsResources(data.id);
            displayDT = $("#displayDtResources");
            displayDT.append(generateDatatableResources(data, ids));
            fillResourcesForm(data, ids.resourceID, ids.editID); //not sure why this was here ??, maybe for insterting new into dom
            deleteResource(data, ids.listItemID, ids.deleteID);
            // update tbResID field to match indexedb key id
            let fieldName = "tbResID"
            let newVal = String(data.id)
            let uid = Number(data.id)  // keys are numbers in idb, we find the new record by key and update the tbid
            setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal)

            // console.log("ids " + JSON.stringify(ids))
            displayMessage(true, "createResource Result: " + JSON.stringify(tbResName) + " added!");

            // redraw the grid 
            emptyDivsForDynamicPages()
            console.log(" here launchTbResources click")
            let targetDiv = '#divTbPagesL1'
            let dropdiv = 'tbResourcesComponentL2'
            drawTbResources(targetDiv, dropdiv)

            displaySuccessMessageDB("New item added, grid refreshed!")

         }
      };
      request.onerror = function () {

         displayMessage(true, "May be duplicate resource name. System error: " + this.error);
      };
   });

}

// do not delete rows from here, they get deleted if deleting timebars rows
const deleteResource = (resource, listItemID, deleteID) => {
   // note may have to modify this to suit resourceid as string for now tbResID and built in id must match
   //  console.log("resource.tbResID: " + resource.tbResID + " check!")
   var indexName = "tbResID"
   var resourceid = resource.tbResID
   // console.log("resourceid " + resourceid)

   let deleteBtn = $(`#${deleteID}`);
   deleteBtn.click(() => {

      deleteRowFromIDBUniversal(storeName, indexName, resourceid)

      $(`#${listItemID}`).remove();
      displayMessage(true, "deleteResource Result: " + listItemID + " deleted!");

   });
}

//  get one fill form first, put show details button, then edit button
const fillResourcesForm = (resource, resourceID, editID) => {

   //  console.log("fillResourcesForm - resource.tbResID " + resource.tbResID)  // returns the id without underscore
   //  console.log("fillResourcesForm - resourceID " + resourceID)  // resource_70
   // console.log("fillResourcesForm - editID " + editID)  //edit_70

   let targetDiv = '#divTbPagesL1'
   resourceFormHTMLComponent(targetDiv)

   let editBtn = $(`#${editID}`);
   editBtn.click(() => {

      openIDBGetStore(storeName, 'readonly', function (store) {

         // var request = store.get(resource.tbResID);
         var resourceid = String(resource.tbResID)

         var index = store.index("tbResID");
         var request = index.get(resourceid);

         request.onerror = function (event) {
            alert("there was error getting store id");
         };
         request.onsuccess = function (event) {

            // here we are returning data to the form
            var data = request.result;
            if (!data) {
               alert("res not a valid key, check for string");
               return;
            }
            // populate the form fields

            $("#tbResID").val(data.tbResID);
            $("#tbResCustomerID").val(data.tbResCustomerID);
            $("#tbResAzureID").val(data.tbResAzureID);
            $("#tbResName").val(data.tbResName);
            $("#tbResNameShort").val(data.tbResNameShort);
            $("#tbResEmail").val(data.tbResEmail);
            $("#tbResStatus").val(data.tbResStatus);
            $("#tbResDepartment").val(data.tbResDepartment);
            $("#tbResManager").val(data.tbResManager);
            $("#tbResPayRate").val(data.tbResPayRate);
            $("#tbResCoordTop").val(data.tbResCoordTop);
            $("#tbResCoordLeft").val(data.tbResCoordLeft);
            $("#tbResTeam").val(data.tbResTeam);
            $("#tbResLocation").val(data.tbResLocation);
            $("#tbResCostCode").val(data.tbResCostCode);
            $("#tbResTeamLeader").val(data.tbResTeamLeader);
            $("#tbResSupervisor").val(data.tbResSupervisor);
            $("#tbResPasswordHash").val(data.tbResPasswordHash);
            $("#tbResResourceType").val(data.tbResResourceType);
            $("#tbResResourceClass").val(data.tbResResourceClass);
            $("#tbResResourceCalendar").val(data.tbResResourceCalendar);
            $("#tbResPIN").val(data.tbResPIN);
            $("#tbResExtSystemResID").val(data.tbResExtSystemResID);
            $("#tbResSortOrder").val(data.tbResSortOrder);
            $("#tbResLastModified").val(data.tbResLastModified);
            $("#tbResLabourType").val(data.tbResLabourType);



            //  console.log("data sent to form: " + JSON.stringify(data))
            displayMessage(true, "Form opened, ready for editing!");
            launchResourcesForm(resourceid)

            // activate the labels on the form
            M.updateTextFields()
            // open or resize textarea fields
            //  M.textareaAutoResize($('#zzz'));
            // to make this work, simply add this beside class data-length="111" to text area or input
            $('input#tbResNameShort').characterCounter();

         };
      });
   });


}

$(document).on('click', '.btnSaveFormDataResource', function () {
   saveFormDataResource()
});

// save the edit - only certain fields are editable, not all
const saveFormDataResource = () => {
   // first get form data

   let tbResCustomerID = $("#tbResCustomerID").val()
   let tbResAzureID = $("#tbResAzureID").val()
   let tbResName = $("#tbResName").val()
   let tbResNameShort = $("#tbResNameShort").val()
   let tbResEmail = $("#tbResEmail").val()
   let tbResStatus = $("#tbResStatus").val()
   let tbResDepartment = $("#tbResDepartment").val()
   let tbResManager = $("#tbResManager").val()
   let tbResPayRate = $("#tbResPayRate").val()
   let tbResCoordTop = $("#tbResCoordTop").val()
   let tbResCoordLeft = $("#tbResCoordLeft").val()
   let tbResTeam = $("#tbResTeam").val()
   let tbResLocation = $("#tbResLocation").val()
   let tbResCostCode = $("#tbResCostCode").val()
   let tbResTeamLeader = $("#tbResTeamLeader").val()
   let tbResSupervisor = $("#tbResSupervisor").val()
   let tbResPasswordHash = $("#tbResPasswordHash").val()
   let tbResResourceType = $("#tbResResourceType").val()
   let tbResResourceClass = $("#tbResResourceClass").val()
   let tbResResourceCalendar = $("#tbResResourceCalendar").val()
   let tbResPIN = $("#tbResPIN").val()
   let tbResExtSystemResID = $("#tbResExtSystemResID").val()
   let tbResSortOrder = $("#tbResSortOrder").val()
   let tbResLastModified = $("#tbResLastModified").val()
   let tbResLabourType = $("#tbResLabourType").val()

   openIDBGetStore(storeName, 'readwrite', function (store) {
      var resourceid = $("#tbResID").val()
      console.log("tbResID: " + resourceid)
      resourceid = String(resourceid)

      var index = store.index("tbResID");
      var request = index.get(resourceid);
      request.onerror = function (event) {
         // Handle errors!
         alert("there was error getting store id")
      };
      request.onsuccess = function (event) {
         // Get the exist value that we want to update
         var data = request.result;
         //    console.log("request.result " + JSON.stringify(data))

         try {
            // apply editable data fields on form to the data object
            data.tbResCustomerID = tbResCustomerID
            data.tbResAzureID = tbResAzureID
            data.tbResName = tbResName
            data.tbResNameShort = tbResNameShort
            data.tbResEmail = tbResEmail
            data.tbResStatus = tbResStatus
            data.tbResDepartment = tbResDepartment
            data.tbResManager = tbResManager
            data.tbResPayRate = tbResPayRate
            data.tbResCoordTop = tbResCoordTop
            data.tbResCoordLeft = tbResCoordLeft
            data.tbResTeam = tbResTeam
            data.tbResLocation = tbResLocation
            data.tbResCostCode = tbResCostCode
            data.tbResTeamLeader = tbResTeamLeader
            data.tbResSupervisor = tbResSupervisor
            data.tbResPasswordHash = tbResPasswordHash
            data.tbResResourceType = tbResResourceType
            data.tbResResourceClass = tbResResourceClass
            data.tbResResourceCalendar = tbResResourceCalendar
            data.tbResPIN = tbResPIN
            data.tbResExtSystemResID = tbResExtSystemResID
            data.tbResSortOrder = tbResSortOrder
            data.tbResLastModified = tbResLastModified
            data.tbResLabourType = tbResLabourType

         } catch (e) {
            console.log("saveFormDataResource - fail")

            alert("If you are creating new item, use Create New button below!")

            throw e;
         }
         // Put this updated object back into the database.
         var requestUpdate = store.put(data);
         requestUpdate.onerror = function (event) {
            // Do something with the error
            alert("there was error updating progress in idb")
         };
         requestUpdate.onsuccess = function (event) {

            console.log("Success - Form is updated!")

            // $("#tbid").text(tbResID);  // universal input on all forms used on focdcommon
            $(`#tbResCustomerID-${resourceid}`).text(tbResCustomerID);
            $(`#tbResAzureID-${resourceid}`).text(tbResAzureID);
            $(`#tbResName-${resourceid}`).text(tbResName);
            $(`#tbResNameShort-${resourceid}`).text(tbResNameShort);
            $(`#tbResEmail-${resourceid}`).text(tbResEmail);
            $(`#tbResStatus-${resourceid}`).text(tbResStatus);
            $(`#tbResDepartment-${resourceid}`).text(tbResDepartment);
            $(`#tbResManager-${resourceid}`).text(tbResManager);
            $(`#tbResPayRate-${resourceid}`).text(tbResPayRate);
            $(`#tbResCoordTop-${resourceid}`).text(tbResCoordTop);
            $(`#tbResCoordLeft-${resourceid}`).text(tbResCoordLeft);
            $(`#tbResTeam-${resourceid}`).text(tbResTeam);
            $(`#tbResLocation-${resourceid}`).text(tbResLocation);
            $(`#tbResCostCode-${resourceid}`).text(tbResCostCode);
            $(`#tbResTeamLeader-${resourceid}`).text(tbResTeamLeader);
            $(`#tbResSupervisor-${resourceid}`).text(tbResSupervisor);
            $(`#tbResPasswordHash-${resourceid}`).text(tbResPasswordHash);
            $(`#tbResResourceType-${resourceid}`).text(tbResResourceType);
            $(`#tbResResourceClass-${resourceid}`).text(tbResResourceClass);
            $(`#tbResResourceCalendar-${resourceid}`).text(tbResResourceCalendar);
            $(`#tbResPIN-${resourceid}`).text(tbResPIN);
            $(`#tbResExtSystemResID-${resourceid}`).text(tbResExtSystemResID);
            $(`#tbResSortOrder-${resourceid}`).text(tbResSortOrder);
            $(`#tbResLastModified-${resourceid}`).text(tbResLastModified);
            $(`#tbResLabourType-${resourceid}`).text(tbResLabourType);

         };
      }; // end onsuccess
   });
};



function initDtResources() {
   $('#dataTableResources').DataTable({
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',  // f = search 
      "dom": '<"top"f>rt<"bottom"Bilp>',
      // dom: 'Bfrtip',
      buttons: [
         'copy', 'excel', 'pdf'
      ],
      select: true,
      colReorder: true,  // change what columns show first, second etc.
      responsive: true,
      fixedHeader: true,
      destroy: true,

   });

};


$(document).on('click', '#clearForm', function (e) {
   //$('#clearForm').on('click', function () {
   console.log("clicked reset resource")
   resetResourcesInput()
});



/* show or hide brief descriptn because it is truncated */
setTimeout(() => {
   $('.showhide').on('dblclick', function () {

      console.log("clicked showhide")

      if ($('.showhide').hasClass('truncate')) {
         $('.showhide').removeClass('truncate');
         $('.showhide').addClass('truncated');
      } else {
         $('.showhide').removeClass('truncated');
         $('.showhide').addClass('truncate');

      }
   });
}, 500);



const resetResourcesInput = () => {


   $("#tbResID").val('');
   $("#tbResCustomerID").val('');
   $("#tbResAzureID").val('');
   $("#tbResName").val('');
   $("#tbResNameShort").val('');
   $("#tbResEmail").val('');
   $("#tbResStatus").val('');
   $("#tbResDepartment").val('');
   $("#tbResManager").val('');
   $("#tbResPayRate").val('');
   $("#tbResCoordTop").val('');
   $("#tbResCoordLeft").val('');
   $("#tbResTeam").val('');
   $("#tbResLocation").val('');
   $("#tbResCostCode").val('');
   $("#tbResTeamLeader").val('');
   $("#tbResSupervisor").val('');
   $("#tbResPasswordHash").val('');
   $("#tbResResourceType").val('');
   $("#tbResResourceClass").val('');
   $("#tbResResourceCalendar").val('');
   $("#tbResPIN").val('');
   $("#tbResExtSystemResID").val('');
   $("#tbResSortOrder").val('');
   $("#tbResLastModified").val('');
   $("#tbResLabourType").val('');



}

const buildIdsResources = (itemid) => {
   return {
      editID: "edit_" + itemid,
      deleteID: "delete_" + itemid,
      listItemID: "listItem_" + itemid,
      resourceID: "resource_" + itemid,
      bpselectorID: "bpselector_" + itemid
   }
}

const displayResources = (data) => {
   data.forEach((Item) => {
      let ids = buildIdsResources(Item.tbResID);
      // console.log("displayResources ids: " + JSON.stringify(ids))
      // looks like this ids: {"editID":"edit_83","deleteID":"delete_83","listItemID":"listItem_83","resourceID":"resource_83","bpselectorID":"bpselector_83"}
      displayDT = $("#displayDtResources");
      displayDT.append(generateDatatableResources(Item, ids));
      fillResourcesForm(Item, ids.resourceID, ids.editID);
      deleteResource(Item, ids.listItemID, ids.deleteID);
   });
}

