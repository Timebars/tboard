
// this file creates single json file of all tb data in indexeddb.
// merge this into tbDatabase later.

import { displaySuccessMessageToast, displayFailureMessageToast, openIDBGetStore, createJsonFileSimple, clearObjectStore, importJsonDataUniversal } from './tbdatabase';

import XLSX from "xlsx";

// functions from here https://gist.github.com/loilo/ed43739361ec718129a15ae5d531095b

// import excel file, single json file (all stores), or individual store files
// using xlsx npm package import directly into idb from excel

$(document).on('click', '#btnExportDataAsOneJSONFile', function () {
    exportDataAsOneJSONFile1()
});



const DB_NAME = 'TimebarsIDB';
const DB_VERSION = 6;
const multiStoreJSONFileName = 'tbFullBackup.js'
const multiStoreExcelFileName = 'tbClient'

// set up the event listener for the drop
let canvasdrop = document.getElementById("gcipDropArea");

// Prevent default drag behaviors
["dragenter", "dragover", "dragleave", "drop"].forEach(item => {
    canvasdrop.addEventListener(item, preventDefaults, false);
    document.body.addEventListener(item, preventDefaults, false);
});
function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}

// Highlight drop area when item is dragged over it
["dragenter"].forEach(item => {
    canvasdrop.addEventListener(item, highlightCanvas, false);
});
["dragleave", "drop"].forEach(item => {
    canvasdrop.addEventListener(item, unhighlightCanvas, false);
});


// Handle dropped files
canvasdrop.addEventListener("drop", handleCanvasDrop, false);


function highlightCanvas(e) {
    console.log("here")

    $("#show-timebars").empty();


    document.getElementById("gcipDropArea").classList.add("canvasDropHighlight");
    document.getElementById("canvasDropText").style.display = 'block';
}

function unhighlightCanvas(e) {

    document.getElementById("gcipDropArea").classList.remove("canvasDropHighlight");
    //  document.getElementById("canvasDropText").style.display = 'none';
}



/* **************   IMPORT Functions ************** */

// main all scenario import logic for excel, json or multi json
function handleCanvasDrop(e) {
    e.stopPropagation();
    e.preventDefault();

    let canvasDropFiles = e.dataTransfer.files;
    var selectedFile = ""
    var storeName = ""
    var txt;

    // backup first
    var r = confirm("Generating backup files first including a single file complete backup!");
    if (r == true) {
        txt = "You pressed OK!";
        console.log(txt);
        ////////         
        //make backup first
        exportDataAsOneJSONFile1();

    } else {
        txt = "You pressed Cancel!";
        console.log(txt);
    }

    // for each file, get storname and populate idb
    for (var j = 0; j < canvasDropFiles.length; j++) {

        selectedFile = canvasDropFiles[j];
        let fullname = selectedFile.name
        let fileExt = (fullname.split(".")[1]).trim();
        let fileName = fullname.split(".")[0];

        console.log(
            `dropped ${JSON.stringify(
                fullname
            )} on canvas, and will import data it recognizes`
        );

        // partial name, first 6 chars (allows user to to change file name past 6)
        var pname = (selectedFile.name).substr(0, 6);

        console.log("file ext : " + fileExt)

        // exit if not right file extention
        if (fileExt === "xlsx" || fileExt === "xlsm" || fileExt === "xls" || fileExt === "ods" || fileExt === "js" || fileExt === "json") {

            displaySuccessMessageToast("Step A: Is valid file extension!");
        } else {
            displayFailureMessageToast(`Step A: Not valid file extension, must be js, json, xls, xlsx, xlsm or ods`)
            return
        }

        if (pname === "tbClie" && (fileExt === "xlsx" || fileExt === "xlsm" || fileExt === "xls" || fileExt === "ods")) {
            // is the excel file
            displaySuccessMessageToast("Step B: Is a valid Excel file name, import starting!");
            importExcelFile(selectedFile)
            return
        } else if (fileExt === "xlsx" || fileExt === "xlsm" || fileExt === "xls" || fileExt === "ods") {
            displayFailureMessageToast(`B nothing imported, not valid Excel file name, must start with tbClient`)
            return
        }

        if (pname == "tbFull") {
            // is the single file multi store backup file for importing back in
            storeName = "NA"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbTime") {
            storeName = "tbTimebars"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbBase") {
            storeName = "tbBaseline"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)

        } else if (pname == "tbTags") {
            storeName = "tbTags"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbReso") {
            storeName = "tbResources"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbMeta") {
            storeName = "tbMetaData"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbAdmi") {
            storeName = "tbAdminPanel"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbResC") {
            storeName = "tbResCalcs"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbDocu") {
            storeName = "tbDocuments"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else if (pname == "tbAddT") {
            storeName = "tbAddTimebars"
            onDropReadJSONTextFileForImport(selectedFile, storeName, pname)
        } else {
            displayFailureMessageToast(`C nothing imported, not valid timebar file name`)
            return
        }
    };
}
// this will process individual json files per store or a single js file with all stores.
function onDropReadJSONTextFileForImport(selectedFile, storeName, pname) {
    console.log("storeName : " + storeName)
    var reader = new FileReader();
    reader.onload = function (e) {

        let jsonData = this.result

        if (jsonData) {
            try {
                let a = JSON.parse(jsonData);
            } catch (e) {
                displayFailureMessageToast(
                    `bad file, could not start restore/import: ${e}`
                );
                return;
            }
        }
        displaySuccessMessageToast("Is a valid JSON Format");
        jsonData = JSON.stringify(jsonData);
        let jsondata = JSON.parse(jsonData);
        // console.log("data: " + jsonData)



        // all stores in one file --- if file name is tbFullBackup.js, then call   importMultiStoreJsonFile(jsonData), else call??
        if (pname === "tbFull") {
            displaySuccessMessageToast("E full restore starting!");
            importMultiStoreJsonFile(jsondata) // not database is cleard there not here

        } else {
            //////            // restoring one store, backup, clear then import
            // backup first before deleting it
            displaySuccessMessageToast("F Individual file restore starting!");
            createJsonFileSimple(storeName)
            // delete all data in the store so we can fill with new data from the canvasDrop evnet
            clearObjectStore(storeName)
            // restore data here
            importJsonDataUniversal(jsondata, storeName)
        }

    };  // end read onload
    reader.readAsText(selectedFile);
}



function importExcelFile(selectedFile) {

    let reader = new FileReader();
    let name = selectedFile.name;
    let storeName = ""
    console.log("excel file name " + name)

    reader.onload = function (e) {

        let data = new Uint8Array(e.target.result);
        let wb = XLSX.read(data, { type: "array" });
        /* DO SOMETHING WITH wb HERE */
        wb.SheetNames.forEach((sht, i) => {

            console.log(`Sheet #${i + 1}: ${sht}`);

            storeName = "tbTimebars";
            if (sht === "Timebars") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }

            storeName = "tbMetaData";
            if (sht === "MetaData") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
            storeName = "tbTags";
            if (sht === "Tags") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
            storeName = "tbResources";
            if (sht === "Resources") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
            storeName = "tbAdminPanel";
            if (sht === "AdminPanel") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
            storeName = "tbDocuments";
            if (sht === "Documents") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
            storeName = "tbResCalcs";
            if (sht === "ResCalcs") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
            storeName = "tbBaseline";
            if (sht === "Baseline") {
                let ws = wb.Sheets[sht];
                let jsondata = XLSX.utils.sheet_to_json(ws, {
                    raw: false,
                    defval: null
                });
                clearObjectStore(storeName);
                importExcelFileFinalStep(jsondata, storeName);
            }
        });
        data = "";
        wb = "";
    };
    reader.readAsArrayBuffer(selectedFile);
}



// import put from Excel sheet or js file
export function importExcelFileFinalStep(jsonData, storeName) {
    openIDBGetStore(storeName, "readwrite", function (store) {
        let request;
        let i;
        try {
            // add rows
            for (i = 1; i < jsonData.length; i++) {
                //  console.log(`Data item: ${i} `)

                request = store.put(jsonData[i]);
            }
        } catch (e) {
            displayFailureMessageToast(
                "Excel populator: Some Problem While adding data, if data already exists, this will happen, clear data out then try again"
            );
            throw e;
        }
        request.onsuccess = function (evnt) {
            displaySuccessMessageToast(`App data added to store: ${storeName}.<br> `);
            console.log(`Data added to store: ${storeName} `);
        };
        request.onerror = function () {
            displayFailureMessageToast(
                "data must exist already, will not overwite it. to check hit F12 key > Application Tab > IndexedDB " +
                this.error
            );
            console.log("Error Default data must exist already! " + this.error);
        };
    });
}

//Import Data, get the local file and serialize it.

export function importMultiStoreJsonFile(serializedData) {
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || { READ_WRITE: "readwrite" }; // This line should only be needed if it is needed to support the object's constants for older browsers
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    if (!window.indexedDB) {
        alert("Sorry!Your browser doesn't support IndexedDB");
    }
    let request = window.indexedDB.open(DB_NAME, DB_VERSION);
    //Handle error On db open
    request.onerror = function (event) {
        console.log(event.target.errorCode);
    };
    request.onsuccess = function (event) {
        // Handle success On db open, get the db object, then open store
        let idb = this.result;

        clearDatabase(idb)
            .then(() => importMultiStoreJsonFileFinalStep(idb, serializedData))
            .then(() => {
                console.log('Successfully cleared database and imported data')
                displaySuccessMessageToast('Successfully cleared old data and imported new data from the file you dropped')
            })
            .catch(error => {
                console.error('Could not clear & import database:', error)
                displayFailureMessageToast(`bad file, could not start restore/import: ${error}`)
            })
    }
}
// Import all data from multi store JSON into an IndexedDB database.
export function importMultiStoreJsonFileFinalStep(idbDatabase, json) {
    return new Promise((resolve, reject) => {
        const transaction = idbDatabase.transaction(
            idbDatabase.objectStoreNames,
            'readwrite'
        )
        transaction.addEventListener('error', reject)
        var importObject = JSON.parse(json)
        for (const storeName of idbDatabase.objectStoreNames) {
            let count = 0
            for (const toAdd of importObject[storeName]) {
                const request = transaction.objectStore(storeName).add(toAdd)
                request.addEventListener('success', () => {
                    count++
                    if (count === importObject[storeName].length) {
                        // Added all objects for this store
                        delete importObject[storeName]
                        if (Object.keys(importObject).length === 0) {
                            // Added all object stores
                            resolve()
                        }
                    }
                })
            }
        }
    })
}


/* **************   EXPORT Functions ************** */

// begin single export of multiple or all stores
export function exportDataAsOneJSONFile1() {
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || { READ_WRITE: "readwrite" }; // This line should only be needed if it is needed to support the object's constants for older browsers
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    if (!window.indexedDB) {
        alert("Sorry!Your browser doesn't support IndexedDB");
    }
    let request = window.indexedDB.open(DB_NAME, DB_VERSION);
    //Handle error On db open
    request.onerror = function (event) {
        console.log(event.target.errorCode);
    };
    request.onsuccess = function (event) {
        // Handle success On db open, get the db object, then open store
        let idb = this.result;
        exportToJson(idb)
            .then(result => {
                //  console.log('Exported JSON string:', result)
                var blob = new Blob([result], { type: "text/plain" });
                saveAs(blob, multiStoreJSONFileName);
            })
            .catch(error => {
                console.error('Something went wrong during export:', error)
            })
    }
}

// step 2 Export all data from an IndexedDB database
export function exportToJson(idbDatabase) {
    return new Promise((resolve, reject) => {
        const exportObject = {}
        if (idbDatabase.objectStoreNames.length === 0) {
            resolve(JSON.stringify(exportObject))
        } else {
            const transaction = idbDatabase.transaction(
                idbDatabase.objectStoreNames,
                'readonly'
            )

            transaction.addEventListener('error', reject)

            for (const storeName of idbDatabase.objectStoreNames) {
                const allObjects = []
                transaction
                    .objectStore(storeName)
                    .openCursor()
                    .addEventListener('success', event => {
                        const cursor = event.target.result
                        if (cursor) {
                            // Cursor holds value, put it into store data
                            allObjects.push(cursor.value)
                            cursor.continue()
                        } else {
                            // No more values, store is done
                            exportObject[storeName] = allObjects

                            // Last store was handled
                            if (
                                idbDatabase.objectStoreNames.length ===
                                Object.keys(exportObject).length
                            ) {
                                resolve(JSON.stringify(exportObject))
                            }
                        }
                    })
            }
        }
    })
}

// Clear a database
export function clearDatabase(idbDatabase) {
    return new Promise((resolve, reject) => {
        const transaction = idbDatabase.transaction(
            idbDatabase.objectStoreNames,
            'readwrite'
        )
        transaction.addEventListener('error', reject)

        let count = 0
        for (const storeName of idbDatabase.objectStoreNames) {
            transaction
                .objectStore(storeName)
                .clear()
                .addEventListener('success', () => {
                    count++
                    if (count === idbDatabase.objectStoreNames.length) {
                        // Cleared all object stores
                        resolve()
                    }
                })
        }
    })
}


// this one not used, not sure why
export function exportDataAsOneJSONFile2(store) {

    let fileName = 'tbFullBackup.js'
    let mybigfile = []

    returnAllStoreRowsForBackupNew(store, function (fnCallbackJSON) {
        let myJsonFile = fnCallbackJSON
        let hdrJSON = '{"name": "Timebars Inc All Data", "type": "IndexedDB JSON Data Structure and All Data", "stores": {'
        myJsonFile = hdrJSON + '"' + store + '":[' + myJsonFile + ']}}'
        //  console.log("myJsonFile: " + myJsonFile)



        if (store === "tbTimebars") {
            mybigfile.push(myJsonFile)
            //  console.log("mybigfile: " + mybigfile)
        }

        //  save to download folder
        console.log("mybigfile: " + mybigfile)
    });

    var blob = new Blob([mybigfile], { type: "text/plain" });
    saveAs(blob, fileName);

}
function returnAllStoreRowsForBackupNew(storeOrTable, fnCallbackJsonAll) {
    var storeOrTable = storeOrTable //"tbTimebars"
    // alert(storeOrTable)

    var jsonString = ""; // this will by my array of json objects from idb
    var jsonStrings = "";

    openIDBGetStore(storeOrTable, 'readonly', function (store) {
        store.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {

                jsonString = JSON.stringify(cursor.value)
                jsonStrings = jsonStrings += jsonString
                cursor.continue();
                // alert(cursor.value.tbID)
                jsonStrings = jsonStrings += ","

            } else {
                // remove the trailing comma
                jsonStrings = jsonStrings.replace(/,$/, '');
                // alert("done looping through object store" + jsonStrings)
                fnCallbackJsonAll(jsonStrings);
            }
        } // end open curso
    }); // end get store
};


// this is my target structure, but was not used.
export const tbJSON = {
    "name": "Timebars Inc All Data",
    "type": "IndexedDB JSON Data Structure and All Data",
    "stores": {
        "tbAdminPanel": [
            {
                "ap_name": "Standard Config Settings",
                "apID": "1",
                "apCustomerID": "44",
                "apFilteredFrom": "Canvas",
                "apAzureID": "a1",
                "apTS_WeeklyOrMonthly": "Weekly",
                "apTS_WeeklyPxFactor": "10",
                "apTS_Start": "15-Dec-2019",
                "apTS_Finish": "",
                "apHelpCoordTop": "50",
                "apHelpCoordLeft": "400",
                "apTS_MonthlyPxFactor": "2",
                "apStatusDate": "6-Jan-2020",
                "apFilteredFromTbID": "nn",
                "ap_HolidayDate": "",
                "ap_HolidayName": "",
                "apSchMethod": "Standard",
                "apTbVisibleLevel": "L5",
                "totalNumberOfPeople": "",
                "numberOfPeoplePerLine": "",
                "pxPerPerson": "",
                "apHideCompletedBarsYN": "false"
            },
            {
                "ap_name": "Bar Graph Settings",
                "apID": "2",
                "apCustomerID": "44",
                "apFilteredFrom": "Canvas",
                "apAzureID": "",
                "apTS_WeeklyOrMonthly": "",
                "apTS_WeeklyPxFactor": "",
                "apTS_Start": "",
                "apTS_Finish": "",
                "apHelpCoordTop": "",
                "apHelpCoordLeft": "",
                "apTS_MonthlyPxFactor": "",
                "apStatusDate": "",
                "apFilteredFromTbID": "",
                "ap_HolidayDate": "",
                "ap_HolidayName": "",
                "apSchMethod": "",
                "apTbVisibleLevel": "",
                "totalNumberOfPeople": "30",
                "numberOfPeoplePerLine": "3",
                "pxPerPerson": "7",
                "apHideCompletedBarsYN": ""
            },
            {
                "ap_name": "Standard Holidays",
                "apID": "3",
                "apCustomerID": "",
                "apFilteredFrom": "",
                "apAzureID": "",
                "apTS_WeeklyOrMonthly": "",
                "apTS_WeeklyPxFactor": "",
                "apTS_Start": "",
                "apTS_Finish": "",
                "apHelpCoordTop": "",
                "apHelpCoordLeft": "",
                "apTS_MonthlyPxFactor": "",
                "apStatusDate": "",
                "apFilteredFromTbID": "",
                "ap_HolidayDate": "25-Dec-2020",
                "ap_HolidayName": "Christmas",
                "apSchMethod": "",
                "apTbVisibleLevel": "",
                "totalNumberOfPeople": "",
                "numberOfPeoplePerLine": "",
                "pxPerPerson": "",
                "apHideCompletedBarsYN": ""
            }
        ],
        "tbTimebars": [
            {
                "tbSelfKey2": 0,
                "tbName": "My New Portfolio",
                "tbType": "Portfolio",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "107",
                "tbCoordLeft": "210",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "0",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "0",
                "tbConstraintType": 0,
                "tbConstraintDate": "",
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": "01",
                "canvasNo": 1,
                "tbAzureID": "01",
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "525"
            }, {
                "tbSelfKey2": "525",
                "tbName": "My New Program/Project",
                "tbType": "Project",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "168",
                "tbCoordLeft": "217",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "526"
            }, {
                "tbSelfKey2": "526",
                "tbName": "My New Task",
                "tbType": "Task",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "268",
                "tbCoordLeft": "289",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "527"
            }, {
                "tbSelfKey2": "526",
                "tbName": "My New Milestone",
                "tbType": "Milestone",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "228",
                "tbCoordLeft": "461",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "528"
            }, {
                "tbSelfKey2": "526",
                "tbName": "My New Sub-Project",
                "tbType": "Sub-Project",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "314",
                "tbCoordLeft": "289",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbTextColor": "1",
                "tbID": "529"
            }, {
                "tbSelfKey2": "526",
                "tbName": "My New Gate",
                "tbType": "Gate",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "204",
                "tbCoordLeft": "330",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "530"
            }
        ],
        "tbMetaData": [
            {
                "tbMDID": "525",
                "tbMDGate": 0,
                "tbMDHealthOverall": 0,
                "tbMDHealthScope": 0,
                "tbMDHealthCost": 0,
                "tbMDHealthIssues": 0,
                "tbMDHealthRisk": 0,
                "tbMDHealthSchedule": 0,
                "tbMDHealthHours": 0,
                "tbMDInvestmentCategory": 0,
                "tbMDInvestmentInitiative": 0,
                "tbMDInvestmentObjective": 0,
                "tbMDInvestmentStrategy": 0,
                "tbMDROMEstimate": 0,
                "tbMDPortfolio": 0,
                "tbMDProgram": 0,
                "tbMDProgActivityAlignment": 0,
                "tbMDSize": 0,
                "tbMDStageApprover": 0,
                "tbMDWrittenBy": 0,
                "tbMDBusinessAdvisor": 0,
                "tbMDBusinessOwner": 0,
                "tbMDDeliveryManager": 0,
                "tbMDOrgManager": 0,
                "tbMDPriorityStrategic": 0,
                "tbMDSponsoringDepartment": 0,
                "tbMDPrimaryContact": 0,
                "tbMDBenefitCostRatio": 0,
                "tbMDContactNumber": 0,
                "tbMDResponsibleTeam": 0,
                "tbMDRiskVsSizeAndComplexity": 0,
                "tbMDEcnomicValueAdded": 0,
                "tbMDEstimationClass": 0,
                "tbMDInternalRateOfReturn": 0,
                "tbMDSprintName": 0,
                "tbMDSunkCosts": 0,
                "tbMDSyncNotes": 0,
                "tbMDNotesProject": 0,
                "tbMDContractNumber": 0,
                "tbMDNetPresentValue": 0,
                "tbMDOpportunityCost": 0,
                "tbMDPaybackPeriod": 0,
                "tbMDPrimaryLineOfBusiness": 0,
                "tbMDBackgroundInfo": 0,
                "tbMDCapabilitiesNeeded": 0,
                "tbMDConsequence": 0,
                "tbMDExpectedBenfits": 0,
                "tbMDProblemOpportunity": 0,
                "tbMDConstraintsAssumptions": 0,
                "tbMDCostBenefitAnalysis": 0,
                "tbMDExecutiveSummary": 0,
                "tbMDSeniorLevelCommittment": 0,
                "tbMDStakeholderDescription": 0,
                "tbMDNotesWorkflow": 0,
                "tbMDOther5": 0,
                "tbMDOther6": 0,
                "tbMDOther7": 0,
                "tbMDOther8": 0,
                "tbMDOther9": 0,
                "tbMDOther10": 0,
                "tbMDOther11": 0,
                "tbMDOther12": 0,
                "tbMDName": "auto-created for tbID cross ref: 525",
                "tbMDCustomerID": "44",
                "tbMDAzureID": 0,
                "tbMDNameShort": 0,
                "tbMDProjectNumber": 0,
                "tbMDDescription": 0,
                "tbMDNotes": 0,
                "tbMDExtLink1": 0,
                "tbMDExtSystemID1": 0,
                "tbMDSortOrder": 0,
                "tbMDtbLastModified": 0,
                "tbMDPriority": 0,
                "tbMDStatus": 0,
                "tbMDState": 0,
                "tbMDSeverity": 0,
                "tbMDStage": 0,
                "tbMDPhase": 0,
                "tbMDCategory": 0,
                "tbMDHealth": 0,
                "tbMDResponsibility": 0,
                "tbMDDepartment": 0,
                "tbMDExSponsor": 0,
                "tbMDPM": 0,
                "tbMDProjectType": 0,
                "tbMDShowIn": 0,
                "tbMDYesNoSelector": 0,
                "tbMDProduct": 0,
                "tbMDContact": 0,
                "tbMDWBS": 0,
                "tbMDWeighting": 0,
                "tbMDLocation": 0,
                "tbMDPrimarySkill": 0,
                "tbMDPrimaryRole": 0,
                "tbMDOther2": 0,
                "tbMDOther3": 0,
                "tbMDOther4": 0,
                "canvasNo": 1,
                "tbMDRefID": "a573"
            }, {
                "tbMDID": "526",
                "tbMDGate": 0,
                "tbMDHealthOverall": 0,
                "tbMDHealthScope": 0,
                "tbMDHealthCost": 0,
                "tbMDHealthIssues": 0,
                "tbMDHealthRisk": 0,
                "tbMDHealthSchedule": 0,
                "tbMDHealthHours": 0,
                "tbMDInvestmentCategory": 0,
                "tbMDInvestmentInitiative": 0,
                "tbMDInvestmentObjective": 0,
                "tbMDInvestmentStrategy": 0,
                "tbMDROMEstimate": 0,
                "tbMDPortfolio": 0,
                "tbMDProgram": 0,
                "tbMDProgActivityAlignment": 0,
                "tbMDSize": 0,
                "tbMDStageApprover": 0,
                "tbMDWrittenBy": 0,
                "tbMDBusinessAdvisor": 0,
                "tbMDBusinessOwner": 0,
                "tbMDDeliveryManager": 0,
                "tbMDOrgManager": 0,
                "tbMDPriorityStrategic": 0,
                "tbMDSponsoringDepartment": 0,
                "tbMDPrimaryContact": 0,
                "tbMDBenefitCostRatio": 0,
                "tbMDContactNumber": 0,
                "tbMDResponsibleTeam": 0,
                "tbMDRiskVsSizeAndComplexity": 0,
                "tbMDEcnomicValueAdded": 0,
                "tbMDEstimationClass": 0,
                "tbMDInternalRateOfReturn": 0,
                "tbMDSprintName": 0,
                "tbMDSunkCosts": 0,
                "tbMDSyncNotes": 0,
                "tbMDNotesProject": 0,
                "tbMDContractNumber": 0,
                "tbMDNetPresentValue": 0,
                "tbMDOpportunityCost": 0,
                "tbMDPaybackPeriod": 0,
                "tbMDPrimaryLineOfBusiness": 0,
                "tbMDBackgroundInfo": 0,
                "tbMDCapabilitiesNeeded": 0,
                "tbMDConsequence": 0,
                "tbMDExpectedBenfits": 0,
                "tbMDProblemOpportunity": 0,
                "tbMDConstraintsAssumptions": 0,
                "tbMDCostBenefitAnalysis": 0,
                "tbMDExecutiveSummary": 0,
                "tbMDSeniorLevelCommittment": 0,
                "tbMDStakeholderDescription": 0,
                "tbMDNotesWorkflow": 0,
                "tbMDOther5": 0,
                "tbMDOther6": 0,
                "tbMDOther7": 0,
                "tbMDOther8": 0,
                "tbMDOther9": 0,
                "tbMDOther10": 0,
                "tbMDOther11": 0,
                "tbMDOther12": 0,
                "tbMDName": "auto-created for tbID cross ref: 526",
                "tbMDCustomerID": "44",
                "tbMDAzureID": 0,
                "tbMDNameShort": 0,
                "tbMDProjectNumber": 0,
                "tbMDDescription": "We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.",
                "tbMDNotes": "These are a bunch of notes regarding this project",
                "tbMDExtLink1": 0,
                "tbMDExtSystemID1": 0,
                "tbMDSortOrder": 0,
                "tbMDtbLastModified": 0,
                "tbMDPriority": 0,
                "tbMDStatus": 0,
                "tbMDState": 0,
                "tbMDSeverity": 0,
                "tbMDStage": 0,
                "tbMDPhase": 0,
                "tbMDCategory": 0,
                "tbMDHealth": 0,
                "tbMDResponsibility": 0,
                "tbMDDepartment": 0,
                "tbMDExSponsor": 0,
                "tbMDPM": 0,
                "tbMDProjectType": 0,
                "tbMDShowIn": 0,
                "tbMDYesNoSelector": 0,
                "tbMDProduct": 0,
                "tbMDContact": 0,
                "tbMDWBS": 0,
                "tbMDWeighting": 0,
                "tbMDLocation": 0,
                "tbMDPrimarySkill": 0,
                "tbMDPrimaryRole": 0,
                "tbMDOther2": 0,
                "tbMDOther3": 0,
                "tbMDOther4": 0,
                "canvasNo": 1,
                "tbMDRefID": "a573"
            }, {
                "tbMDID": "527",
                "tbMDName": "auto-created for tbID cross ref: 527",
                "tbMDCustomerID": "44",
                "tbMDAzureID": 0,
                "tbMDNameShort": 0,
                "tbMDProjectNumber": 0,
                "tbMDDescription": 0,
                "tbMDNotes": 0,
                "tbMDExtLink1": 0,
                "tbMDExtSystemID1": 0,
                "tbMDSortOrder": 0,
                "tbMDtbLastModified": 0,
                "tbMDPriority": 0,
                "tbMDStatus": 0,
                "tbMDState": 0,
                "tbMDSeverity": 0,
                "tbMDStage": 0,
                "tbMDPhase": 0,
                "tbMDCategory": 0,
                "tbMDHealth": 0,
                "tbMDResponsibility": 0,
                "tbMDDepartment": 0,
                "tbMDExSponsor": 0,
                "tbMDPM": 0,
                "tbMDProjectType": 0,
                "tbMDShowIn": 0,
                "tbMDYesNoSelector": 0,
                "tbMDProduct": 0,
                "tbMDContact": 0,
                "tbMDWBS": 0,
                "tbMDWeighting": 0,
                "tbMDLocation": 0,
                "tbMDPrimarySkill": 0,
                "tbMDPrimaryRole": 0,
                "tbMDOther2": 0,
                "tbMDOther3": 0,
                "tbMDOther4": 0,
                "canvasNo": 1,
                "tbMDRefID": "a573"
            }, {
                "tbMDID": "528",
                "tbMDName": "auto-created for tbID cross ref: 528",
                "tbMDCustomerID": "44",
                "tbMDAzureID": 0,
                "tbMDNameShort": 0,
                "tbMDProjectNumber": 0,
                "tbMDDescription": 0,
                "tbMDNotes": 0,
                "tbMDExtLink1": 0,
                "tbMDExtSystemID1": 0,
                "tbMDSortOrder": 0,
                "tbMDtbLastModified": 0,
                "tbMDPriority": 0,
                "tbMDStatus": 0,
                "tbMDState": 0,
                "tbMDSeverity": 0,
                "tbMDStage": 0,
                "tbMDPhase": 0,
                "tbMDCategory": 0,
                "tbMDHealth": 0,
                "tbMDResponsibility": 0,
                "tbMDDepartment": 0,
                "tbMDExSponsor": 0,
                "tbMDPM": 0,
                "tbMDProjectType": 0,
                "tbMDShowIn": 0,
                "tbMDYesNoSelector": 0,
                "tbMDProduct": 0,
                "tbMDContact": 0,
                "tbMDWBS": 0,
                "tbMDWeighting": 0,
                "tbMDLocation": 0,
                "tbMDPrimarySkill": 0,
                "tbMDPrimaryRole": 0,
                "tbMDOther2": 0,
                "tbMDOther3": 0,
                "tbMDOther4": 0,
                "canvasNo": 1,
                "tbMDRefID": "a573"
            }, {
                "tbMDID": "529",
                "tbMDGate": 0,
                "tbMDHealthOverall": 0,
                "tbMDHealthScope": 0,
                "tbMDHealthCost": 0,
                "tbMDHealthIssues": 0,
                "tbMDHealthRisk": 0,
                "tbMDHealthSchedule": 0,
                "tbMDHealthHours": 0,
                "tbMDInvestmentCategory": 0,
                "tbMDInvestmentInitiative": 0,
                "tbMDInvestmentObjective": 0,
                "tbMDInvestmentStrategy": 0,
                "tbMDROMEstimate": 0,
                "tbMDPortfolio": 0,
                "tbMDProgram": 0,
                "tbMDProgActivityAlignment": 0,
                "tbMDSize": 0,
                "tbMDStageApprover": 0,
                "tbMDWrittenBy": 0,
                "tbMDBusinessAdvisor": 0,
                "tbMDBusinessOwner": 0,
                "tbMDDeliveryManager": 0,
                "tbMDOrgManager": 0,
                "tbMDPriorityStrategic": 0,
                "tbMDSponsoringDepartment": 0,
                "tbMDPrimaryContact": 0,
                "tbMDBenefitCostRatio": 0,
                "tbMDContactNumber": 0,
                "tbMDResponsibleTeam": 0,
                "tbMDRiskVsSizeAndComplexity": 0,
                "tbMDEcnomicValueAdded": 0,
                "tbMDEstimationClass": 0,
                "tbMDInternalRateOfReturn": 0,
                "tbMDSprintName": 0,
                "tbMDSunkCosts": 0,
                "tbMDSyncNotes": 0,
                "tbMDNotesProject": 0,
                "tbMDContractNumber": 0,
                "tbMDNetPresentValue": 0,
                "tbMDOpportunityCost": 0,
                "tbMDPaybackPeriod": 0,
                "tbMDPrimaryLineOfBusiness": 0,
                "tbMDBackgroundInfo": 0,
                "tbMDCapabilitiesNeeded": 0,
                "tbMDConsequence": 0,
                "tbMDExpectedBenfits": 0,
                "tbMDProblemOpportunity": 0,
                "tbMDConstraintsAssumptions": 0,
                "tbMDCostBenefitAnalysis": 0,
                "tbMDExecutiveSummary": 0,
                "tbMDSeniorLevelCommittment": 0,
                "tbMDStakeholderDescription": 0,
                "tbMDNotesWorkflow": 0,
                "tbMDOther5": 0,
                "tbMDOther6": 0,
                "tbMDOther7": 0,
                "tbMDOther8": 0,
                "tbMDOther9": 0,
                "tbMDOther10": 0,
                "tbMDOther11": 0,
                "tbMDOther12": 0,
                "tbMDName": "auto-created for tbID cross ref: 529",
                "tbMDCustomerID": "44",
                "tbMDAzureID": 0,
                "tbMDNameShort": 0,
                "tbMDProjectNumber": 0,
                "tbMDDescription": 0,
                "tbMDNotes": 0,
                "tbMDExtLink1": 0,
                "tbMDExtSystemID1": 0,
                "tbMDSortOrder": 0,
                "tbMDtbLastModified": 0,
                "tbMDPriority": 0,
                "tbMDStatus": 0,
                "tbMDState": 0,
                "tbMDSeverity": 0,
                "tbMDStage": 0,
                "tbMDPhase": 0,
                "tbMDCategory": 0,
                "tbMDHealth": 0,
                "tbMDResponsibility": 0,
                "tbMDDepartment": 0,
                "tbMDExSponsor": 0,
                "tbMDPM": 0,
                "tbMDProjectType": 0,
                "tbMDShowIn": 0,
                "tbMDYesNoSelector": 0,
                "tbMDProduct": 0,
                "tbMDContact": 0,
                "tbMDWBS": 0,
                "tbMDWeighting": 0,
                "tbMDLocation": 0,
                "tbMDPrimarySkill": 0,
                "tbMDPrimaryRole": 0,
                "tbMDOther2": 0,
                "tbMDOther3": 0,
                "tbMDOther4": 0,
                "canvasNo": 1,
                "tbMDRefID": "a573"
            }, {
                "tbMDID": "530",
                "tbMDGate": 0,
                "tbMDHealthOverall": 0,
                "tbMDHealthScope": 0,
                "tbMDHealthCost": 0,
                "tbMDHealthIssues": 0,
                "tbMDHealthRisk": 0,
                "tbMDHealthSchedule": 0,
                "tbMDHealthHours": 0,
                "tbMDInvestmentCategory": 0,
                "tbMDInvestmentInitiative": 0,
                "tbMDInvestmentObjective": 0,
                "tbMDInvestmentStrategy": 0,
                "tbMDROMEstimate": 0,
                "tbMDPortfolio": 0,
                "tbMDProgram": 0,
                "tbMDProgActivityAlignment": 0,
                "tbMDSize": 0,
                "tbMDStageApprover": 0,
                "tbMDWrittenBy": 0,
                "tbMDBusinessAdvisor": 0,
                "tbMDBusinessOwner": 0,
                "tbMDDeliveryManager": 0,
                "tbMDOrgManager": 0,
                "tbMDPriorityStrategic": 0,
                "tbMDSponsoringDepartment": 0,
                "tbMDPrimaryContact": 0,
                "tbMDBenefitCostRatio": 0,
                "tbMDContactNumber": 0,
                "tbMDResponsibleTeam": 0,
                "tbMDRiskVsSizeAndComplexity": 0,
                "tbMDEcnomicValueAdded": 0,
                "tbMDEstimationClass": 0,
                "tbMDInternalRateOfReturn": 0,
                "tbMDSprintName": 0,
                "tbMDSunkCosts": 0,
                "tbMDSyncNotes": 0,
                "tbMDNotesProject": 0,
                "tbMDContractNumber": 0,
                "tbMDNetPresentValue": 0,
                "tbMDOpportunityCost": 0,
                "tbMDPaybackPeriod": 0,
                "tbMDPrimaryLineOfBusiness": 0,
                "tbMDBackgroundInfo": 0,
                "tbMDCapabilitiesNeeded": 0,
                "tbMDConsequence": 0,
                "tbMDExpectedBenfits": 0,
                "tbMDProblemOpportunity": 0,
                "tbMDConstraintsAssumptions": 0,
                "tbMDCostBenefitAnalysis": 0,
                "tbMDExecutiveSummary": 0,
                "tbMDSeniorLevelCommittment": 0,
                "tbMDStakeholderDescription": 0,
                "tbMDNotesWorkflow": 0,
                "tbMDOther5": 0,
                "tbMDOther6": 0,
                "tbMDOther7": 0,
                "tbMDOther8": 0,
                "tbMDOther9": 0,
                "tbMDOther10": 0,
                "tbMDOther11": 0,
                "tbMDOther12": 0,
                "tbMDName": "auto-created for tbID cross ref: 529",
                "tbMDCustomerID": "44",
                "tbMDAzureID": 0,
                "tbMDNameShort": 0,
                "tbMDProjectNumber": 0,
                "tbMDDescription": 0,
                "tbMDNotes": 0,
                "tbMDExtLink1": 0,
                "tbMDExtSystemID1": 0,
                "tbMDSortOrder": 0,
                "tbMDtbLastModified": 0,
                "tbMDPriority": 0,
                "tbMDStatus": 0,
                "tbMDState": 0,
                "tbMDSeverity": 0,
                "tbMDStage": 0,
                "tbMDPhase": 0,
                "tbMDCategory": 0,
                "tbMDHealth": 0,
                "tbMDResponsibility": 0,
                "tbMDDepartment": 0,
                "tbMDExSponsor": 0,
                "tbMDPM": 0,
                "tbMDProjectType": 0,
                "tbMDShowIn": 0,
                "tbMDYesNoSelector": 0,
                "tbMDProduct": 0,
                "tbMDContact": 0,
                "tbMDWBS": 0,
                "tbMDWeighting": 0,
                "tbMDLocation": 0,
                "tbMDPrimarySkill": 0,
                "tbMDPrimaryRole": 0,
                "tbMDOther2": 0,
                "tbMDOther3": 0,
                "tbMDOther4": 0,
                "canvasNo": 1,
                "tbMDRefID": "a573"
            }],
        "tbResources": [
            {
                "tbResID": "700",
                "tbResName": "Joe Invent",
                "tbResNameShort": "Joe Invent",
                "tbResPrimarySkill": "Inventing",
                "tbResPrimaryRole": "Product Owner/Sponsor",
                "tbResDepartment": "Office of CEO",
                "tbResManager": "Mario CEO",
                "tbResPayRate": "50",
                "tbResTeam": "Ateam",
                "tbResLocation": "Ottawa",
                "tbResTeamLeader": "NA",
                "tbResSupervisor": "NA",
                "tbResPartTimeFullTime": "Full",
                "tbResResourceType": "Human",
                "tbResResourceClass": "Internal",
                "tbResResourceCalendar": "8",
                "tbResPercentGeneralAvailability": "50",
                "tbResCostCode": "A404",
                "tbResCustomerID": "44",
                "tbResExtSystemResID": "t504",
                "tbResSortOrder": "1",
                "tbResLastModified": "7/4/2018",
                "tbResAzureID": "",
                "tbResLabourType": "xrt",
                "tbResCoordTop": "10",
                "tbResCoordLeft": "6",
                "tbResEmail": "a1",
                "tbResPasswordHash": "a1",
                "tbResPIN": "a1"
            }, {
                "tbResID": "704",
                "tbResName": "Jack Project",
                "tbResNameShort": "Jack Project",
                "tbResPrimarySkill": "Project Management",
                "tbResPrimaryRole": "SME PM",
                "tbResDepartment": "Office of CEO",
                "tbResManager": "Mario CEO",
                "tbResPayRate": "100",
                "tbResTeam": "Bteam",
                "tbResLocation": "Ottawa",
                "tbResTeamLeader": "NA",
                "tbResSupervisor": "NA",
                "tbResPartTimeFullTime": "Full",
                "tbResResourceType": "Human",
                "tbResResourceClass": "Internal",
                "tbResResourceCalendar": "4",
                "tbResPercentGeneralAvailability": "50",
                "tbResCostCode": "A408",
                "tbResCustomerID": "44",
                "tbResExtSystemResID": "t508",
                "tbResSortOrder": "5",
                "tbResLastModified": "7/8/2018",
                "tbResAzureID": "",
                "tbResLabourType": "xrt",
                "tbResCoordTop": "10",
                "tbResCoordLeft": "6",
                "tbResEmail": "a1",
                "tbResPasswordHash": "a1",
                "tbResPIN": "a1"
            }, {
                "tbResID": "708",
                "tbResName": "Sam Coder",
                "tbResNameShort": "Sam Coder",
                "tbResPrimarySkill": "Coder",
                "tbResPrimaryRole": "Developer General",
                "tbResDepartment": "IT Dept",
                "tbResManager": "Mr IT",
                "tbResPayRate": "75",
                "tbResTeam": "Bteam",
                "tbResLocation": "Ottawa",
                "tbResTeamLeader": "NA",
                "tbResSupervisor": "NA",
                "tbResPartTimeFullTime": "Full",
                "tbResResourceType": "Human",
                "tbResResourceClass": "Internal",
                "tbResResourceCalendar": "7.5",
                "tbResPercentGeneralAvailability": "25",
                "tbResCostCode": "A411",
                "tbResCustomerID": "44",
                "tbResExtSystemResID": "t511",
                "tbResSortOrder": "8",
                "tbResLastModified": "7/11/2018",
                "tbResAzureID": "",
                "tbResLabourType": "xrt",
                "tbResCoordTop": "10",
                "tbResCoordLeft": "6",
                "tbResEmail": "a1",
                "tbResPasswordHash": "a1",
                "tbResPIN": "a1"
            }, {
                "tbResID": "709",
                "tbResName": "Jay Script",
                "tbResNameShort": "Jay Script",
                "tbResPrimarySkill": "HTML 5 CSS Javascript",
                "tbResPrimaryRole": "Developer Web",
                "tbResDepartment": "IT Dept",
                "tbResManager": "Mr IT",
                "tbResPayRate": "100",
                "tbResTeam": "Ateam",
                "tbResLocation": "Ottawa",
                "tbResTeamLeader": "NA",
                "tbResSupervisor": "NA",
                "tbResPartTimeFullTime": "Full",
                "tbResResourceType": "Human",
                "tbResResourceClass": "Internal",
                "tbResResourceCalendar": "7.5",
                "tbResPercentGeneralAvailability": "25",
                "tbResCostCode": "A411",
                "tbResCustomerID": "44",
                "tbResExtSystemResID": "t511",
                "tbResSortOrder": "8",
                "tbResLastModified": "7/11/2018",
                "tbResAzureID": "",
                "tbResLabourType": "xrt",
                "tbResCoordTop": "10",
                "tbResCoordLeft": "6",
                "tbResEmail": "a1",
                "tbResPasswordHash": "a1",
                "tbResPIN": "a1"
            }, {
                "tbResID": "719",
                "tbResName": "Chris Java",
                "tbResNameShort": "Chris Java",
                "tbResPrimarySkill": "HTML 5 CSS Javascript",
                "tbResPrimaryRole": "Developer Web",
                "tbResDepartment": "IT Dept",
                "tbResManager": "Mr IT",
                "tbResPayRate": "50",
                "tbResTeam": "Ateam",
                "tbResLocation": "Ottawa",
                "tbResTeamLeader": "NA",
                "tbResSupervisor": "NA",
                "tbResPartTimeFullTime": "PartTime",
                "tbResResourceType": "Human",
                "tbResResourceClass": "Contract",
                "tbResResourceCalendar": "7.5",
                "tbResPercentGeneralAvailability": "25",
                "tbResCostCode": "A411",
                "tbResCustomerID": "44",
                "tbResExtSystemResID": "t511",
                "tbResSortOrder": "8",
                "tbResLastModified": "7/11/2018",
                "tbResAzureID": "",
                "tbResLabourType": "xrt",
                "tbResCoordTop": "10",
                "tbResCoordLeft": "6",
                "tbResEmail": "a1",
                "tbResPasswordHash": "a1",
                "tbResPIN": "a1"
            }, {
                "tbResID": "800",
                "tbResName": "Product Owner",
                "tbResNameShort": "Product Owner/Sponsor",
                "tbResPrimarySkill": "Inventing",
                "tbResPrimaryRole": "Product Owner/Sponsor",
                "tbResDepartment": "Office of CEO",
                "tbResManager": "Mario CEO",
                "tbResPayRate": "75",
                "tbResTeam": "Bteam",
                "tbResLocation": "Ottawa",
                "tbResTeamLeader": "NA",
                "tbResSupervisor": "NA",
                "tbResPartTimeFullTime": "Generic",
                "tbResResourceType": "Generic",
                "tbResResourceClass": "NA",
                "tbResResourceCalendar": "7.5",
                "tbResPercentGeneralAvailability": "25",
                "tbResCostCode": "A411",
                "tbResCustomerID": "44",
                "tbResExtSystemResID": "t511",
                "tbResSortOrder": "8",
                "tbResLastModified": "7/11/2018",
                "tbResAzureID": "",
                "tbResLabourType": "xrt",
                "tbResCoordTop": "10",
                "tbResCoordLeft": "6",
                "tbResEmail": "a1",
                "tbResPasswordHash": "a1",
                "tbResPIN": "a1"
            }
        ],
        "tbBaseline": [
            {
                "tbSelfKey2": "BL01:525:b",
                "tbName": "BL01c: My New Program/Project",
                "tbType": "Project",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "107",
                "tbCoordLeft": "210",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "BL01:526:b"

            }, {
                "tbSelfKey2": "BL01:526:b",
                "tbName": "BL01c: My New Task",
                "tbType": "Task",
                "tbL1": "My Portfolio",
                "tbL2": "My Project or Program",
                "tbL3": "My SubProject or Project if program above it",
                "tbL4": "My Task or my SubProject if program above",
                "tbL5": "My Allocation or task if program above",
                "tbStart": "6-Jan-2020",
                "tbFinish": "13-Jan-2020",
                "tbDuration": "5",
                "tbCoordTop": "107",
                "tbCoordLeft": "210",
                "tbResID": "",
                "tbCalendar": "0",
                "tbPercentTimeOn": "0",
                "tbCustomerID": "44",
                "tbRemainingDuration": "5",
                "tbOwner": 0,
                "tbMetaDataID": 0,
                "tbBLID": 0,
                "tbPredecessor": 0,
                "tbAStart": "",
                "tbAFinish": "",
                "tbWork": "0",
                "tbAWork": "",
                "tbWorkRemaining": "0",
                "tbPercentComplete": "",
                "tbConstraintType": 0,
                "tbConstraintDate": 0,
                "tbFloat": 0,
                "tbFreeFloat": 0,
                "tbExpHoursPerWeek": 0,
                "tbCostID": 0,
                "tbCost": 0,
                "tbCostRemaining": 0,
                "tbACost": 0,
                "tbPayRate": "0",
                "tbCostType": 0,
                "canvasNo": 1,
                "tbAzureID": 0,
                "kbCoordTop": "",
                "kbCoordLeft": "",
                "tbHierarchyOrder": 1,
                "focdItemCoordLeft": "1",
                "focdItemCoordTop": "1",
                "tbBarColor": "1",
                "tbTextColor": "1",
                "tbID": "BL01:527:b"
            }
        ],
        "tbDocuments": [
            { "tbDocID": "10", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 4 Project charter and project management plan (PMP)", "tbDocNameShort": "Gate 4 Project charter and project management plan (PMP)", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "Mike Ba", "tbDocBriefDescription": "To address business case unknowns. A complete project charter, high-level PMP, and definition of the business solution are prerequisites for this gate.", "tbDocCustomerID": "44", "tbDocType": "HTML", "tbDocHTMLContent": "<h1>Purpose: Gate 4 Project charter and PMP<\/h1>\r\n<p>To address business case unknowns. A complete project charter, high-level PMP, and definition of the business solution are prerequisites for this gate. Any business case unknowns should be resolved here (or a plan in place to resolve them). Project cost and schedule estimates for the entire project should be 25%.<\/p>\r\n<h1>Review issues<\/h1>\r\n<ul>\r\n<li>Validation that the project charter has addressed all issues critical for a successful project<\/li>\r\n<li>Confirmation that proper project governance, planning, and management are in place<\/li>\r\n<\/ul>\r\n<h1>Why<\/h1>\r\n<ul>\r\n<li>To ensure that all necessary ingredients for successful execution are in place prior to construction and implementation<\/li>\r\n<li>To reduce the risk of having to introduce quick fixes later<\/li>\r\n<\/ul>\r\n<h1>Core review items for this gate<\/h1>\r\n<ol>\r\n<li>Reconfirmation of business case, particularly feasibility of realizing outcomes, assumptions, constraints, and dependencies:<\/li>\r\n<\/ol>\r\n<p>Refinement of estimates and estimating assumptions<\/p>\r\n<ol start=\"2\">\r\n<li>Reconfirmation of readiness to undertake the project<\/li>\r\n<li>Completeness of the project charter:<\/li>\r\n<\/ol>\r\n<p>Absolute clarity of definition, and what is in and out of project scope<\/p>\r\n<p>Clarity of roles, project sponsor, stakeholders, and governance<\/p>\r\n<p>Documented definition of success, business outcomes and how they will be measured, and clarity of accountability for attaining the business outcomes Summary of approach, with a focus on roles, governance, and risks ? Identification of risks and assessment of whether risks are contained well enough to proceed 4. Definition of business solution:<\/p>\r\n<p>Business architecture or model<\/p>\r\n<p>Solution description, including high-level program design and business model; business transformation strategy and business process re-engineering strategy, if applicable&nbsp;<\/p>\r\n<p>High-level functional requirements, and technical and performance requirements&nbsp;<\/p>\r\n<p>High-level data model and data considerations<\/p>\r\n<p>High-level functional design and concept of operations ? Commercial off-the-shelf options assessment, if applicable<\/p>\r\n<ol start=\"5\">\r\n<li>High-level PMP: ? Elaboration of the project plan (work breakdown structure) and link to estimated costs and schedule \\Requirements-gathering workshop<\/li>\r\n<\/ol>\r\n<p>Procurement plan Is it achievable in project time?&nbsp;<\/p>\r\n<p>Business change management strategy and ability to be absorbed by organization&nbsp;<\/p>\r\n<p>Business deployment strategy, including data issues<\/p>\r\n<ol start=\"6\">\r\n<li>Skeleton PMO in place, suitable project manager identified, and key project staffing initiated<\/li>\r\n<\/ol>\r\n<h1>Supporting item(s)<\/h1>\r\n<ol>\r\n<li>Updated plan and estimate (10%) for tasks and level of effort to next project gate<\/li>\r\n<\/ol>\r\n<p>&nbsp; A Guide to Project Gating for IT-Enabled Projects<\/p>\r\n<h1>Typical input to the review<\/h1>\r\n<p>Updated business case, Project charter standard, Preliminary PMP, Solution description<\/p>\r\n<h1>Review format<\/h1>\r\n<p>Full review for larger projects or quick review as considered appropriate for smaller, low-risk initiatives<\/p>\r\n<p>20<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "0", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "22", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 5 Detailed project plan and functional specifications", "tbDocNameShort": "Gate 5 Detailed project plan and functional specifications", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "To confirm the completeness and feasibility of the detailed project plan and definition of requirements.", "tbDocCustomerID": "44", "tbDocType": "HTML", "tbDocHTMLContent": "<h1>Purpose: Gate 5 Detailed project plan and functional specifications<\/h1>\r\n<p><strong>To answer the key questions, How?<\/strong>&nbsp;The most feasible project options are considered here and the preferred option recommended. The project approach is now fully articulated. A high-level project plan has been tabled, upon which preliminary costing is based. The business case should include an investment rationale and describe outcomes to be achieved, as well as their justification relative to the proposed cost. Project cost and schedule estimates for the entire project should be in the 40% range. (Ranges assume a typical development or integration project where there are many unknowns at the early stages. An infrastructure replacement project might have a much smaller estimating range.)<\/p>\r\n<h1>Review issues<\/h1>\r\n<p>Assurance that the business case is thorough, complete, and compelling<\/p>\r\n<p>Confirmation that the organization is ready to undertake the project<\/p>\r\n<h1>Why<\/h1>\r\n<p>To confirm that the business case is sufficiently compelling to justify, sustain, and guide the project<\/p>\r\n<p>To identify any shortcomings in readiness for action before approval and confirm that key identified risks can be managed<\/p>\r\n<h1>Core review items for this gate<\/h1>\r\n<ol>\r\n<li>Business case requirements:<\/li>\r\n<\/ol>\r\n<ul>\r\n<li>Clarity of business problem statement<\/li>\r\n<li>Clarity and precision of project goals must be sufficiently clear to provide focussed outcomes, and guide what is in and out of project scope<\/li>\r\n<li>Clear business justification for the project investment, including how goals can be quantified and measured, and their attainment confirmed<\/li>\r\n<li>Reconfirmation of the alignment of the project with organization s goals<\/li>\r\n<li>Thoroughness of options analysis, including reasonableness of costs and benefits for each option and reasonableness of selected option<\/li>\r\n<li>Indicative cost estimate and schedule<\/li>\r\n<li>Estimating methodology, basis for assumptions, and sensitivity analysis<\/li>\r\n<\/ul>\r\n<ol start=\"2\">\r\n<li>Organizational readiness to undertake the project:<\/li>\r\n<\/ol>\r\n<p>Arrangements decided upon Project Management Office (PMO), strategies for outcome management, performance management, and risk management<\/p>\r\n<ul>\r\n<li>Initial project planning under way and mechanisms in place<\/li>\r\n<li>Business requirements approach fully defined<\/li>\r\n<li>Preparation of environment in which to run project<\/li>\r\n<li>Assessment of organizational capacity relative to project difficulty (OPMCA)<\/li>\r\n<li>Evidence of intent and ability to create an environment for project success<\/li>\r\n<li>A Guide to Project Gating for IT-Enabled Projects 17<\/li>\r\n<\/ul>\r\n<h1>Supporting item(s)<\/h1>\r\n<ol>\r\n<li>Complete definition of the project:<\/li>\r\n<\/ol>\r\n<ul>\r\n<li>Project complexity and risk levels, scope, size, and packaging<\/li>\r\n<li>Risks, constraints, and dependencies (PCRA)<\/li>\r\n<li>Architecture and technological alignment (within federal government and department)<\/li>\r\n<li>Privacy issues (Privacy Impact Assessment, or PIA), security issues (Threat and Risk Assessment, or TRA), and other policy issues<\/li>\r\n<\/ul>\r\n<ol start=\"2\">\r\n<li>Updated plan and estimate (10%) for tasks, level of effort to next project gate<\/li>\r\n<li>PCRA and OPMCA<\/li>\r\n<\/ol>\r\n<h1>Typical input to the review<\/h1>\r\n<p>Business case standard, and detailed supporting analysis and documentation<\/p>\r\n<p>PCRA<\/p>\r\n<p>OPMCA<\/p>\r\n<p>The Rubik's Cube the most popular puzzle. Learn the <a href=\"https:\/\/ruwix.com\/the-rubiks-cube\/how-to-solve-the-rubiks-cube-beginners-method\/\" target=\"_blank\">easiest solution here<\/a>.<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "1", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "3", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 3 Business case and general readiness", "tbDocNameShort": "Gate 3 Business case and general readiness", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "To answer the key question: How?", "tbDocCustomerID": "44", "tbDocType": "HTML", "tbDocHTMLContent": "<h1>Purpose Gate 3 Business case and general readiness:<\/h1>\r\n<p><strong>To answer the key question How?<\/strong>&nbsp;The most feasible project options are considered here and the preferred option recommended. The project approach is now fully articulated. A high-level project plan has been tabled, upon which preliminary costing is based. The business case should include an investment rationale and describe outcomes to be achieved, as well as their justification relative to the proposed cost. Project cost and schedule estimates for the entire project should be in the 40% range. (Ranges assume a typical development or integration project where there are many unknowns at the early stages. An infrastructure replacement project might have a much smaller estimating range.)<\/p>\r\n<h1>Review issues<\/h1>\r\n<p>Assurance that the business case is thorough, complete, and compelling<\/p>\r\n<p>Confirmation that the organization is ready to undertake the project<\/p>\r\n<h1>Why<\/h1>\r\n<p>To confirm that the business case is sufficiently compelling to justify, sustain, and guide the project<\/p>\r\n<p>To identify any shortcomings in readiness for action before approval and confirm that key identified risks can be managed<\/p>\r\n<h1>Core review items for this gate<\/h1>\r\n<ol>\r\n<li>Business case requirements:<\/li>\r\n<\/ol>\r\n<ul>\r\n<li>Clarity of business problem statement<\/li>\r\n<li>Clarity and precision of project goals must be sufficiently clear to provide focussed outcomes, and guide what is in and out of project scope<\/li>\r\n<li>Clear business justification for the project investment, including how goals can be quantified and measured, and their attainment confirmed<\/li>\r\n<li>Reconfirmation of the alignment of the project with organizations goals<\/li>\r\n<li>Thoroughness of options analysis, including reasonableness of costs and benefits for each option and reasonableness of selected option<\/li>\r\n<li>Indicative cost estimate and schedule<\/li>\r\n<li>Estimating methodology, basis for assumptions, and sensitivity analysis<\/li>\r\n<\/ul>\r\n<ol start=\"2\">\r\n<li>Organizational readiness to undertake the project:<\/li>\r\n<\/ol>\r\n<ul>\r\n<li>Arrangements decided upon Project Management Office (PMO), strategies for outcome management, performance management, and risk management<\/li>\r\n<li>Initial project planning under way and mechanisms in place<\/li>\r\n<li>Business requirements approach fully defined<\/li>\r\n<li>Preparation of environment in which to run project<\/li>\r\n<li>Assessment of organizational capacity relative to project difficulty (OPMCA)<\/li>\r\n<li>Evidence of intent and ability to create an environment for project success<\/li>\r\n<li>A Guide to Project Gating for IT-Enabled Projects 17<\/li>\r\n<\/ul>\r\n<h1>Supporting item(s)<\/h1>\r\n<ol>\r\n<li>Complete definition of the project:<\/li>\r\n<\/ol>\r\n<ul>\r\n<li>Project complexity and risk levels, scope, size, and packaging<\/li>\r\n<li>Risks, constraints, and dependencies (PCRA)<\/li>\r\n<li>Architecture and technological alignment (within federal government and department)<\/li>\r\n<li>Privacy issues (Privacy Impact Assessment, or PIA), security issues (Threat and Risk Assessment, or TRA), and other policy issues<\/li>\r\n<\/ul>\r\n<ol start=\"2\">\r\n<li>Updated plan and estimate (10%) for tasks, level of effort to next project gate<\/li>\r\n<li>PCRA and OPMCA<\/li>\r\n<\/ol>\r\n<h1>Typical input to the review<\/h1>\r\n<p>Business case standard, and detailed supporting analysis and documentation<\/p>\r\n<p>PCRA<\/p>\r\n<p>OPMCA<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "3", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "4", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 6 Construction complete and deployment readiness", "tbDocNameShort": "Gate 6 Construction complete and deployment readiness", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "Mike Ba", "tbDocBriefDescription": "To verify that the system under development is ready for implementation and that the project is fully prepared for a successful deployment.", "tbDocCustomerID": "44", "tbDocType": "HTML", "tbDocHTMLContent": "<h1>Purpose: Gate 6 Construction complete and deployment readiness<\/h1>\r\n<p>To verify that the system under development is ready for implementation and that the project is fully prepared for a successful deployment. This gate represents a major point of approval for business readiness. There may be only one or a number of sub-gates and sub-gate reviews related to construction and deployment. (Other gateway approaches reviewed tended to treat construction or construction and deployment as one gate. This is perhaps because the projects under consideration were small or because there is an assumption that construction and deployment are well understood and have relatively low risk.) Based on the given situation and the degree of risk, the department should decide on the number, timing, and focus area for intermediate Gate 6 reviews during construction and deployment. For example, depending on the project structure, a gate could be established at the completion of a major development release or at the completion of a major rollout and deployment to a specified set of users. (See Notes on Gate 6 )<\/p>\r\n<h1>Review issues<\/h1>\r\n<p>Verification that construction is complete with user acceptance testing and migration to production firmly based on meeting acceptance criteria that support proceeding with deployment ? Confirm that deployment teams have been established and are prepared to manage a smooth transition<\/p>\r\n<h1>Why<\/h1>\r\n<p>To ensure the project is ready to proceed with deployment, and that system migration plans and ongoing support are in place<\/p>\r\n<p>Core review items for this gate<\/p>\r\n<ol>\r\n<li>Reconfirmation that the project is aligned with departmental goals, the business case is valid, and expected outcomes will occur<\/li>\r\n<li>Construction complete and deliverables, including all documentation, accepted<\/li>\r\n<li>System migrated to production, and user acceptance testing complete<\/li>\r\n<li>Business change management, deployment, training, data migration, and conversion plans complete<\/li>\r\n<li>System migration plan validated<\/li>\r\n<li>Ongoing support and service management plans in place<\/li>\r\n<li>Vulnerability assessment complete<\/li>\r\n<li>Overall business and project readiness<\/li>\r\n<\/ol>\r\n<h1>Supporting item(s)<\/h1>\r\n<ol>\r\n<li>Updated plan and estimate (10%) for tasks and level of effort to project close-out<\/li>\r\n<\/ol>\r\n<h1>Typical input to the review<\/h1>\r\n<p>All sign-offs for user acceptance, production acceptance by operations, security certification and accreditation to go into production, maintenance team acceptance of documentation ? Deployment, training, data migration and system migration plans, and vulnerability assessment approved<\/p>\r\n<p>Support and service management plan&nbsp;<\/p>\r\n<p>Disaster recovery and business resumption plans<\/p>\r\n<h1>Review format<\/h1>\r\n<p>Quick or full review, depending on project size, risk, and complexity<\/p>\r\n<h1>Notes on Gate 6<\/h1>\r\n<p>Projects may adopt a variety of phasing approaches, and not all phasing approaches will be sequential. Some may be structured with parallel phases, making the choice of gates and the scope of gate reviews more subjective. In the case of large iterative development methodologies, gates and review points would ideally be established to ensure that development is moving toward closure that the iterations would not continue indefinitely or until the project runs out of time and money. The focus, then, is on the sign-offs against what has been delivered and on the clear agreement on what remains to be done. Examples of intermediate gates within the purview of Gate 6 include:<\/p>\r\n<p>Construction release completion In a project that is structured to have multiple major releases, it is recommended that a gate be established at the completion of one or more of these releases.<\/p>\r\n<p>Mid-phase health check A health check could be established in the middle of a project phase even if no major deliverables have been completed or decision points reached. The decision to do so might be based on some combination of dollars spent (e.g., $25 million), time elapsed (e.g., one year), and rate of expenditure (e.g., $2 million per month).<\/p>\r\n<p>Construction and deployment readiness In many projects, deployment activities and construction actually occur in parallel. In some cases, deployment occurs after the completion of construction. Depending on the nature and size of the project, a gate might be established to create a decision point about whether or not to deploy.&nbsp;<\/p>\r\n<p>Pilot deployment and full deployment readiness In some projects, a pilot deployment occurs immediately following the construction phase as a basis for deciding whether the system is ready for general deployment. This might be an appropriate point at which to establish a gate, depending on the size and nature of the project.<\/p>\r\n<p>24<\/p>\r\n<p>The Rubik's Cube seems to be an impossible puzzle but it's <a href=\"https:\/\/ruwix.com\/the-rubiks-cube\/how-to-solve-the-rubiks-cube-beginners-method\/\" rel=\"nofollow noopener\">easy to solve<\/a> using just a few algorithms.<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "4", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "5", "tbDocName": "Gate 7 Post-implementation review", "tbDocNameShort": "Gate 7 Post-implementation review", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money.", "tbDocCustomerID": "44", "tbDocType": "HTML", "tbDocHTMLContent": "<h1>Purpose Gate 7 Post-implementation review<\/h1>\r\n<p>To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money. This gate typically occurs approximately six months following project completion. A review at this point can also catalogue the lessons learned during the project those identified by the project group and those captured by independent reviewers.<\/p>\r\n<h1>Review issues<\/h1>\r\n<p>Verify that the project was completed as planned and that the expected business outcomes were actually realized ? Assess the degree of success for the transition to an ongoing service&nbsp;<\/p>\r\n<h1>Why<\/h1>\r\n<p>To confirm success from a project delivery and business perspective ? To determine what lessons might benefit the department and the broader community in future undertakings<\/p>\r\n<h1>Core review items for this gate<\/h1>\r\n<ol>\r\n<li>Project delivery measured against original objectives<\/li>\r\n<li>Confirmation of the archiving of information and deliverables, as applicable<\/li>\r\n<li>Knowledge transfer and transition to successful service<\/li>\r\n<li>Completion of contractual obligations<\/li>\r\n<li>Validation of business outcomes<\/li>\r\n<li>Capture of lessons learned, including review process<\/li>\r\n<li>Project close-out report completed<\/li>\r\n<\/ol>\r\n<h1>Typical input to the review<\/h1>\r\n<ul>\r\n<li>Project close-out report<\/li>\r\n<li>Business outcomes measurement plan<\/li>\r\n<li>Contract acceptance reports<\/li>\r\n<li>Project lessons learned<\/li>\r\n<\/ul>\r\n<h1>Review format<\/h1>\r\n<p>Workshop to full review, depending on project<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "5", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "27", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 1 Strategic assessment and concept", "tbDocNameShort": "Gate 1 Strategic assessment and concept", "tbDocStatus": "Approved", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "To answer the key questions What do we want to do? and Why?", "tbDocCustomerID": "44", "tbDocType": "HTML", "tbDocHTMLContent": "<h1>Purpose Gate 1 Strategic assessment and concept<\/h1>\r\n<p>To answer the key questions What do we want to do? and Why? The objectives at this early stage are to test the wisdom and appropriateness of the proposed undertaking, and to ensure that key stakeholders are identified, and that everyone understands what is to be done and why. A half- to one-day workshop session is typical, preceded by reading of any available early project documents. The Gate 1 review seeks to arm the review sponsor with considerations that should be addressed before the next phase of the project and, in some cases, may actually dictate going back to the drawing board before proceeding further.<\/p>\r\n<h1>Review issues<\/h1>\r\n<ul><li>Validation of the rationale for the project<\/li><li> Confirmation that underlying fundamentals make sense&nbsp;<\/li><li> Assessment that the project is doable as proposed<\/li><\/ul>\r\n<h1>Why<\/h1>\r\n<p>To eliminate ideas that do not make sense or will prove to be impossible to execute<\/p>\r\n<h1>Core review items for this gate<\/h1>\r\n<ol>\r\n<li>Wisdom and appropriateness of proposed undertaking<\/li>\r\n<li>Articulation of the business problem and validity of the business imperative<\/li>\r\n<li>Definition and boundaries of scope<\/li>\r\n<li>Definition and measures of success<\/li>\r\n<li>Degree of common understanding about the proposal among parties involved<\/li>\r\n<li>Identification of stakeholders and extent of support and commitment for the initiative<\/li>\r\n<li>Confirmation that the project makes sense in the context of the departmental project portfolio and federal government priorities&nbsp;<\/li>\r\n<\/ol>\r\n<h1>Supporting item(s)<\/h1>\r\n<ol>\r\n<li>Plan and estimate (10%) for tasks and level of effort to next project gate<\/li>\r\n<\/ol>\r\n<h1>Typical input to the review<\/h1>\r\n<p>The project group provides reviewers with an understanding of why the project is being proposed, what it is intended to accomplish, and how it is defined. Supporting information to position the full context of the proposal might include reference to departmental reports and plans as well as an overview of the departments main business lines.<\/p>\r\n<p>The following areas need to be addressed:<\/p>\r\n<ol>\r\n<li>The concept and imperative of the project, including identification of the project sponsor, the business problem statement in the context of the overall business strategy, the broad scope of the project, expected general business outcomes and indicators of success, key stakeholders, general business risks, approximate sizing of the project, and critical success factors.<\/li>\r\n<li>The alignment of the project proposal with departmental Program Activity Architecture, program(s) delivery, departmental plans and priorities, broader federal government or cross-departmental goals, as appropriate, and positioning of the project in the context of the departmental information management and IT portfolio (including reference to departmental architectures). The positioning might also reference TBS-sponsored shared or common services and cluster initiatives, if relevant.<\/li>\r\n<li>Review format Workshop review (conducting a few targeted interviews may be necessary)<\/li>\r\n<\/ol>\"", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "6", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807430", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 2 Business case and general readiness", "tbDocNameShort": "Gate 2 Business case and general readiness", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose&nbsp;Gate 2 Business case and general readiness</p><p>To confirm that the business case is sufficiently compelling to justify, sustain and guide the project; to identify any shortcomings in readiness for action before approval; and to confirm that key identified risks can be managed.</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "7", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "20180745", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 1 Project approach", "tbDocNameShort": "Gate 1 Project approach", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "To confirm that the approach selected to address the business problem or opportunity is both feasible and appropriate.", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose&nbsp;Gate 1 Project approach</p><p>To confirm that the approach selected to address the business problem or opportunity is both feasible and appropriate.</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "8", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807434", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 3 Pre-Construction", "tbDocNameShort": "Gate 3 Pre-Construction", "tbDocStatus": "In Progress", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "9", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807435", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 4 Pre-Deployment", "tbDocNameShort": "Gate 4 Pre-Deployment", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose:&nbsp;&nbsp;Gate 4 Pre-Deployment</p><p>To ensure that the resources, support and governance necessary for successful execution are in place prior to construction</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "10", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807436", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 5 Post Implementation", "tbDocNameShort": "Gate 5 Post Implementation", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose:&nbsp;Gate 5 Post Implementation</p><p>To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money.</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "11", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807437", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 1 Business Case", "tbDocNameShort": "Gate 1 Business Case", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose&nbsp;Gate 1 Business Case</p><p>To confirm that the business case is sufficiently compelling to justify, sustain and guide the project; to identify any shortcomings in readiness for action before approval; and to confirm that key identified risks can be managed.</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "12", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807417", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 2 Pre-Construction", "tbDocNameShort": "Gate 2 Pre-Construction", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose:&nbsp;Gate 2 Pre-Construction</p><p>To ensure that the resources, support and governance necessary for successful execution are in place prior to deployment</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "13", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807419", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Gate 3 Post-implementation review", "tbDocNameShort": "Gate 3 Post-implementation review", "tbDocStatus": "First Draft", "tbDocPurpose": "Gate Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p>Purpose:&nbsp;Gate 3 Post-implementation review</p><p>To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money.</p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "14", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807140", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Marketing Site Main Page", "tbDocNameShort": "edit me", "tbDocStatus": "First Draft", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "15", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071515", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Why use Timebars web page", "tbDocNameShort": "edit me", "tbDocStatus": "First Draft", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<h3>Why choose to use Timebars?<br><\/h3>\r\n                \r\n<p>                    \r\n\r\nTo transform and simplify how you manage work with a new approach on modern computer screens. To allow you to manage your people and their commitments your way and still respecting the Enterprise systems. We need better and faster scheduling tools to graphically visualize commitments on a time scale. We cannot afford to keep using Excel and maintaining duplicate data any longer. And we should be constrained to limited screen real estate of typical PC monitors of today.<\/p><p><img src=\"images\/timebarsbigscreen.png\" style=\"width: 668px;\"><br><\/p><p> <\/p>\r\n\r\n<p> \r\nThe above graphic illustrates my people working on Projects, Tasks and Software releases and I can schedule and track the work with my finger on a touch screen or with a mouse. <\/p>\r\n              \r\n\r\n                <a id=\"HotNewTools\"><\/a>\r\n                <p>Timebars is a \"no installation\", browser based dynamic web page for tracking who is working on what project and who is available for other work. <\/p><p>Using visual drag and canvasDrop gestures, you can move work allocations across the time scale and level over allocated resources.<\/p>\r\n\r\n                <p>\r\n\r\n                    \r\n                <\/p>\r\n\r\n                <p class=\"comments align-left\">\r\n                    You no longer have to depend on the IT Department, with Timebars you only need a\r\n                    web browser and an internet connection.\r\n                <\/p>\r\n\r\n\r\n\r\n            \r\n\r\n            <div id=\"sidebar\">\r\n\r\n                <h4>Wise Words<\/h4>\r\n                <p>\r\n                    \"Criticism is something you can avoid easily by saying nothing,\r\n                    doing nothing, and being nothing\"<\/p><h3>Modern Technology<\/h3><p>\r\n                <\/p>\r\n\r\n\r\n            <\/div>\r\n\r\n\r\n<p>ECMAScript, often referred to as JavaScript, is a memory-safe programming language that all websites and web applications depend upon. JavaScript is what turns a static page into a dynamic one (i.e. allowing it to perform dynamic computations). It has been a fast-growing language ever since its inception, and is now the prefered language amongst developers. ECMAScript has been thrust forward by active 2 superset languages, pushing its evolution ahead of other programming languages.<\/p>\r\n<p>JavaScript Object Notation (JSON) is an open-standard file format using serialized objects. It has become the standard for browser and server communication. It is also used to translate data to other programming languages.<\/p>\r\n\r\n<p>No Web Assembly required. Web Assemly is heralded as the next step for general public compute intensive web applications. By allowing performance-specific tasks to be hard-typed and compiled ahead of time, developers benefit from the convenience and performance of both high and low level languages, respectively.<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "16", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071558", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "How to Purchase", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "                    <h1>How to Purchase<\/h1>\r\n\r\n                    <p>\r\n                        Use your credit card to get started <br>\r\n                        Coming soon...\r\n                    <\/p>  ", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "17", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807150", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "What Can I use it for web page", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<p><br></p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "18", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "201807151", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Licensing", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "Licensing", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<h1>Licensing<\/h1>\r\n<p>We plan to charge monthly subscription fee.<\/p>\r\n<p>We have a free version but does not allow saving your data.<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "19", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071656", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Demos", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<h3>Demos<\/h3>\r\n<a href=\"http:\/\/res.cloudinary.com\/dbdibjgrn\/video\/upload\/v1532633134\/Timebars%20Demos\/Album_Medium.mp4\">Timebars Launch Demo<\/a> <br>\r\n<a href=\"http:\/\/res.cloudinary.com\/dbdibjgrn\/video\/upload\/v1525521858\/tbvideo_custom.mp4\">See Timebars live on 80 inch screen<\/a>\r\n", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "20", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071657", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Feedback", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "                    <h1>Feedback<\/h1>\r\n\r\n                    <p>\r\nPlease send us an email with any feedback to help us improve our product.\r\n                    <\/p>  ", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "21", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071658", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Returning Users", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "                <a id=\"ReturningUsers\"><\/a>\r\n                <h1>Returning Users<\/h1>\r\n\r\n  <p>\r\n                <a href=\"\/Canvas.html\">Launch Timebars! (July 2018 Release)<\/a>\r\n\r\n               \r\n\r\n                <\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "22", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071629", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "New Users", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "\r\n        <!-- content-wrap starts here -->\r\n        <div id=\"content-wrap\">\r\n\r\n            <div id=\"main\">\r\n\r\n\r\n                <a id=\"NewUsers\"><\/a>\r\n                <h1>New Users<\/h1>\r\n\r\n                <p>\r\n                    It is easy to get started and use Timebars, it takes less than a minute and no software installation\r\n                    is necessary because Timebars is simply a web page. You don't install web pages! We use browser cached only. <\/p><p>To get started, you navigate to <a href=\"http:\/\/www.timebars.com\" style=\"background-color: rgb(255, 255, 255);\">www.timebars.com<\/a>, click on Help button at the top and and follow instructions on the Help page to load demo data.<\/p>\r\n                \r\n                <p><span style=\"color: inherit; font-family: inherit; font-size: 36px;\">What is Browser Cache?<\/span><br><\/p>\r\n                \r\n\r\n\r\n                <p><a href=\"https:\/\/developer.mozilla.org\/en-US\/docs\/Web\/API\/IndexedDB_API\/Basic_Concepts_Behind_IndexedDB\">Mozilla<\/a> describes it as follows: Is a way for you to persistently store data inside a user's browser. Because it lets you create web applications with rich query abilities regardless of network availability, your applications can work both online and offline. The IndexedDB component is useful for browser based applications that store a large amount of data (for example, a list of tasks on a project), and applications that don't need persistent internet connectivity to operate.<\/p>\r\n                \r\n                                <p>\r\n                \r\n                Wikipedia describes it as follows: IndexedDB is a transnational database embedded in the browser. The database is organised around the concept of collections of JSON objects similarly to NoSQL databases MongoDB or CouchDB. Each object is identified with a key generated during insert. An indexation system allows to optimize access to objects. Optionally see&nbsp;<a href=\"https:\/\/www.w3.org\/TR\/IndexedDB\/\" style=\"background-color: rgb(255, 255, 255);\">W3C HTML 5 IndexedDB<\/a>&nbsp;for details of the modern browser cache.<\/p><\/div><div id=\"sidebar\">\r\n\r\n\r\n\r\n            <\/div>\r\n\r\n            <!-- content-wrap ends here -->\r\n        <\/div>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "23", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018071652", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "How to return", "tbDocNameShort": "edit me", "tbDocStatus": "edit me", "tbDocPurpose": "Marketing Page", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "                    <h1>How to Return<\/h1>\r\n\r\n                    <p>\r\n                        There would be nothing to return because there is nothing installed and nothing resides on our servers. <br>\r\n                        If you no longer wish to pay for Timebars, send us an email and we will stop billing your credit card.\r\n                    <\/p>  ", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "24", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018072422", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Getting Started Help Page", "tbDocNameShort": "Getting Started", "tbDocStatus": "First Draft", "tbDocPurpose": "Timebar Help", "tbDocWrittenBy": "Mike Ba", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<ol><li>To get started, click menu items Admin &gt; DB-Admin &gt; Go<\/li><li>Look for \"Success\" in red text top left, if successful, Click Go-Timebars!<\/li><li>You should see the timescale Canvas and demo timebars! Drag bars to make start and finish date changes.<\/li><li>Click \"Show Menu\" on top left of Canvas. Use this to filter timebars for specific projects in Portfolios.<\/li><li>Hover over the Menu items, click on a Portfolio, Project or Sub-Project to display filtered Timebars.<\/li><li>Close the Menu, Click on a Timebar title to see cost, schedule and metadata information in Side Panel.<\/li><li>To edit the Timebar Title, begin typing in the Side Panel<\/li><li>To change cost and schedule data, move, stretch or shrink bars to suit.<\/li><li>To change Timescale and scheduling settings, click \"Settings button\".<\/li><\/ol>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "25", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018072440", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Getting Started Help Page 2", "tbDocNameShort": "Back up and Restore", "tbDocStatus": "edit me", "tbDocPurpose": "Timebar Help", "tbDocWrittenBy": "Joe Ba", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<h3>Make a Backup<\/h3>\r\n<p>Click the \"Data\" button on main menu, click Backup Now! This will save the data within text files to a destination of your choosing. The following files are saved<\/p>\r\n<ol>\r\n<li>tbTimebars.txt (Timebars cost and schedule Data File)<\/li>\r\n<li>tbMetaData.txt (Timebars Metadata Data File)<\/li>\r\n<li>tbResources.txt (Resources Data File)<\/li>\r\n<li>tbTags.txt (Tagging Data File)<\/li>\r\n<li>tbResCalcs.txt (Resource Calculations Data File)<\/li>\r\n<li>tbConfigData.txt (Admin Panel Confi Data File)<\/li><\/ol><p><br><\/p><ol>\r\n<\/ol>\r\n<h3>How do I populate the system with my Project data?<\/h3>\r\n<div>\r\n<p>Your data must be within a text file in JSON format with matching field names. Use the downloaded files to determine format.<\/p>\r\nTo upload your data, drag the text file onto the Canvas, Timebars will perform a backup then delete existing data and push the data from your text file into Timebars.\r\n<ol>\r\n<li>Timebars Data File<\/li>\r\n<li>Metadata Data File<\/li>\r\n<li>Resources Data File<\/li>\r\n<li>Tagging Data File<\/li>\r\n<li>Resource Calculations Data File<\/li>\r\n<li>Admin Panel Confi Data File<\/li>\r\n<\/ol><\/div>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "26", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018072447", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Kanban help page 1", "tbDocNameShort": "Getting Started", "tbDocStatus": "First Draft", "tbDocPurpose": "Kanban Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<ul>\r\n<li> To get started, click menu item DB-Admin &gt; Go. <\/li>\r\n<li> Click Go-TbKanban to load up Kanban board. <\/li>\r\n<li> The black round icons indicate sub-projects, click on a subproject \"Title\" to launch Kanban\r\nbars. <\/li>\r\n<li> To create a new Kanbar Bar, drag and canvasDrop the sub-project title onto the Canvas. <\/li>\r\n<li> The bar will be in edit mode, change data to suit. When done adding items, click refresh.<\/li>\r\n<li> Move bars around to suit, to edit click Edit Mode button on menu.<\/li>\r\n<\/ul>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "27", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" },
            { "tbDocID": "2018072436", "tbDocL1": "xxx", "tbDocL2": "xxx", "tbDocL3": "xxx", "tbDocL4": "xxx", "tbDocL5": "xxx", "tbDocCardImage": "xxx", "tbDocName": "Common Help Page", "tbDocNameShort": "Common help topics", "tbDocStatus": "First Draft", "tbDocPurpose": "Common Help", "tbDocWrittenBy": "edit me", "tbDocBriefDescription": "edit me", "tbDocCustomerID": "44", "tbDocType": "edit me", "tbDocHTMLContent": "<h1>Estimating Process<\/h1>\r\n<p>For complex or sizeable projects, five categories of estimates are prepared. The process begins with the development of an initial estimate that is further developed during the early phases of the project. It is recommended that Cost Planners be involved in preparing overall project.<\/p>\r\n<h3><strong>Broad Cost Projection<\/strong><\/h3>\r\n<p>Based on historical data from similar projects, indicates a budget for resources to develop a project as well whether or not total project costs are expected to exceed $1&nbsp;million. This is not a construction estimate.<\/p>\r\n<h3><strong>Class&nbsp;'D' (Indicative) Estimate<\/strong><\/h3>\r\n<p>To be in unit cost analysis format (such as cost per m\u00C2\u00B2 or other measurement unit) based upon a comprehensive list of project requirements (i.e. scope) and assumptions; the Class D estimate is evolved throughout the phases of the Project Identification Stage, finally being incorporated into the cash flows in the Analysis Phase; for more complex projects such as laboratories, elemental cost analysis and the input of specific disciplines may be required;&nbsp;<em>the Class D Indicative estimates developed during the National Project Management System (NPMS) Feasibility Phase shall be revisited with cost planners in the Analysis Phase before finalizing.<\/em><\/p>\r\n<h3><strong>Class&nbsp;'C' Estimate<\/strong><\/h3>\r\n<p>To be in elemental cost analysis format latest edition issued by the Canadian Institute of Quantity Surveyors and based on a comprehensive list of requirements and assumptions, including a full description of the preferred schematic design option, construction\/design experience, and market conditions;&nbsp;<em>Class C estimates are developed during the&nbsp;NPMS&nbsp;Design Phase<\/em><\/p>\r\n<h3><strong>Class&nbsp;'B' (Substantive) Estimate<\/strong><\/h3>\r\n<p>To be in elemental cost analysis format latest edition issued by the Canadian Institute of Quantity Surveyors and based on design development drawings and outline specifications, which include the design of all major systems and subsystems, as well as the results of all site\/installation investigations;&nbsp;<em>Class B estimates are developed during the&nbsp;NPMS&nbsp;Design Phase;<\/em><\/p>\r\n<h3><strong>Class&nbsp;'A' (Pre-Tender) Estimate<\/strong><\/h3>\r\n<p>To be in both elemental cost analysis format as well as trade divisional format latest edition issued by the Canadian Institute of Quantity Surveyors and based on completed construction drawings and specifications prepared prior to calling competitive tenders. The Class 'A' Estimate is generally expected to be within 5% to 10% of the actual contract award price for new construction. Tendering risks should be included in the project risk plan and costed accordingly. The accuracy of Class 'A' estimates can be influenced by many factors, including complexity of project, volatile market, remote locations, tight schedules, and unclear contract documents;&nbsp;<em>Class 'A' estimates are prepared during the&nbsp;NPMS&nbsp;Implementation Phase and can be a more accurate Substantive Estimate, depending on the complexity of the project;<\/em><\/p>\r\n<p>Note: from Public Services and Procurement Canada<\/p>", "tbDocGroup": "edit me", "tbDocOwner": "edit me", "tbDocPopular": "edit me", "tbDocSortOrder": "28", "tbDocLastModified": "", "tbDocApprovedBy": "edit me" }
        ],
        "tbTags": [
            {
                "tbTagID": "2",
                "tbTagGroup": "Business Advisors",
                "tbTagName": "Joe Ba",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Joe Ba",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "3",
                "tbTagGroup": "Business Advisors",
                "tbTagName": "Sally Ba",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Sally Ba",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "4",
                "tbTagGroup": "Business Owners",
                "tbTagName": "Joe Bo",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Joe Bo",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "5",
                "tbTagGroup": "Business Owners",
                "tbTagName": "Sam bo",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Sam bo",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "6",
                "tbTagGroup": "Business Owners",
                "tbTagName": "Julia bo",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Julia bo",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "7",
                "tbTagGroup": "Calendar",
                "tbTagName": "8",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "8",
                "tbTagPurpose": "defining working calendar in hours per day on this Timebar ",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "8",
                "tbTagGroup": "Calendar",
                "tbTagName": "7.5",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "7.5",
                "tbTagPurpose": "defining working calendar in hours per day on this Timebar ",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "9",
                "tbTagGroup": "Category",
                "tbTagName": "Risk-based",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Risk-based",
                "tbTagPurpose": "use for any means to categorize timebars for custom reporting",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "10",
                "tbTagGroup": "Category",
                "tbTagName": "Operational",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Operational",
                "tbTagPurpose": "use for any means to categorize timebars for custom reporting",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "11",
                "tbTagGroup": "Category",
                "tbTagName": "Corporate",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Corporate",
                "tbTagPurpose": "use for any means to categorize timebars for custom reporting",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "12",
                "tbTagGroup": "Category",
                "tbTagName": "Core",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Core",
                "tbTagPurpose": "use for any means to categorize timebars for custom reporting",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020"
            }, {
                "tbTagID": "13",
                "tbTagGroup": "Contact",
                "tbTagName": "Jim",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Jim",
                "tbTagPurpose": "Person name and address to contact for questions regarding Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 13
            }, {
                "tbTagID": "14",
                "tbTagGroup": "Contact",
                "tbTagName": "George",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "George",
                "tbTagPurpose": "Person name and address to contact for questions regarding Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 14
            }, {
                "tbTagID": "15",
                "tbTagGroup": "Cost Code",
                "tbTagName": "A5645",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "A5645",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 15
            }, {
                "tbTagID": "16",
                "tbTagGroup": "Delivery Managers",
                "tbTagName": "Jim Dm",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Jim Dm",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 16
            }, {
                "tbTagID": "17",
                "tbTagGroup": "Delivery Managers",
                "tbTagName": "Al dm",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Al dm",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 17
            }, {
                "tbTagID": "18",
                "tbTagGroup": "Department",
                "tbTagName": "Sales and Marketing",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Marketing",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 18
            }, {
                "tbTagID": "19",
                "tbTagGroup": "Department",
                "tbTagName": "Engineering",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Engineering",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 19
            }, {
                "tbTagID": "20",
                "tbTagGroup": "Department",
                "tbTagName": "Manufacturing",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "BTS",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 20
            }, {
                "tbTagID": "21",
                "tbTagGroup": "Department",
                "tbTagName": "Procurement",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Insurance",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 21
            }, {
                "tbTagID": "22",
                "tbTagGroup": "Department",
                "tbTagName": "Business Development",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "BD",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 22
            }, {
                "tbTagID": "23",
                "tbTagGroup": "Department",
                "tbTagName": "Research and Development",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "S&!",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 23
            }, {
                "tbTagID": "24",
                "tbTagGroup": "Department",
                "tbTagName": "Enterprise Services",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "ERM",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 24
            }, {
                "tbTagID": "25",
                "tbTagGroup": "Department",
                "tbTagName": "Finance",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Finance",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 25
            }, {
                "tbTagID": "26",
                "tbTagGroup": "Department",
                "tbTagName": "Human Resources",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "HR",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 26
            }, {
                "tbTagID": "27",
                "tbTagGroup": "Department",
                "tbTagName": "Product Management",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "F&I",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 27
            }, {
                "tbTagID": "28",
                "tbTagGroup": "Department",
                "tbTagName": "Information Technology",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "CA",
                "tbTagPurpose": "Define what area of the business is responsible for Timebar",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 28
            }, {
                "tbTagID": "29",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Project Charter",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 29
            }, {
                "tbTagID": "30",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Business Case",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 30
            }, {
                "tbTagID": "31",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Approval Document",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 31
            }, {
                "tbTagID": "32",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Gate Help",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Key for the Help pages on Gating forms",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 32
            }, {
                "tbTagID": "33",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Timebar Help",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Key for pages appearing on help accordions",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 33
            }, {
                "tbTagID": "34",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Main Page",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "To identify web pages in marketing site",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 34
            }, {
                "tbTagID": "35",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Marketing Page",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "To identify web pages in marketing site",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 35
            }, {
                "tbTagID": "36",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Contact Us Page",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "To identify web pages in marketing site",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 36
            }, {
                "tbTagID": "37",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Kanban Help",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Key for pages appearing on help accordions",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 37
            }, {
                "tbTagID": "38",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "Common Help",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Key for pages appearing on help accordions",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 38
            }, {
                "tbTagID": "39",
                "tbTagGroup": "Document Purpose",
                "tbTagName": "General Web Page",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "To identify web pages",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 39
            }, {
                "tbTagID": "40",
                "tbTagGroup": "Document Status",
                "tbTagName": "Approved",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 40
            }, {
                "tbTagID": "41",
                "tbTagGroup": "Document Status",
                "tbTagName": "First Draft",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 41
            }, {
                "tbTagID": "42",
                "tbTagGroup": "Document Status",
                "tbTagName": "In Progress",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 42
            }, {
                "tbTagID": "43",
                "tbTagGroup": "Document Status",
                "tbTagName": "Canceled",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 43
            }, {
                "tbTagID": "44",
                "tbTagGroup": "Document Type",
                "tbTagName": "HTML",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 44
            }, {
                "tbTagID": "45",
                "tbTagGroup": "Document Type",
                "tbTagName": "MS Word",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 45
            }, {
                "tbTagID": "46",
                "tbTagGroup": "Document Type",
                "tbTagName": "PDF",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging documents",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 46
            }, {
                "tbTagID": "47",
                "tbTagGroup": "Estimation Class",
                "tbTagName": "Class A",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Class A",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 47
            }, {
                "tbTagID": "48",
                "tbTagGroup": "Estimation Class",
                "tbTagName": "Class B",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Class B",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 48
            }, {
                "tbTagID": "49",
                "tbTagGroup": "Estimation Class",
                "tbTagName": "Class C",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Class C",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 49
            }, {
                "tbTagID": "50",
                "tbTagGroup": "ExpectedHoursPerWeek",
                "tbTagName": "5",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "5",
                "tbTagPurpose": "Use for estimating work needs",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 50
            }, {
                "tbTagID": "51",
                "tbTagGroup": "ExpectedHoursPerWeek",
                "tbTagName": "10",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "10",
                "tbTagPurpose": "Use for estimating work needs",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 51
            }, {
                "tbTagID": "52",
                "tbTagGroup": "ExSponsor",
                "tbTagName": "Ally",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Ally",
                "tbTagPurpose": "List out your Executive Sponsors",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 52
            }, {
                "tbTagID": "53",
                "tbTagGroup": "ExSponsor",
                "tbTagName": "Mike",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Mike",
                "tbTagPurpose": "List out your Executive Sponsors",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 53
            }, {
                "tbTagID": "54",
                "tbTagGroup": "ExtSystemResID",
                "tbTagName": "456M23",
                "tbTagEntity": "ResList",
                "tbTagPopular": "General",
                "tbTagNameShort": "456M23",
                "tbTagPurpose": "Use to tie into exsiting systems in the organization",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 54
            }, {
                "tbTagID": "55",
                "tbTagGroup": "ExtSystemResID",
                "tbTagName": "456M24",
                "tbTagEntity": "ResList",
                "tbTagPopular": "General",
                "tbTagNameShort": "456M24",
                "tbTagPurpose": "Use to tie into exsiting systems in the organization",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 55
            }, {
                "tbTagID": "56",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 1 Strategic assessment and concept",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Concept",
                "tbTagPurpose": "To confirm the project's objectives, both what is to be done and why; and to identify key stakeholders.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 56
            }, {
                "tbTagID": "57",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 2 Project approach",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Approach",
                "tbTagPurpose": "To confirm that the approach selected to address the business problem or opportunity is both feasible and appropriate.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 57
            }, {
                "tbTagID": "58",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 3 Business case and general readiness",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Case",
                "tbTagPurpose": "To confirm that the business case is sufficiently compelling to justify, sustain and guide the project; to identify any shortcomings in readiness for action before approval; and to confirm that key identified risks can be managed.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 58
            }, {
                "tbTagID": "59",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 4 Project charter and project management plan (PMP)",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Project Charter",
                "tbTagPurpose": "To ensure that the resources, support and governance necessary for successful execution are in place prior to construction and implementation.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 59
            }, {
                "tbTagID": "60",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 5 Detailed project plan and functional specifications",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Detail Plan",
                "tbTagPurpose": "To confirm the completeness and feasibility of the detailed project plan; and to confirm the definition of requirements.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 60
            }, {
                "tbTagID": "61",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 6 Construction complete and deployment readiness",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Construction&Deployment",
                "tbTagPurpose": "To confirm the project is fully prepared for successful deployment.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 61
            }, {
                "tbTagID": "62",
                "tbTagGroup": "Gates Large",
                "tbTagName": "Gate 7 Post-implementation review",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Post Implementation",
                "tbTagPurpose": "To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 62
            }, {
                "tbTagID": "63",
                "tbTagGroup": "Gates Medium",
                "tbTagName": "Gate 1 Project approach",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Approach",
                "tbTagPurpose": "To confirm that the approach selected to address the business problem or opportunity is both feasible and appropriate.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 63
            }, {
                "tbTagID": "64",
                "tbTagGroup": "Gates Medium",
                "tbTagName": "Gate 2 Business case and general readiness",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Case",
                "tbTagPurpose": "To confirm that the business case is sufficiently compelling to justify, sustain and guide the project; to identify any shortcomings in readiness for action before approval; and to confirm that key identified risks can be managed.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 64
            }, {
                "tbTagID": "65",
                "tbTagGroup": "Gates Medium",
                "tbTagName": "Gate 3 Pre-Construction",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Pre-Construction",
                "tbTagPurpose": "To ensure that the resources, support and governance necessary for successful execution are in place prior to deployment",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 65
            }, {
                "tbTagID": "66",
                "tbTagGroup": "Gates Medium",
                "tbTagName": "Gate 4 Pre-Deployment",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Pre-Deployment",
                "tbTagPurpose": "To ensure that the resources, support and governance necessary for successful execution are in place prior to construction",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 66
            }, {
                "tbTagID": "67",
                "tbTagGroup": "Gates Medium",
                "tbTagName": "Gate 5 Post Implementation",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Post Implementation",
                "tbTagPurpose": "To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 67
            }, {
                "tbTagID": "68",
                "tbTagGroup": "Gates Release",
                "tbTagName": "1 Approved to Design",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "1 Approved to Design",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 68
            }, {
                "tbTagID": "69",
                "tbTagGroup": "Gates Release",
                "tbTagName": "2 Approved to Develop",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "2 Approved to Develop",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 69
            }, {
                "tbTagID": "70",
                "tbTagGroup": "Gates Release",
                "tbTagName": "3 Approved to Implement",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "3 Approved to Implement",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 70
            }, {
                "tbTagID": "71",
                "tbTagGroup": "Gates Small",
                "tbTagName": "Gate 1 Business Case",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Case",
                "tbTagPurpose": "To confirm that the business case is sufficiently compelling to justify, sustain and guide the project; to identify any shortcomings in readiness for action before approval; and to confirm that key identified risks can be managed.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 71
            }, {
                "tbTagID": "72",
                "tbTagGroup": "Gates Small",
                "tbTagName": "Gate 2 Pre-Construction",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Pre-Construction",
                "tbTagPurpose": "To ensure that the resources, support and governance necessary for successful execution are in place prior to deployment",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 72
            }, {
                "tbTagID": "73",
                "tbTagGroup": "Gates Small",
                "tbTagName": "Gate 3 Post-implementation review",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Post Implementation",
                "tbTagPurpose": "To confirm completion, assess the extent to which the project has achieved its goals, and provide an assessment of value for money.",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 73
            }, {
                "tbTagID": "74",
                "tbTagGroup": "Health",
                "tbTagName": "On schedule",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "On schedule",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 74
            }, {
                "tbTagID": "75",
                "tbTagGroup": "Health",
                "tbTagName": "Early",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Early",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 75
            }, {
                "tbTagID": "76",
                "tbTagGroup": "Health",
                "tbTagName": "Slipping",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Slipping",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 76
            }, {
                "tbTagID": "77",
                "tbTagGroup": "Health",
                "tbTagName": "Late",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Late",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 77
            }, {
                "tbTagID": "78",
                "tbTagGroup": "Health",
                "tbTagName": "Blocked",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Blocked",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 78
            }, {
                "tbTagID": "79",
                "tbTagGroup": "Health",
                "tbTagName": "Completed",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Completed",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 79
            }, {
                "tbTagID": "80",
                "tbTagGroup": "Health",
                "tbTagName": "Attention",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Attention",
                "tbTagPurpose": "For Dashboard indicators",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 80
            }, {
                "tbTagID": "81",
                "tbTagGroup": "Health Indicators",
                "tbTagName": "Green",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Green",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 81
            }, {
                "tbTagID": "82",
                "tbTagGroup": "Health Indicators",
                "tbTagName": "Yellow",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Yellow",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 82
            }, {
                "tbTagID": "83",
                "tbTagGroup": "Health Indicators",
                "tbTagName": "Red",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Red",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 83
            }, {
                "tbTagID": "84",
                "tbTagGroup": "HyperLinks",
                "tbTagName": "Link 1",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "General",
                "tbTagNameShort": "Link 1",
                "tbTagPurpose": "for manually storing hyperlink to external info.",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 84
            }, {
                "tbTagID": "85",
                "tbTagGroup": "HyperLinks",
                "tbTagName": "Link 2",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "General",
                "tbTagNameShort": "Link 2",
                "tbTagPurpose": "for manually storing hyperlink to external info.",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 85
            }, {
                "tbTagID": "86",
                "tbTagGroup": "Investment Category",
                "tbTagName": "IT Platform",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "IT Platform",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 86
            }, {
                "tbTagID": "87",
                "tbTagGroup": "Investment Category",
                "tbTagName": "Office Environment",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Office Environment",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 87
            }, {
                "tbTagID": "88",
                "tbTagGroup": "Investment Category",
                "tbTagName": "Business Support",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Support",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 88
            }, {
                "tbTagID": "89",
                "tbTagGroup": "Investment Category",
                "tbTagName": "Engineering Tools",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Engineering Tools",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 89
            }, {
                "tbTagID": "90",
                "tbTagGroup": "Investment Category",
                "tbTagName": "Core Products",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Core Products",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 90
            }, {
                "tbTagID": "91",
                "tbTagGroup": "Investment Category",
                "tbTagName": "Innovation",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Innovation",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 91
            }, {
                "tbTagID": "92",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Improve product reliability",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Improve product reliability",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 92
            }, {
                "tbTagID": "93",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Cross-sell our Network Device products",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Cross-sell our Network Device products",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 93
            }, {
                "tbTagID": "94",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Engineering Tool Innovation",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Engineering Tool Innovation",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 94
            }, {
                "tbTagID": "95",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Faster response to new business opportunities",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Faster response to new business opportunities",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 95
            }, {
                "tbTagID": "96",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Source Cloud services for our Core products",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Source Cloud services for our Core products",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 96
            }, {
                "tbTagID": "97",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Support moden digital workplace",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Support moden digital workplace",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 97
            }, {
                "tbTagID": "98",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Improve Employee Skills",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Improve Employee Skills",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 98
            }, {
                "tbTagID": "99",
                "tbTagGroup": "Investment Initiative",
                "tbTagName": "Improve productivity with Agile Teams",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Improve productivity with Agile Teams",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 99
            }, {
                "tbTagID": "100",
                "tbTagGroup": "Investment Objective",
                "tbTagName": "Run the Business",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Run the Business",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 100
            }, {
                "tbTagID": "101",
                "tbTagGroup": "Investment Objective",
                "tbTagName": "Grow the Business",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Grow the Business",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 101
            }, {
                "tbTagID": "102",
                "tbTagGroup": "Investment Objective",
                "tbTagName": "Transform the Business",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Transform the Business",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 102
            }, {
                "tbTagID": "103",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Server more customers",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Server more customers",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 103
            }, {
                "tbTagID": "104",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Best in market core products",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Best in market core products",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 104
            }, {
                "tbTagID": "105",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Increase revenue",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Increase revenue",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 105
            }, {
                "tbTagID": "106",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Increase share of market",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Increase share of market",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 106
            }, {
                "tbTagID": "107",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Ensure compliance",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Ensure compliance",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 107
            }, {
                "tbTagID": "108",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Attract and retain the best people",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Attract and retain the best people",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 108
            }, {
                "tbTagID": "109",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Improve and maintain workplace safety",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Improve and maintain workplace safety",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 109
            }, {
                "tbTagID": "110",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Reduce waste",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Reduce waste",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 110
            }, {
                "tbTagID": "111",
                "tbTagGroup": "Investment Strategy",
                "tbTagName": "Improve customer service",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Improve customer service",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 111
            }, {
                "tbTagID": "112",
                "tbTagGroup": "L1",
                "tbTagName": "TopLevel",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "General",
                "tbTagNameShort": "TopLevel",
                "tbTagPurpose": "Can use to make custom hierarchy",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 112
            }, {
                "tbTagID": "113",
                "tbTagGroup": "L2",
                "tbTagName": "Next Level Down",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "General",
                "tbTagNameShort": "Next Level Down",
                "tbTagPurpose": "Can use to make custom hierarchy",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 113
            }, {
                "tbTagID": "114",
                "tbTagGroup": "L3",
                "tbTagName": "Third Level Down",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "General",
                "tbTagNameShort": "Third Level Down",
                "tbTagPurpose": "Can use to make custom hierarchy",
                "tbTagDescription": "NA",
                "tbTagOwner": "System",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 114
            }, {
                "tbTagID": "115",
                "tbTagGroup": "Labour Type",
                "tbTagName": "External",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "External",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 115
            }, {
                "tbTagID": "116",
                "tbTagGroup": "Labour Type",
                "tbTagName": "Internal",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Internal",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 116
            }, {
                "tbTagID": "117",
                "tbTagGroup": "Labour Type",
                "tbTagName": "Contract",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Contract",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 117
            }, {
                "tbTagID": "118",
                "tbTagGroup": "Lifecycle Phase",
                "tbTagName": "1 Pre-execution (evaluation and selection)",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "1 Pre-execution (evaluation and selection)",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 118
            }, {
                "tbTagID": "119",
                "tbTagGroup": "Lifecycle Phase",
                "tbTagName": "2 Execution (gating approvals)",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "2 Execution (gating approvals)",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 119
            }, {
                "tbTagID": "120",
                "tbTagGroup": "Lifecycle Phase",
                "tbTagName": "3 Closing (outcome analysis)",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "3 Closing (outcome analysis)",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 120
            }, {
                "tbTagID": "121",
                "tbTagGroup": "Location",
                "tbTagName": "Ottawa",
                "tbTagEntity": "General",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Ottawa",
                "tbTagPurpose": "Where is resource located",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 121
            }, {
                "tbTagID": "122",
                "tbTagGroup": "Location",
                "tbTagName": "Gatineau",
                "tbTagEntity": "General",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Gatineau",
                "tbTagPurpose": "Where is resource located",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 122
            }, {
                "tbTagID": "123",
                "tbTagGroup": "Non Project Tag",
                "tbTagName": "No",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "No",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 123
            }, {
                "tbTagID": "124",
                "tbTagGroup": "Non Project Tag",
                "tbTagName": "Yes",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Yes",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 124
            }, {
                "tbTagID": "125",
                "tbTagGroup": "OOM Estimates",
                "tbTagName": "0 to 100K",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "0 to 100K",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 125
            }, {
                "tbTagID": "126",
                "tbTagGroup": "OOM Estimates",
                "tbTagName": "1001K to 5000K",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "1001K to 5000K",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 126
            }, {
                "tbTagID": "127",
                "tbTagGroup": "OOM Estimates",
                "tbTagName": "101K to 300K",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "101K to 300K",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 127
            }, {
                "tbTagID": "128",
                "tbTagGroup": "OOM Estimates",
                "tbTagName": "301K to 1000K",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "301K to 1000K",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 128
            }, {
                "tbTagID": "129",
                "tbTagGroup": "OOM Estimates",
                "tbTagName": "1000K Plus",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "1000K Plus",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 129
            }, {
                "tbTagID": "130",
                "tbTagGroup": "Organizational Manager",
                "tbTagName": "Kent Om",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Kent Om",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 130
            }, {
                "tbTagID": "131",
                "tbTagGroup": "Organizational Manager",
                "tbTagName": "Perry Om",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Perry Om",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 131
            }, {
                "tbTagID": "132",
                "tbTagGroup": "Parttime Fulltime",
                "tbTagName": "Full Time",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Full Time",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 132
            }, {
                "tbTagID": "133",
                "tbTagGroup": "Parttime Fulltime",
                "tbTagName": "Part Time",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Part Time",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 133
            }, {
                "tbTagID": "134",
                "tbTagGroup": "PartTimeFullTime",
                "tbTagName": "FullTime",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "FullTime",
                "tbTagPurpose": "Resource is a full time person or a part time person such as contractor",
                "tbTagDescription": "NA",
                "tbTagOwner": "HR",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 134
            }, {
                "tbTagID": "135",
                "tbTagGroup": "PartTimeFullTime",
                "tbTagName": "PartTime",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "PartTime",
                "tbTagPurpose": "Resource is a full time person or a part time person such as contractor",
                "tbTagDescription": "NA",
                "tbTagOwner": "HR",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 135
            }, {
                "tbTagID": "136",
                "tbTagGroup": "PartTimeFullTime",
                "tbTagName": "Unknown",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Unknown",
                "tbTagPurpose": "Resource is a full time person or a part time person such as contractor",
                "tbTagDescription": "NA",
                "tbTagOwner": "HR",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 136
            }, {
                "tbTagID": "137",
                "tbTagGroup": "PercentTimeAllocated",
                "tbTagName": "10",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "10",
                "tbTagPurpose": "Main tool to use to estimate timebar work",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 137
            }, {
                "tbTagID": "138",
                "tbTagGroup": "PercentTimeAllocated",
                "tbTagName": "25",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "25",
                "tbTagPurpose": "Main tool to use to estimate timebar work",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 138
            }, {
                "tbTagID": "139",
                "tbTagGroup": "PercentTimeAllocated",
                "tbTagName": "50",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "50",
                "tbTagPurpose": "Main tool to use to estimate timebar work",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 139
            }, {
                "tbTagID": "140",
                "tbTagGroup": "PercentTimeAllocated",
                "tbTagName": "75",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "75",
                "tbTagPurpose": "Main tool to use to estimate timebar work",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 140
            }, {
                "tbTagID": "141",
                "tbTagGroup": "PercentTimeAllocated",
                "tbTagName": "100",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "100",
                "tbTagPurpose": "Main tool to use to estimate timebar work",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 141
            }, {
                "tbTagID": "142",
                "tbTagGroup": "PerecentAvailable",
                "tbTagName": "10",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "10",
                "tbTagPurpose": "For taggin an allocation for resource management",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 142
            }, {
                "tbTagID": "143",
                "tbTagGroup": "PerecentAvailable",
                "tbTagName": "25",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "25",
                "tbTagPurpose": "For taggin an allocation for resource management",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 143
            }, {
                "tbTagID": "144",
                "tbTagGroup": "PerecentAvailable",
                "tbTagName": "50",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "50",
                "tbTagPurpose": "For taggin an allocation for resource management",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 144
            }, {
                "tbTagID": "145",
                "tbTagGroup": "PerecentAvailable",
                "tbTagName": "75",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "75",
                "tbTagPurpose": "For taggin an allocation for resource management",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 145
            }, {
                "tbTagID": "146",
                "tbTagGroup": "PerecentAvailable",
                "tbTagName": "100",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "100",
                "tbTagPurpose": "For taggin an allocation for resource management",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 146
            }, {
                "tbTagID": "147",
                "tbTagGroup": "Phase",
                "tbTagName": "Initiating",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Initiating",
                "tbTagPurpose": "Highlevel Phase of Project or Program",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 147
            }, {
                "tbTagID": "148",
                "tbTagGroup": "Phase",
                "tbTagName": "Executing",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Executing",
                "tbTagPurpose": "Highlevel Phase of Project or Program",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 148
            }, {
                "tbTagID": "149",
                "tbTagGroup": "Phase",
                "tbTagName": "Closing",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Closing",
                "tbTagPurpose": "Highlevel Phase of Project or Program",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 149
            }, {
                "tbTagID": "150",
                "tbTagGroup": "Phase",
                "tbTagName": "Planning",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Planning",
                "tbTagPurpose": "Highlevel Phase of Project or Program",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 150
            }, {
                "tbTagID": "151",
                "tbTagGroup": "Portfolios",
                "tbTagName": "R&D New Technology",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "R&D New Technology",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 151
            }, {
                "tbTagID": "152",
                "tbTagGroup": "Portfolios",
                "tbTagName": "Core Product Delivery",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Core Product Delivery",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 152
            }, {
                "tbTagID": "153",
                "tbTagGroup": "Portfolios",
                "tbTagName": "Business Support Systems",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Support Systems",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 153
            }, {
                "tbTagID": "154",
                "tbTagGroup": "Portfolios",
                "tbTagName": "IT Platform",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "IT Platform",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 154
            }, {
                "tbTagID": "155",
                "tbTagGroup": "Portfolios",
                "tbTagName": "Building Services",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Building Services",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 155
            }, {
                "tbTagID": "156",
                "tbTagGroup": "Primary Contact",
                "tbTagName": "Ben Pc",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Ben Pc",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 156
            }, {
                "tbTagID": "157",
                "tbTagGroup": "Primary Contact",
                "tbTagName": "Sam Pc",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Sam Pc",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 157
            }, {
                "tbTagID": "158",
                "tbTagGroup": "Primary Contact",
                "tbTagName": "Dave Pc",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Dave Pc",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 158
            }, {
                "tbTagID": "159",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Finance",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Finance",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 159
            }, {
                "tbTagID": "160",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Research and Development",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Research and Development",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 160
            }, {
                "tbTagID": "161",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Business Development",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Development",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 161
            }, {
                "tbTagID": "162",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Corporate Affairs",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Corporate Affairs",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 162
            }, {
                "tbTagID": "163",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Business Technology Solutions",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Technology Solutions",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 163
            }, {
                "tbTagID": "164",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Engineering and Product Delivery",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Engineering and Product Delivery",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 164
            }, {
                "tbTagID": "165",
                "tbTagGroup": "Primary Line of Business",
                "tbTagName": "Human Resources",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Human Resources",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 165
            }, {
                "tbTagID": "166",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Inventing",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 166
            }, {
                "tbTagID": "167",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "HR Policies",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 167
            }, {
                "tbTagID": "168",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Finance Policies",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 168
            }, {
                "tbTagID": "169",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Procurement",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 169
            }, {
                "tbTagID": "170",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Project Management",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 170
            }, {
                "tbTagID": "171",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "IT Architecture",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 171
            }, {
                "tbTagID": "172",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Azure and AWS",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 172
            }, {
                "tbTagID": "173",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Enterprise Architect",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 173
            }, {
                "tbTagID": "174",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Coder",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 174
            }, {
                "tbTagID": "175",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "HTML 5 CSS Javascript",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 175
            }, {
                "tbTagID": "176",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "MS SQL and Oracle",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 176
            }, {
                "tbTagID": "177",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "SAP",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 177
            }, {
                "tbTagID": "178",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Testing",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 178
            }, {
                "tbTagID": "179",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Testing",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 179
            }, {
                "tbTagID": "180",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Radio Design",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 180
            }, {
                "tbTagID": "181",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Board Design",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 181
            }, {
                "tbTagID": "182",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Antenna Design",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 182
            }, {
                "tbTagID": "183",
                "tbTagGroup": "Primary Skill",
                "tbTagName": "Tower Design",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 183
            }, {
                "tbTagID": "184",
                "tbTagGroup": "Primary Strategic Priority",
                "tbTagName": "High",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Improve intiitative visibility",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 184
            }, {
                "tbTagID": "185",
                "tbTagGroup": "Primary Strategic Priority",
                "tbTagName": "Medium",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Reduce complexity of process",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 185
            }, {
                "tbTagID": "186",
                "tbTagGroup": "Primary Strategic Priority",
                "tbTagName": "Low",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Fewer meetings",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 186
            }, {
                "tbTagID": "187",
                "tbTagGroup": "Priority",
                "tbTagName": "High",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "High",
                "tbTagPurpose": "Set timebar priority",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 187
            }, {
                "tbTagID": "188",
                "tbTagGroup": "Priority",
                "tbTagName": "Medium",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Med",
                "tbTagPurpose": "Set timebar priority",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 188
            }, {
                "tbTagID": "189",
                "tbTagGroup": "Priority",
                "tbTagName": "Low",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Low",
                "tbTagPurpose": "Set timebar priority",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 189
            }, {
                "tbTagID": "190",
                "tbTagGroup": "Product",
                "tbTagName": "ModelA",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "ModelA",
                "tbTagPurpose": "If this timebar is for creating a product, use this tag",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 190
            }, {
                "tbTagID": "191",
                "tbTagGroup": "Product",
                "tbTagName": "ModelT",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "ModelT",
                "tbTagPurpose": "If this timebar is for creating a product, use this tag",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 191
            }, {
                "tbTagID": "192",
                "tbTagGroup": "Program Activity Alignment (PAA)",
                "tbTagName": "1 1 Program: Common One-Benefit service for all Canadians",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 192
            }, {
                "tbTagID": "193",
                "tbTagGroup": "Program Activity Alignment (PAA)",
                "tbTagName": "1 1 1 Sub-Program: One Canada wide monthly payment  to each Canadian",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 193
            }, {
                "tbTagID": "194",
                "tbTagGroup": "Program Activity Alignment (PAA)",
                "tbTagName": "1 1 1 1 Sub-Sub-Program: Banks support of One-Benefit distribution",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 194
            }, {
                "tbTagID": "195",
                "tbTagGroup": "Program Activity Alignment (PAA)",
                "tbTagName": "1 1 1 2 Sub-Sub-Program: Tax System changes to suit new One-Benefit.",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 195
            }, {
                "tbTagID": "196",
                "tbTagGroup": "Programs",
                "tbTagName": "SAP System Upgrade",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "SAP System Upgrade",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 196
            }, {
                "tbTagID": "197",
                "tbTagGroup": "Programs",
                "tbTagName": "Building and Office Modernization",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Building and Office Modernization",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 197
            }, {
                "tbTagID": "198",
                "tbTagGroup": "Programs",
                "tbTagName": "Centralized Billing and Receivables",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Centralized Billing and Receivables",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 198
            }, {
                "tbTagID": "199",
                "tbTagGroup": "Programs",
                "tbTagName": "Continuous Improvement Transformation",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Continuous Improvement Transformation",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 199
            }, {
                "tbTagID": "200",
                "tbTagGroup": "Programs",
                "tbTagName": "ERP Transformation",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "ERP Transformation",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 200
            }, {
                "tbTagID": "201",
                "tbTagGroup": "Programs",
                "tbTagName": "Information Solutions Resiliency",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Information Solutions Resiliency",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 201
            }, {
                "tbTagID": "202",
                "tbTagGroup": "Programs",
                "tbTagName": "SharePoint 2016 Upgrade",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "SharePoint 2016 Upgrade",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 202
            }, {
                "tbTagID": "203",
                "tbTagGroup": "Programs",
                "tbTagName": "Business Continuity Management Program",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Business Continuity Management Program",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 203
            }, {
                "tbTagID": "204",
                "tbTagGroup": "Programs",
                "tbTagName": "TBS Compliance",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "TBS Compliance",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 204
            }, {
                "tbTagID": "205",
                "tbTagGroup": "Programs",
                "tbTagName": "Digital Workplace - Transformation",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Digital Workplace - Transformation",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 205
            }, {
                "tbTagID": "206",
                "tbTagGroup": "Programs",
                "tbTagName": "Ticketing System and Requirements Management",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Ticketing System and Requirements Management",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 206
            }, {
                "tbTagID": "207",
                "tbTagGroup": "Programs",
                "tbTagName": "Continuous Internal Delivery Enablement",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Continuous Internal Delivery Enablement",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 207
            }, {
                "tbTagID": "208",
                "tbTagGroup": "Programs",
                "tbTagName": "HR Service Delivery",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "HR Service Delivery",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 208
            }, {
                "tbTagID": "209",
                "tbTagGroup": "Programs",
                "tbTagName": "Market Information Management",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Market Information Management",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 209
            }, {
                "tbTagID": "210",
                "tbTagGroup": "Programs",
                "tbTagName": "IM Policy and Guidelines",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "IM Policy and Guidelines",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 210
            }, {
                "tbTagID": "211",
                "tbTagGroup": "Programs",
                "tbTagName": "Core Operations Program",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Core Operations Program",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 211
            }, {
                "tbTagID": "212",
                "tbTagGroup": "Programs",
                "tbTagName": "Integration API Development Program",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Integration API Development Program",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 212
            }, {
                "tbTagID": "213",
                "tbTagGroup": "Programs",
                "tbTagName": "NA",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 213
            }, {
                "tbTagID": "214",
                "tbTagGroup": "Programs",
                "tbTagName": "Standalone Project with No Program",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Standalone Project with No Program",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 214
            }, {
                "tbTagID": "215",
                "tbTagGroup": "Programs",
                "tbTagName": "Infrastructure as a Service (IaaS) Program",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Infrastructure as a Service (IaaS) Program",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 215
            }, {
                "tbTagID": "216",
                "tbTagGroup": "Project Managers",
                "tbTagName": "PM011",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "PM011",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 216
            }, {
                "tbTagID": "217",
                "tbTagGroup": "Project Managers",
                "tbTagName": "Jim Pm",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Jim Pm",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 217
            }, {
                "tbTagID": "218",
                "tbTagGroup": "Project Managers",
                "tbTagName": "Al PM",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Al PM",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 218
            }, {
                "tbTagID": "219",
                "tbTagGroup": "Project Status",
                "tbTagName": "Active",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Active",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 219
            }, {
                "tbTagID": "220",
                "tbTagGroup": "Project Status",
                "tbTagName": "On-Hold",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "On-Hold",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 220
            }, {
                "tbTagID": "221",
                "tbTagGroup": "Project Status",
                "tbTagName": "Canceled",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Canceled",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 221
            }, {
                "tbTagID": "222",
                "tbTagGroup": "ProjectManager",
                "tbTagName": "Sam",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Sam",
                "tbTagPurpose": "List out your PM's",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 222
            }, {
                "tbTagID": "223",
                "tbTagGroup": "ProjectManager",
                "tbTagName": "Ralph",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Ralph",
                "tbTagPurpose": "List out your PM's",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 223
            }, {
                "tbTagID": "224",
                "tbTagGroup": "ProjectNumber",
                "tbTagName": "15101",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "0",
                "tbTagPurpose": "Custom numbering scheme specific to user",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 224
            }, {
                "tbTagID": "225",
                "tbTagGroup": "ProjectNumber",
                "tbTagName": "15201",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "0",
                "tbTagPurpose": "Custom numbering scheme specific to user",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 225
            }, {
                "tbTagID": "226",
                "tbTagGroup": "Resource Class",
                "tbTagName": "Contractor",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Contractor",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 226
            }, {
                "tbTagID": "227",
                "tbTagGroup": "Resource Class",
                "tbTagName": "Perm",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Perm",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 227
            }, {
                "tbTagID": "228",
                "tbTagGroup": "Resource Class",
                "tbTagName": "Seasonal",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Seasonal",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 228
            }, {
                "tbTagID": "229",
                "tbTagGroup": "Resource Manager",
                "tbTagName": "Joe Rm",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Joe Rm",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 229
            }, {
                "tbTagID": "230",
                "tbTagGroup": "Resource Manager",
                "tbTagName": "Sally Rm",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Sally Rm",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 230
            }, {
                "tbTagID": "231",
                "tbTagGroup": "Resource Manager",
                "tbTagName": "Al Rm",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Al Rm",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 231
            }, {
                "tbTagID": "232",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Product Owner/Sponsor",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 232
            }, {
                "tbTagID": "233",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME HR",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",
                "id": 233
            }, {
                "tbTagID": "234",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME Finance",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",
                "id": 234
            }, {
                "tbTagID": "235",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME Purchasing",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 235
            }, {
                "tbTagID": "236",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME PM",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 236
            }, {
                "tbTagID": "237",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME IT Platforms and Datacenter",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 237
            }, {
                "tbTagID": "238",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME Cloud Technologies",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 238
            }, {
                "tbTagID": "239",
                "tbTagGroup": "Resource Role",
                "tbTagName": "SME Solution Architecture",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 239
            }, {
                "tbTagID": "240",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Developer General",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 240
            }, {
                "tbTagID": "241",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Developer Web",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 241
            }, {
                "tbTagID": "242",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Developer SQL",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 242
            }, {
                "tbTagID": "243",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Developer ERP System",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 243
            }, {
                "tbTagID": "244",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Tester New Software",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 244
            }, {
                "tbTagID": "245",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Tester Platforms and Datacenter",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 245
            }, {
                "tbTagID": "246",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Engineer RF",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 246
            }, {
                "tbTagID": "247",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Engineer Hardware",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 247
            }, {
                "tbTagID": "248",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Engineer Antennas",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 248
            }, {
                "tbTagID": "249",
                "tbTagGroup": "Resource Role",
                "tbTagName": "Engineer Towers",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "Tag resources for resource demand calculations",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 249
            }, {
                "tbTagID": "250",
                "tbTagGroup": "Resource Supervisor",
                "tbTagName": "Sam Supr",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Sam Supr",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 250
            }, {
                "tbTagID": "251",
                "tbTagGroup": "ResourceCalendar",
                "tbTagName": "8",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "8",
                "tbTagPurpose": "For calculating wok on an Allocation",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 251
            }, {
                "tbTagID": "252",
                "tbTagGroup": "ResourceCalendar",
                "tbTagName": "7.5",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "7.5",
                "tbTagPurpose": "For calculating wok on an Allocation",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 252
            }, {
                "tbTagID": "253",
                "tbTagGroup": "ResourceCalendar",
                "tbTagName": "24",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "24",
                "tbTagPurpose": "For calculating wok on an Allocation",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 253
            }, {
                "tbTagID": "254",
                "tbTagGroup": "Responsibility",
                "tbTagName": "Jim",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Jim",
                "tbTagPurpose": "Define who is responsible for Timebar when in trouble",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 254
            }, {
                "tbTagID": "255",
                "tbTagGroup": "Responsibility",
                "tbTagName": "John",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "John",
                "tbTagPurpose": "Define who is responsible for Timebar when in trouble",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 255
            }, {
                "tbTagID": "256",
                "tbTagGroup": "Responsibility",
                "tbTagName": "Sally",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Sally",
                "tbTagPurpose": "Define who is responsible for Timebar when in trouble",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 256
            }, {
                "tbTagID": "257",
                "tbTagGroup": "Responsible Teams",
                "tbTagName": "Accounting",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Accounting",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 257
            }, {
                "tbTagID": "258",
                "tbTagGroup": "Responsible Teams",
                "tbTagName": "Finance",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Finance",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 258
            }, {
                "tbTagID": "259",
                "tbTagGroup": "Responsible Teams",
                "tbTagName": "Procurement",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Procurement",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 259
            }, {
                "tbTagID": "260",
                "tbTagGroup": "Responsible Teams",
                "tbTagName": "Product Engineering",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Product Engineering",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 260
            }, {
                "tbTagID": "261",
                "tbTagGroup": "Responsible Teams",
                "tbTagName": "Information Technology",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Information Technology",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 261
            }, {
                "tbTagID": "262",
                "tbTagGroup": "Severity",
                "tbTagName": "Critical",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "1",
                "tbTagPurpose": "Indicates how severe a priority itme is, in terms of impact",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 262
            }, {
                "tbTagID": "263",
                "tbTagGroup": "Severity",
                "tbTagName": "Important",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "2",
                "tbTagPurpose": "Indicates how severe a priority itme is, in terms of impact",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 263
            }, {
                "tbTagID": "264",
                "tbTagGroup": "Severity",
                "tbTagName": "Normal",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "3",
                "tbTagPurpose": "Indicates how severe a priority itme is, in terms of impact",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 264
            }, {
                "tbTagID": "265",
                "tbTagGroup": "Show In Reports",
                "tbTagName": "Executive Dashboard",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Executive Dashboard",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 265
            }, {
                "tbTagID": "266",
                "tbTagGroup": "Show In Reports",
                "tbTagName": "PM Dashboard",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "PM Dashboard",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 266
            }, {
                "tbTagID": "267",
                "tbTagGroup": "Show In Reports",
                "tbTagName": "Project Sites",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Project Sites",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 267
            }, {
                "tbTagID": "268",
                "tbTagGroup": "ShowIn",
                "tbTagName": "Dashboard",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Dashboard",
                "tbTagPurpose": "A way to control visibility of Timebar for reporting and display",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 268
            }, {
                "tbTagID": "269",
                "tbTagGroup": "ShowIn",
                "tbTagName": "MilesoneReport",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "MilesoneReport",
                "tbTagPurpose": "A way to control visibility of Timebar for reporting and display",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 269
            }, {
                "tbTagID": "270",
                "tbTagGroup": "ShowIn",
                "tbTagName": "NeverShow",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "NeverShow",
                "tbTagPurpose": "A way to control visibility of Timebar for reporting and display",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 270
            }, {
                "tbTagID": "271",
                "tbTagGroup": "Size",
                "tbTagName": "Small",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Small",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 271
            }, {
                "tbTagID": "272",
                "tbTagGroup": "Size",
                "tbTagName": "Medium",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Medium",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 272
            }, {
                "tbTagID": "273",
                "tbTagGroup": "Size",
                "tbTagName": "Large",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Large",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 273
            }, {
                "tbTagID": "274",
                "tbTagGroup": "Size",
                "tbTagName": "Release",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Release",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 274
            }, {
                "tbTagID": "275",
                "tbTagGroup": "Sponsoring Department",
                "tbTagName": "IT Department",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "IT Department",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 275
            }, {
                "tbTagID": "276",
                "tbTagGroup": "Sponsoring Department",
                "tbTagName": "Customer Support Department",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Customer Support Department",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 276
            }, {
                "tbTagID": "277",
                "tbTagGroup": "Sponsoring Department",
                "tbTagName": "Presidents Office",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Presidents Office",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 277
            }, {
                "tbTagID": "278",
                "tbTagGroup": "Sponsors",
                "tbTagName": "Marsha Sp",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Marsha Sp",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 278
            }, {
                "tbTagID": "279",
                "tbTagGroup": "Sponsors",
                "tbTagName": "Sydney Sp",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Sydney Sp",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 279
            }, {
                "tbTagID": "280",
                "tbTagGroup": "Sponsors",
                "tbTagName": "Colin Sp",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Colin Sp",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 280
            }, {
                "tbTagID": "281",
                "tbTagGroup": "Stage",
                "tbTagName": "1 Proposed",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Proposed",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 281
            }, {
                "tbTagID": "282",
                "tbTagGroup": "Stage",
                "tbTagName": "2 Triaged",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Triaged",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 282
            }, {
                "tbTagID": "283",
                "tbTagGroup": "Stage",
                "tbTagName": "3 Prioritized",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Prioritized",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 283
            }, {
                "tbTagID": "284",
                "tbTagGroup": "Stage",
                "tbTagName": "4 Selected",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Selected",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 284
            }, {
                "tbTagID": "285",
                "tbTagGroup": "Stage",
                "tbTagName": "5 Assigned",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Assigned",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 285
            }, {
                "tbTagID": "286",
                "tbTagGroup": "Stage",
                "tbTagName": "6 Delivering",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Delivering",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 286
            }, {
                "tbTagID": "287",
                "tbTagGroup": "Stage",
                "tbTagName": "7 Delivered",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Delivered",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 287
            }, {
                "tbTagID": "288",
                "tbTagGroup": "Stage",
                "tbTagName": "8 Closed",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Closed",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 288
            }, {
                "tbTagID": "289",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "1 Proposed",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Proposed",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 289
            }, {
                "tbTagID": "290",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "2 Triaged",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Triaged",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 290
            }, {
                "tbTagID": "291",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "3 Prioritized",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Prioritized",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 291
            }, {
                "tbTagID": "292",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "4 Selected",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Selected",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 292
            }, {
                "tbTagID": "293",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "5 Assigned",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Assigned",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 293
            }, {
                "tbTagID": "294",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "6 Delivering",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Delivering",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 294
            }, {
                "tbTagID": "295",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "7 Delivered",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Delivered",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 295
            }, {
                "tbTagID": "296",
                "tbTagGroup": "Stage (Manual)",
                "tbTagName": "8 Closed",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Closed",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 296
            }, {
                "tbTagID": "297",
                "tbTagGroup": "State",
                "tbTagName": "Awaiting Approval",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Awaiting Approval",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 297
            }, {
                "tbTagID": "298",
                "tbTagGroup": "State",
                "tbTagName": "Awaiting Input",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Awaiting Input",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 298
            }, {
                "tbTagID": "299",
                "tbTagGroup": "State",
                "tbTagName": "Requested Approval",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Requested Approval",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 299
            }, {
                "tbTagID": "300",
                "tbTagGroup": "State",
                "tbTagName": "Approved",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Approved",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 300
            }, {
                "tbTagID": "301",
                "tbTagGroup": "State",
                "tbTagName": "Spawned",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Spawned",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 301
            }, {
                "tbTagID": "302",
                "tbTagGroup": "State",
                "tbTagName": "Awaiting Review",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Awaiting Review",
                "tbTagPurpose": "Controlling project lifecycle workflow",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 302
            }, {
                "tbTagID": "303",
                "tbTagGroup": "Status",
                "tbTagName": "Pending",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Not Started",
                "tbTagPurpose": "set Timebar Status",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 303
            }, {
                "tbTagID": "304",
                "tbTagGroup": "Status",
                "tbTagName": "Active",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "In Progress",
                "tbTagPurpose": "set Timebar Status",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 304
            }, {
                "tbTagID": "305",
                "tbTagGroup": "Status",
                "tbTagName": "Deployed",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Finished",
                "tbTagPurpose": "set Timebar Status",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 305
            }, {
                "tbTagID": "306",
                "tbTagGroup": "Status",
                "tbTagName": "Closed",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Closed",
                "tbTagPurpose": "set Timebar Status",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 306
            }, {
                "tbTagID": "307",
                "tbTagGroup": "Status",
                "tbTagName": "Partially Deployed",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Archived",
                "tbTagPurpose": "set Timebar Status",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 307
            }, {
                "tbTagID": "308",
                "tbTagGroup": "Status",
                "tbTagName": "On-Hold",
                "tbTagEntity": "General",
                "tbTagPopular": "Sched",
                "tbTagNameShort": "Archived",
                "tbTagPurpose": "set Timebar Status",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 308
            }, {
                "tbTagID": "309",
                "tbTagGroup": "stdTasks",
                "tbTagName": "Project Start",
                "tbTagEntity": "StdTasks",
                "tbTagPopular": "StdTasks",
                "tbTagNameShort": "Start",
                "tbTagPurpose": "Drag and Drop on Project or Release to create a Standard Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 309
            }, {
                "tbTagID": "310",
                "tbTagGroup": "stdTasks",
                "tbTagName": "Design",
                "tbTagEntity": "StdTasks",
                "tbTagPopular": "StdTasks",
                "tbTagNameShort": "Design",
                "tbTagPurpose": "Drag and Drop on Project or Release to create a Standard Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 310
            }, {
                "tbTagID": "311",
                "tbTagGroup": "stdTasks",
                "tbTagName": "Develop",
                "tbTagEntity": "StdTasks",
                "tbTagPopular": "StdTasks",
                "tbTagNameShort": "Dev",
                "tbTagPurpose": "Drag and Drop on Project or Release to create a Standard Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 311
            }, {
                "tbTagID": "312",
                "tbTagGroup": "stdTasks",
                "tbTagName": "Test",
                "tbTagEntity": "StdTasks",
                "tbTagPopular": "StdTasks",
                "tbTagNameShort": "Test",
                "tbTagPurpose": "Drag and Drop on Project or Release to create a Standard Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 312
            }, {
                "tbTagID": "313",
                "tbTagGroup": "stdTasks",
                "tbTagName": "UAT",
                "tbTagEntity": "StdTasks",
                "tbTagPopular": "StdTasks",
                "tbTagNameShort": "UAT",
                "tbTagPurpose": "Drag and Drop on Project or Release to create a Standard Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 313
            }, {
                "tbTagID": "314",
                "tbTagGroup": "stdTasks",
                "tbTagName": "Project Finish",
                "tbTagEntity": "StdTasks",
                "tbTagPopular": "StdTasks",
                "tbTagNameShort": "Finish",
                "tbTagPurpose": "Drag and Drop on Project or Release to create a Standard Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "PMO",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 314
            }, {
                "tbTagID": "315",
                "tbTagGroup": "tbType",
                "tbTagName": "Portfolio",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "Pf",
                "tbTagPurpose": "Drag and Drop on Canvas to create new Portfolio",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 315
            }, {
                "tbTagID": "316",
                "tbTagGroup": "tbType",
                "tbTagName": "Program",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "General",
                "tbTagNameShort": "Prog",
                "tbTagPurpose": "Drag and Drop on Portfilio to create new Program",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 316
            }, {
                "tbTagID": "317",
                "tbTagGroup": "tbType",
                "tbTagName": "Project",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "Proj",
                "tbTagPurpose": "Drag and Drop on on Portfolio or Program to create new Project",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 317
            }, {
                "tbTagID": "318",
                "tbTagGroup": "tbType",
                "tbTagName": "Task",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "Task",
                "tbTagPurpose": "Drag and Drop on,  another Task to clone it, a Project or Release to create new Task",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 318
            }, {
                "tbTagID": "319",
                "tbTagGroup": "tbType",
                "tbTagName": "Milestone",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "MS",
                "tbTagPurpose": "Drag and Drop on,  another Milestone to clone it, a Project or Release to create new Milestone",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 319
            }, {
                "tbTagID": "320",
                "tbTagGroup": "tbType",
                "tbTagName": "Sub-Project",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "subProj",
                "tbTagPurpose": "Drag and Drop on project to create sub project",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 320
            }, {
                "tbTagID": "321",
                "tbTagGroup": "tbType",
                "tbTagName": "Gate",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "Gate",
                "tbTagPurpose": "Drop to create a gate object on canvas",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 321
            }, {
                "tbTagID": "322",
                "tbTagGroup": "tbType",
                "tbTagName": "Allocation",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "General",
                "tbTagNameShort": "Alloc",
                "tbTagPurpose": "Not used, see Resource Allocator to assign a new Resource",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 322
            }, {
                "tbTagID": "323",
                "tbTagGroup": "tbType",
                "tbTagName": "Risk",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "General",
                "tbTagNameShort": "Risk",
                "tbTagPurpose": "Future - canvasDrop anywhere to create a Risk",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 323
            }, {
                "tbTagID": "324",
                "tbTagGroup": "tbType",
                "tbTagName": "Issue",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "General",
                "tbTagNameShort": "Issue",
                "tbTagPurpose": "Future - canvasDrop anywhere to create a Issue",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 324
            }, {
                "tbTagID": "325",
                "tbTagGroup": "tbType",
                "tbTagName": "ActionItem",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "General",
                "tbTagNameShort": "Actn",
                "tbTagPurpose": "Future - canvasDrop anywhere to create an Action Item",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 325
            }, {
                "tbTagID": "326",
                "tbTagGroup": "tbType",
                "tbTagName": "Release",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "Rel",
                "tbTagPurpose": "Drag and Drop on Canvas to create new Tile for free form text.",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 326
            }, {
                "tbTagID": "327",
                "tbTagGroup": "Team Leader",
                "tbTagName": "Ben tl",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Ben tl",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 327
            }, {
                "tbTagID": "328",
                "tbTagGroup": "Team Leader",
                "tbTagName": "Erica tl",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Erica tl",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 328
            }, {
                "tbTagID": "329",
                "tbTagGroup": "Team Leader",
                "tbTagName": "Julia tl",
                "tbTagEntity": "ResList",
                "tbTagPopular": "ResList",
                "tbTagNameShort": "Julia tl",
                "tbTagPurpose": "Resource or person tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 329
            }, {
                "tbTagID": "330",
                "tbTagGroup": "Type Of Initiative",
                "tbTagName": "Project",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Project",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 330
            }, {
                "tbTagID": "331",
                "tbTagGroup": "Type Of Initiative",
                "tbTagName": "Sprint",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Sprint",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 331
            }, {
                "tbTagID": "332",
                "tbTagGroup": "Type Of Initiative",
                "tbTagName": "Program",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Program",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 332
            }, {
                "tbTagID": "333",
                "tbTagGroup": "Type Of Initiative",
                "tbTagName": "Other",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Other",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 333
            }, {
                "tbTagID": "334",
                "tbTagGroup": "Type Of Initiative",
                "tbTagName": "Portfolio",
                "tbTagEntity": "ProjList",
                "tbTagPopular": "ProjList",
                "tbTagNameShort": "Portfolio",
                "tbTagPurpose": "Project Tagging",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 334
            }, {
                "tbTagID": "335",
                "tbTagGroup": "YesNo",
                "tbTagName": "Yes",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "Yes",
                "tbTagPurpose": "for tagging a time bar for true or false on unknown",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 335
            }, {
                "tbTagID": "336",
                "tbTagGroup": "YesNo",
                "tbTagName": "No",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "No",
                "tbTagPurpose": "for tagging a time bar for true or false on unknown",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 336
            }, {
                "tbTagID": "337",
                "tbTagGroup": "YesNo",
                "tbTagName": "NA",
                "tbTagEntity": "General",
                "tbTagPopular": "General",
                "tbTagNameShort": "NA",
                "tbTagPurpose": "for tagging a time bar for true or false on unknown",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "6",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",

                "id": 337
            }, {
                "tbTagID": "3211",
                "tbTagGroup": "tbType",
                "tbTagName": "Tile",
                "tbTagEntity": "NewBars",
                "tbTagPopular": "NewBars",
                "tbTagNameShort": "Tile",
                "tbTagPurpose": "Drop to create a gate object on canvas",
                "tbTagDescription": "NA",
                "tbTagOwner": "system",
                "tbTagSortOrder": "0",
                "tbTagCustomerID": "44",
                "tbTagAzureID": "0",
                "tbTagLastModified": "01/01/2020",
                "id": 338
            }],
        "tbResCalcs": [
            {
                "tbResCalcID": "a1",
                "tbResCalcTbResID": "700",
                "tbResCalcTbID": "525",
                "tbResCalcName": "SummaryRow",
                "tbResCalcWeek": 0,
                "tbResCalcHours": 0,
                "tbResCalcWdays": 0,
                "tbResCalcStartWeekNo": 5,
                "tbResCalcTbWeekNo": 30,
                "tbResCalcWeekSumHours": 0,
                "tbResCalcWeekSumCost": 0,
                "tbResCalcWeek1": 0,
                "tbResCalcWeek2": 0,
                "tbResCalcWeek3": 0,
                "tbResCalcWeek4": 0,
                "tbResCalcWeek5": 0,
                "tbResCalcWeek6": 0,
                "tbResCalcWeek7": 40,
                "tbResCalcWeek8": 40,
                "tbResCalcWeek9": 40,
                "tbResCalcWeek10": 40,
                "tbResCalcWeek11": 40,
                "tbResCalcWeek12": 41.400000000000006,
                "tbResCalcWeek13": 0,
                "tbResCalcWeek14": 0,
                "tbResCalcWeek15": 0,
                "tbResCalcWeek16": 46.34,
                "tbResCalcWeek17": 18.74,
                "tbResCalcWeek18": 18.74,
                "tbResCalcWeek19": 18.74,
                "tbResCalcWeek20": 18.74,
                "tbResCalcWeek21": 65.61,
                "tbResCalcWeek22": 56.239999999999995,
                "tbResCalcWeek23": 0,
                "tbResCalcWeek24": 0,
                "tbResCalcWeek25": 0,
                "tbResCalcWeek26": 0,
                "tbResCalcWeek27": 0,
                "tbResCalcWeek28": 0,
                "tbResCalcWeek29": 0,
                "tbResCalcWeek30": 0,
                "tbResCalcWeek31": 0,
                "tbResCalcWeek32": 0,
                "tbResCalcAzureID": "a1",
                "tbResCalcCustomerID": "a44",
                "tbResCalcLastModified": "22-Jan-19"
            },
            {
                "tbResCalcID": "2",
                "tbResCalctbResID": "700",
                "tbResCalcTbID": "525",
                "tbResCalcName": "DetailRow",
                "tbResCalcWeek": 5,
                "tbResCalcHours": 40,
                "tbResCalcWdays": 61,
                "tbResCalcStartWeekNo": 6,
                "tbResCalcTbWeekNo": 30,
                "tbResCalcWeekSumHours": 2.5,
                "tbResCalcWeekSumCost": 2.5,
                "tbResCalcWeek1": 0,
                "tbResCalcWeek2": 0,
                "tbResCalcWeek3": 0,
                "tbResCalcWeek4": 0,
                "tbResCalcWeek5": 0,
                "tbResCalcWeek6": 0,
                "tbResCalcWeek7": 0,
                "tbResCalcWeek8": 0,
                "tbResCalcWeek9": 0,
                "tbResCalcWeek10": 0,
                "tbResCalcWeek11": 0,
                "tbResCalcWeek12": 0,
                "tbResCalcWeek13": 0,
                "tbResCalcWeek14": 0,
                "tbResCalcWeek15": 0,
                "tbResCalcWeek16": 0,
                "tbResCalcWeek17": 0,
                "tbResCalcWeek18": 0,
                "tbResCalcWeek19": 0,
                "tbResCalcWeek20": 0,
                "tbResCalcWeek21": 0,
                "tbResCalcWeek22": 0,
                "tbResCalcWeek23": 0,
                "tbResCalcWeek24": 0,
                "tbResCalcWeek25": 0,
                "tbResCalcWeek26": 0,
                "tbResCalcWeek27": 0,
                "tbResCalcWeek28": 0,
                "tbResCalcWeek29": 0,
                "tbResCalcWeek30": 0,
                "tbResCalcWeek31": 0,
                "tbResCalcWeek32": 0,
                "tbResCalcAzureID": "a1",
                "tbResCalcCustomerID": "44",
                "tbResCalcLastModified": "22-Jan-19"
            }
        ]
    }
}


