// Copyright © 2018 Timebars Ltd.  All rights reserved.
// FOCD Meta Data Form

// import { openIDBGetStore } from '../scripts/tbdatabase';  
import { openIDBGetStore, getAdminPanelData, reloadPage, updateOneMetadataField } from '../scripts/tbdatabase';
import { UpdatableStd } from './focdcommon';




$('.btnSaveFOCDMetadata').on('click', function () {
    var form = document.getElementById('mdFormAdvL1')
    var tbmdid = form.getAttribute('data-tbid')
    saveFOCDMetadata(tbmdid)
});


/* START CKE Editor area */
// init all editors
setTimeout(() => {
    let tbmdid = "101"
    let formName = 'mdAdv'
    let editordata = ""
    let dbfieldName = ""

    /* Brief Descr */
    editordata = $('#tbMDDescription_mdAdv').text()
    dbfieldName = 'tbMDDescription'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* exec summary */
    editordata = $('#tbMDExecutiveSummary_mdAdv').text()
    dbfieldName = 'tbMDExecutiveSummary'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* notes */
    editordata = $('#tbMDNotes_mdAdv').text()
    dbfieldName = 'tbMDNotes'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* consequence */
    editordata = $('#tbMDConsequence_mdAdv').text()
    dbfieldName = 'tbMDConsequence'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* problem oppor hidden for now*/
    /*     editordata = $('#tbMDProblemOpportunity_mdAdv').text()
        dbfieldName = 'tbMDProblemOpportunity'
        initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid) */

    /* background */
    editordata = $('#tbMDBackgroundInfo_mdAdv').text()
    dbfieldName = 'tbMDBackgroundInfo'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* constraints */
    editordata = $('#tbMDConstraintsAssumptions_mdAdv').text()
    dbfieldName = 'tbMDConstraintsAssumptions'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* capabilities */
    editordata = $('#tbMDCapabilitiesNeeded_mdAdv').text()
    dbfieldName = 'tbMDCapabilitiesNeeded'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

    /* exp benefits */
    editordata = $('#tbMDExpectedBenfits_mdAdv').text()
    dbfieldName = 'tbMDExpectedBenfits'
    initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid)

}, 300);




export function initEditorMdAdvUniversal(editordata, dbfieldName, formName, tbmdid) {

    let formFieldName = "#" + dbfieldName + "_" + formName
    // 7th editor StakeholderDescription
    InlineEditor
        .create(document.querySelector(formFieldName), {
            toolbar: ['bold', 'italic', 'numberedList', 'blockQuote']
        })
        .then(editorIL => {
            // console.log("success loading editor for: " + dbfieldName)
        })
        .catch(error => {
            console.error(error);
        });
}

/* END CKE editor area */


/* START Editor Save Button */

// save the edit - only certain fields are editable, not all
export const saveFOCDMetadata = (tbmdid) => {
    // first get form data from ckeditor fields

    let tbMDDescription = document.querySelector('#tbMDDescription_mdAdv').innerHTML
    let tbMDExecutiveSummary = document.querySelector('#tbMDExecutiveSummary_mdAdv').innerHTML
    let tbMDNotes = document.querySelector('#tbMDNotes_mdAdv').innerHTML
    let tbMDConsequence = document.querySelector('#tbMDConsequence_mdAdv').innerHTML

    let tbMDBackgroundInfo = document.querySelector('#tbMDBackgroundInfo_mdAdv').innerHTML
    let tbMDConstraintsAssumptions = document.querySelector('#tbMDConstraintsAssumptions_mdAdv').innerHTML
    let tbMDExpectedBenfits = document.querySelector('#tbMDExpectedBenfits_mdAdv').innerHTML
    let tbMDCapabilitiesNeeded = document.querySelector('#tbMDCapabilitiesNeeded_mdAdv').innerHTML

    // tbMDProblemOpportunity = document.querySelector('#tbMDProblemOpportunity_mdAdv').innerHTML
    //   tbMDCostBenefitAnalysis = document.querySelector('#tbMDCostBenefitAnalysis_mdAdv').innerHTML 
    // tbMDSeniorLevelCommittment  = document.querySelector('#tbMDSeniorLevelCommittment_mdAdv').innerHTML 
    // tbMDStakeholderDescription = document.querySelector('#tbMDStakeholderDescription_mdAdv').innerHTML
    //   tbMDNotesWorkflow = document.querySelector('#tbMDNotesWorkflow_mdAdv').innerHTML 
    //  let tbMDSyncNotes = $("#tbMDSyncNotes").val()

    let storeName = 'tbMetaData'
    openIDBGetStore(storeName, 'readwrite', function (store) {

        console.log("tbmdid: " + tbmdid)
        let metadataid = String(tbmdid)

        var index = store.index("tbMDID");
        var request = index.get(metadataid);
        request.onerror = function (event) {
            // Handle errors!
            alert("there was error getting store id")
        };
        request.onsuccess = function (event) {
            // Get the exist value that we want to update
            var data = request.result;
            //    console.log("request.result " + JSON.stringify(data))

            try {
                // apply editable data fields on form to the data object
                data.tbMDDescription = tbMDDescription
                data.tbMDExecutiveSummary = tbMDExecutiveSummary
                data.tbMDNotes = tbMDNotes
                data.tbMDConsequence = tbMDConsequence

                data.tbMDBackgroundInfo = tbMDBackgroundInfo
                data.tbMDConstraintsAssumptions = tbMDConstraintsAssumptions
                data.tbMDExpectedBenfits = tbMDExpectedBenfits
                data.tbMDCapabilitiesNeeded = tbMDCapabilitiesNeeded

            } catch (e) {
                console.log("saveFOCDMetadata - fail try")
                throw e;
            }
            // Put this updated object back into the database.
            var requestUpdate = store.put(data);
            requestUpdate.onerror = function (event) {
                // Do something with the error
                alert("there was error updating idb")
            };
            requestUpdate.onsuccess = function (event) {
                console.log("saveFormDataMetadata - success")
            };
        }; // end onsuccess
    });
};


/* END Editor Save Button */




// make mdAdv Form collapsable
function initCollapsableMdFormAdv() {

    var elem = document.querySelector('.collapsible');
    var instance = M.Collapsible.init(elem, {
        accordion: false
    });
}
// run it on load
initCollapsableMdFormAdv()




// fill form with data from TBMetadata table idb
export function populateMetadataFormMD(tbMDID) {
    var tbMDID = String(tbMDID)
    openIDBGetStore('tbMetaData', 'readonly', function (store) {
        var index = store.index("tbMDID");
        var request = index.get(tbMDID);
        request.onerror = function (event) {
            // Handle errors!
            alert("there was error getting store id");
        };
        request.onsuccess = function (event) {
            // Get the old value that we want to update
            var data = request.result;
            if (!data) {
                alert("Refresh browser up to two times to initiate database cache.");
                return;
            }
            // ckeditor fields
            document.querySelector('#tbMDDescription_mdAdv').innerHTML = data.tbMDDescription
            document.querySelector('#tbMDNotes_mdAdv').innerHTML = data.tbMDNotes
            document.querySelector('#tbMDBackgroundInfo_mdAdv').innerHTML = data.tbMDBackgroundInfo
            document.querySelector('#tbMDConstraintsAssumptions_mdAdv').innerHTML = data.tbMDConstraintsAssumptions
            document.querySelector('#tbMDCapabilitiesNeeded_mdAdv').innerHTML = data.tbMDCapabilitiesNeeded
            document.querySelector('#tbMDConsequence_mdAdv').innerHTML = data.tbMDConsequence
            document.querySelector('#tbMDExpectedBenfits_mdAdv').innerHTML = data.tbMDExpectedBenfits
            //   document.querySelector('#tbMDCostBenefitAnalysis_mdAdv').innerHTML = data.tbMDCostBenefitAnalysis
            document.querySelector('#tbMDExecutiveSummary_mdAdv').innerHTML = data.tbMDExecutiveSummary
            //  document.querySelector('#tbMDSeniorLevelCommittment_mdAdv').innerHTML = data.tbMDSeniorLevelCommittment
            //  document.querySelector('#tbMDStakeholderDescription_mdAdv').innerHTML = data.tbMDStakeholderDescription
            //   document.querySelector('#tbMDNotesWorkflow_mdAdv').innerHTML = data.tbMDNotesWorkflow
            // document.querySelector('#tbMDProblemOpportunity_mdAdv').innerHTML = data.tbMDProblemOpportunity

            $("#tbMDID_mdAdv").val(data.tbMDID);
            $("#tbMDCustomerID_mdAdv").val(data.tbMDCustomerID);
            $("#tbMDProjectNumber_mdAdv").val(data.tbMDProjectNumber);
            $("#tbMDPriority_mdAdv").val(data.tbMDPriority);
            $("#tbMDStatus_mdAdv").val(data.tbMDStatus);
            $("#tbMDState_mdAdv").val(data.tbMDState);
            $("#tbMDSeverity_mdAdv").val(data.tbMDSeverity);
            $("#tbMDStage_mdAdv").val(data.tbMDStage);
            $("#tbMDPhase_mdAdv").val(data.tbMDPhase);
            $("#tbMDCategory_mdAdv").val(data.tbMDCategory);
            $("#tbMDHealth_mdAdv").val(data.tbMDHealth);
            $("#tbMDResponsibility_mdAdv").val(data.tbMDResponsibility);
            $("#tbMDDepartment_mdAdv").val(data.tbMDDepartment);
            $("#tbMDExSponsor_mdAdv").val(data.tbMDExSponsor);
            $("#tbMDPM_mdAdv").val(data.tbMDPM);
            $("#tbMDProjectType_mdAdv").val(data.tbMDProjectType);
            $("#tbMDShowIn_mdAdv").val(data.tbMDShowIn);
            $("#tbMDProduct_mdAdv").val(data.tbMDProduct);
            $("#tbMDContact_mdAdv").val(data.tbMDContact);
            $("#tbMDWBS_mdAdv").val(data.tbMDWBS);
            $("#tbMDWeighting_mdAdv").val(data.tbMDWeighting);
            $("#tbMDLocation_mdAdv").val(data.tbMDLocation);
            $("#tbMDPrimarySkill_mdAdv").val(data.tbMDPrimarySkill);
            $("#tbMDPrimaryRole_mdAdv").val(data.tbMDPrimaryRole);
            $("#tbMDGate_mdAdv").val(data.tbMDGate);
            $("#tbMDHealthOverall_mdAdv").val(data.tbMDHealthOverall);
            $("#tbMDHealthCost_mdAdv").val(data.tbMDHealthCost);
            $("#tbMDHealthIssues_mdAdv").val(data.tbMDHealthIssues);
            $("#tbMDHealthRisk_mdAdv").val(data.tbMDHealthRisk);
            $("#tbMDHealthSchedule_mdAdv").val(data.tbMDHealthSchedule);
            $("#tbMDHealthScope_mdAdv").val(data.tbMDHealthScope);
            $("#tbMDInvestmentCategory_mdAdv").val(data.tbMDInvestmentCategory);
            $("#tbMDInvestmentInitiative_mdAdv").val(data.tbMDInvestmentInitiative);
            $("#tbMDInvestmentObjective_mdAdv").val(data.tbMDInvestmentObjective);
            $("#tbMDInvestmentStrategy_mdAdv").val(data.tbMDInvestmentStrategy);
            $("#tbMDROMEstimate_mdAdv").val(data.tbMDROMEstimate);
            $("#tbMDPortfolio_mdAdv").val(data.tbMDPortfolio);
            $("#tbMDProgram_mdAdv").val(data.tbMDProgram);
            $("#tbMDProgActivityAlignment_mdAdv").val(data.tbMDProgActivityAlignment);
            $("#tbMDSize_mdAdv").val(data.tbMDSize);
            $("#tbMDStageApprover_mdAdv").val(data.tbMDStageApprover);
            $("#tbMDWrittenBy_mdAdv").val(data.tbMDWrittenBy);
            $("#tbMDBusinessAdvisor_mdAdv").val(data.tbMDBusinessAdvisor);
            $("#tbMDBusinessOwner_mdAdv").val(data.tbMDBusinessOwner);
            $("#tbMDDeliveryManager_mdAdv").val(data.tbMDDeliveryManager);
            $("#tbMDOrgManager_mdAdv").val(data.tbMDOrgManager);
            $("#tbMDPriorityStrategic_mdAdv").val(data.tbMDPriorityStrategic);
            $("#tbMDSponsoringDepartment_mdAdv").val(data.tbMDSponsoringDepartment);
            $("#tbMDPrimaryContact_mdAdv").val(data.tbMDPrimaryContact);
            $("#tbMDBenefitCostRatio_mdAdv").val(data.tbMDBenefitCostRatio);
            $("#tbMDContactNumber_mdAdv").val(data.tbMDContactNumber);
            $("#tbMDResponsibleTeam_mdAdv").val(data.tbMDResponsibleTeam);
            $("#tbMDRiskVsSizeAndComplexity_mdAdv").val(data.tbMDRiskVsSizeAndComplexity);
            $("#tbMDEcnomicValueAdded_mdAdv").val(data.tbMDEcnomicValueAdded);
            $("#tbMDEstimationClass_mdAdv").val(data.tbMDEstimationClass);
            $("#tbMDInternalRateOfReturn_mdAdv").val(data.tbMDInternalRateOfReturn);
            $("#tbMDSprintName_mdAdv").val(data.tbMDSprintName);
            $("#tbMDSunkCosts_mdAdv").val(data.tbMDSunkCosts);
            $("#tbMDSyncNotes_mdAdv").val(data.tbMDSyncNotes);
            $("#tbMDNotesProject_mdAdv").val(data.tbMDNotesProject);
            $("#tbMDContractNumber_mdAdv").val(data.tbMDContractNumber);
            $("#tbMDNetPresentValue_mdAdv").val(data.tbMDNetPresentValue);
            $("#tbMDOpportunityCost_mdAdv").val(data.tbMDOpportunityCost);
            $("#tbMDPaybackPeriod_mdAdv").val(data.tbMDPaybackPeriod);
            $("#tbMDPrimaryLineOfBusiness_mdAdv").val(data.tbMDPrimaryLineOfBusiness);

            M.updateTextFields()

        };
    });
}

