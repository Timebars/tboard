// was the todo app from noob coder
// was doc datatableuniversal
// refactored to use tb and idb


/* idb code */
import { openIDBGetStore, setTbidBasedOnKeyUniversal, deleteRowFromIDBUniversal, importJsonDataUniversal, clearObjectStore, createJsonFileSimple } from './tbdatabase';
import { displayMessage } from './focdcommon';



// global variables
const displayDT = $("#displayDtConfigdata");
//const message = $("#msgReporting");
const storeName = 'tbAdminPanel'



$('#btnBackupConfigdata').on('click', function () {
    //  console.log("clicked btnBackupConfigdata")
    createJsonFileSimple(storeName)
});

$('#cfgTab').on('click', function () {
    //  console.log("clicked Configdata")
    getConfigdata()
    setTimeout(() => {
        console.log("page load get Configdata")
        initDtConfigdata()

    }, 300);
});



// appends to displayDtConfigdata
const generateDatatableConfigdata = (Item, ids) => {
    return `<tr id="${ids.listItemID}">
        <td><a title="Edit this Configdata" id="${ids.editID}"><i class="material-icons tiny">edit</i></a></td>

        <td id="apID-${Item.apID}"><div>${Item.apID}</div></td>
        <td id="ap_name-${Item.apID}"><div>${Item.ap_name}</div></td>
        <td id="ap_HolidayName-${Item.apID}"><div>${Item.ap_HolidayName}</div></td>
        <td id="ap_HolidayDate-${Item.apID}"><div>${Item.ap_HolidayDate}</div></td>
        <td id="apStatusDate-${Item.apID}"><div>${Item.apStatusDate}</div></td>
        <td id="apTS_Start-${Item.apID}"><div>${Item.apTS_Start}</div></td>
        <td id="apHideCompletedBarsYN-${Item.apID}"><div>${Item.apHideCompletedBarsYN}</div></td>


        <td id="pxPerPerson-${Item.apID}"><div>${Item.pxPerPerson}</div></td>
        <td id="apFilteredFrom-${Item.apID}"><div>${Item.apFilteredFrom}</div></td>
        <td id="apFilteredFromTbID-${Item.apID}"><div>${Item.apFilteredFromTbID}</div></td>
        <td id="apSchMethod-${Item.apID}"><div>${Item.apSchMethod}</div></td>
        <td id="apTbVisibleLevel-${Item.apID}"><div>${Item.apTbVisibleLevel}</div></td>

        <td><button title="Delete this row" type="button" class="btn-flat" id="${ids.deleteID}"><i class="material-icons">delete_forever</i></button></td>
        </tr>`;
}

// Fields removed july 27
// <td id="totalNumberOfPeople-${Item.apID}"><div>${Item.totalNumberOfPeople}</div></td>
// <td id="numberOfPeoplePerLine-${Item.apID}"><div>${Item.numberOfPeoplePerLine}</div></td>
// <td id="hideCompletedBarsYN-${Item.apID}"><div>${Item.hideCompletedBarsYN}</div></td>
// <td id="apTS_WeeklyOrMonthly-${Item.apID}">${Item.apTS_WeeklyOrMonthly}</div></td>
// <td id="apTS_WeeklyPxFactor-${Item.apID}"><div>${Item.apTS_WeeklyPxFactor}</div></td>
// <td id="apTS_MonthlyPxFactor-${Item.apID}"><div>${Item.apTS_MonthlyPxFactor}</div></td>
// <td id="apHelpCoordTop-${Item.apID}"><div>${Item.apHelpCoordTop}</div></td>
// <td id="apHelpCoordLeft-${Item.apID}"><div>${Item.apHelpCoordLeft}</div></td>       
// <td id="apCustomerID-${Item.apID}"><div>${Item.apCustomerID}</div></td>
// <td id="apAzureID-${Item.apID}"><div>${Item.apAzureID}</div></td>

// new code get all items and display them
const getConfigdata = () => {

    returnAllConfigdata(function (data) {

        displayConfigdata(data);
    })
}

// master very simple function to get all rows in a table store  // later send store name as var
function returnAllConfigdata(callbackConfigdata) {
    openIDBGetStore(storeName, 'readwrite', function (store) {
        var request = store.getAll();
        request.onsuccess = function (evt) {
            var tb = evt.target.result
            // console.log(data)
            callbackConfigdata(tb)
        };
    })
};


// univ form materialize javascript
function launchConfigdataForm(configdataid) {

    // set data-tbid on form for later actions
    var myform = document.getElementById('launchConfigdataForm')
    var myform = myform.setAttribute('data-tbid', configdataid)

    var elem = document.querySelector('#launchConfigdataForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    //  console.log("launchEmptyConfigdataForm finished")

}


// univ form
$('#btnLaunchEmptyConfigdataForm').on('click', function () {    // open modal first
    launchEmptyConfigdataForm()
});

// univ form materialize javascript
function launchEmptyConfigdataForm() {


    var elem = document.querySelector('#launchConfigdataForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();



    setTimeout(() => {
        resetConfigdataInput()
    }, 300);
    //  console.log("launchEmptyConfigdataForm finished")

}

// not used, we only create Configdata rows from the app
$('#btnCreateConfigdata').on('click', function () {

    confirm("Are you sure, this will create a new Configdata. Must be unique Configdata Name")
    // remove any cached data in the form
    createConfigdata()
});

// not used, we only create Configdata rows from the app when creating new timebar
function createConfigdata() {

    // let apID = $('#apID').val();  // is auto generated by idb
    let ap_name = $("#ap_name").val()
    let ap_HolidayName = $("#ap_HolidayName").val()
    let ap_HolidayDate = $("#ap_HolidayDate").val()
    let apStatusDate = $("#apStatusDate").val()
    let apTS_Start = $("#apTS_Start").val()
    let apHideCompletedBarsYN = $("#apHideCompletedBarsYN").val()
    let apTS_WeeklyOrMonthly = $("#apTS_WeeklyOrMonthly").val()
    let apTS_WeeklyPxFactor = $("#apTS_WeeklyPxFactor").val()
    let apTS_MonthlyPxFactor = $("#apTS_MonthlyPxFactor").val()
    let apHelpCoordTop = $("#apHelpCoordTop").val()
    let apHelpCoordLeft = $("#apHelpCoordLeft").val()
    let totalNumberOfPeople = $("#totalNumberOfPeople").val()
    let numberOfPeoplePerLine = $("#numberOfPeoplePerLine").val()
    let pxPerPerson = $("#pxPerPerson").val()
    let apFilteredFrom = $("#apFilteredFrom").val()
    let apFilteredFromTbID = $("#apFilteredFromTbID").val()
    let apSchMethod = $("#apSchMethod").val()
    let apTbVisibleLevel = $("#apTbVisibleLevel").val()
    let hideCompletedBarsYN = $("#hideCompletedBarsYN").val()
    let apCustomerID = $("#apCustomerID").val()
    let apAzureID = $("#apAzureID").val()

    let nameCheck = $("#ap_HolidayDate").val()
    console.log("Name check " + nameCheck)
    if (nameCheck == '' || nameCheck == ' ') {
        alert("You must enter a Configdata Name!")
        return
    }

    /* build up an object */
    var obj = {

        ap_name: ap_name,
        ap_HolidayName: ap_HolidayName,
        ap_HolidayDate: ap_HolidayDate,
        apStatusDate: apStatusDate,
        apTS_Start: apTS_Start,
        apHideCompletedBarsYN: apHideCompletedBarsYN,
        apTS_WeeklyOrMonthly: apTS_WeeklyOrMonthly,
        apTS_WeeklyPxFactor: apTS_WeeklyPxFactor,
        apTS_MonthlyPxFactor: apTS_MonthlyPxFactor,
        apHelpCoordTop: apHelpCoordTop,
        apHelpCoordLeft: apHelpCoordLeft,
        totalNumberOfPeople: totalNumberOfPeople,
        numberOfPeoplePerLine: numberOfPeoplePerLine,
        pxPerPerson: pxPerPerson,
        apFilteredFrom: apFilteredFrom,
        apFilteredFromTbID: apFilteredFromTbID,
        apSchMethod: apSchMethod,
        apTbVisibleLevel: apTbVisibleLevel,
        hideCompletedBarsYN: hideCompletedBarsYN,
        apCustomerID: apCustomerID,
        apAzureID: apAzureID,

    };

    let newItemId = ""
    /*  add the Configdata */
    openIDBGetStore(storeName, 'readwrite', function (store) {

        var request;
        try {
            request = store.add(obj);
        } catch (e) {
            displayFailureMessage("Some Problem While adding");
            throw e;
        }
        request.onsuccess = function (e) {

            // return the id of the newly added item
            newItemId = request.result

            // now fetch the new record, and generate template stuff
            var request2 = store.get(newItemId);
            request2.onsuccess = function (e) {

                var data = e.target.result;

                console.log("data " + JSON.stringify(data))

                let ids = buildIdsConfigdata(data.id);

                displayDT.append(generateDatatableConfigdata(data, ids));
                fillConfigdataForm(data, ids.configdataID, ids.editID); //not sure why this was here ??, maybe for insterting new into dom
                deleteConfigdata(data, ids.listItemID, ids.deleteID);
                // update apID field to match indexedb key id
                let fieldName = "apID"
                let newVal = String(data.id)
                let uid = Number(data.id)  // keys are numbers in idb, we find the new record by key and update the tbid
                setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal)

                // console.log("ids " + JSON.stringify(ids))
                displayMessage(true, "createConfigdata Result: " + JSON.stringify(ap_HolidayDate) + " added!");

            }
        };
        request.onerror = function () {

            displayMessage(true, "May be duplicate configdata name. System error: " + this.error);
        };
    });

}

// do not delete rows from here, they get deleted if deleting timebars rows
const deleteConfigdata = (configdata, listItemID, deleteID) => {
    // note may have to modify this to suit configdataid as string for now apID and built in id must match
    //  console.log("configdata.apID: " + configdata.apID + " check!")
    var indexName = "apID"
    var configdataid = configdata.apID
    // console.log("configdataid " + configdataid)

    let deleteBtn = $(`#${deleteID}`);
    deleteBtn.click(() => {

        deleteRowFromIDBUniversal(storeName, indexName, configdataid)

        $(`#${listItemID}`).remove();
        displayMessage(true, "deleteConfigdata Result: " + listItemID + " deleted!");

    });
}

//  get one fill form first, put show details button, then edit button
const fillConfigdataForm = (configdata, configdataID, editID) => {

    //  console.log("fillConfigdataForm - configdata.apID " + configdata.apID)  // returns the id without underscore
    //  console.log("fillConfigdataForm - configdataID " + configdataID)  // configdata_70
    // console.log("fillConfigdataForm - editID " + editID)  //edit_70

    let editBtn = $(`#${editID}`);
    editBtn.click(() => {

        openIDBGetStore(storeName, 'readonly', function (store) {

            // var request = store.get(configdata.apID);
            var configdataid = String(configdata.apID)

            var index = store.index("apID");
            var request = index.get(configdataid);

            request.onerror = function (event) {
                alert("there was error getting store id");
            };
            request.onsuccess = function (event) {

                // here we are returning data to the form
                var data = request.result;
                if (!data) {
                    alert("not a valid key, check for string");
                    return;
                }
                // populate the form fields

                $("#apID").val(data.apID);
                $("#ap_name").val(data.ap_name);
                $("#ap_HolidayName").val(data.ap_HolidayName);
                $("#ap_HolidayDate").val(data.ap_HolidayDate);
                $("#apStatusDate").val(data.apStatusDate);
                $("#apTS_Start").val(data.apTS_Start);
                $("#apHideCompletedBarsYN").val(data.apHideCompletedBarsYN);
                $("#apTS_WeeklyOrMonthly").val(data.apTS_WeeklyOrMonthly);
                $("#apTS_WeeklyPxFactor").val(data.apTS_WeeklyPxFactor);
                $("#apTS_MonthlyPxFactor").val(data.apTS_MonthlyPxFactor);
                $("#apHelpCoordTop").val(data.apHelpCoordTop);
                $("#apHelpCoordLeft").val(data.apHelpCoordLeft);
                $("#totalNumberOfPeople").val(data.totalNumberOfPeople);
                $("#numberOfPeoplePerLine").val(data.numberOfPeoplePerLine);
                $("#pxPerPerson").val(data.pxPerPerson);
                $("#apFilteredFrom").val(data.apFilteredFrom);
                $("#apFilteredFromTbID").val(data.apFilteredFromTbID);
                $("#apSchMethod").val(data.apSchMethod);
                $("#apTbVisibleLevel").val(data.apTbVisibleLevel);
                $("#hideCompletedBarsYN").val(data.hideCompletedBarsYN);
                $("#apCustomerID").val(data.apCustomerID);
                $("#apAzureID").val(data.apAzureID);


                //  console.log("data sent to form: " + JSON.stringify(data))
                displayMessage(true, "Form opened, ready for editing!");
                launchConfigdataForm(configdataid)
                // activate the labels on the form
                M.updateTextFields()
            };
        });
    });


}


$('#btnSaveFormDataConfigdata').on('click', function () {
    saveFormDataConfigdata()
});

// save the edit - only certain fields are editable, not all
const saveFormDataConfigdata = () => {
    // first get form data

    let ap_name = $("#ap_name").val()
    let ap_HolidayName = $("#ap_HolidayName").val()
    let ap_HolidayDate = $("#ap_HolidayDate").val()
    let apStatusDate = $("#apStatusDate").val()
    let apTS_Start = $("#apTS_Start").val()
    let apHideCompletedBarsYN = $("#apHideCompletedBarsYN").val()
    let apTS_WeeklyOrMonthly = $("#apTS_WeeklyOrMonthly").val()
    let apTS_WeeklyPxFactor = $("#apTS_WeeklyPxFactor").val()
    let apTS_MonthlyPxFactor = $("#apTS_MonthlyPxFactor").val()
    let apHelpCoordTop = $("#apHelpCoordTop").val()
    let apHelpCoordLeft = $("#apHelpCoordLeft").val()
    let totalNumberOfPeople = $("#totalNumberOfPeople").val()
    let numberOfPeoplePerLine = $("#numberOfPeoplePerLine").val()
    let pxPerPerson = $("#pxPerPerson").val()
    let apFilteredFrom = $("#apFilteredFrom").val()
    let apFilteredFromTbID = $("#apFilteredFromTbID").val()
    let apSchMethod = $("#apSchMethod").val()
    let apTbVisibleLevel = $("#apTbVisibleLevel").val()
    let hideCompletedBarsYN = $("#hideCompletedBarsYN").val()
    let apCustomerID = $("#apCustomerID").val()
    let apAzureID = $("#apAzureID").val()


    openIDBGetStore(storeName, 'readwrite', function (store) {
        var configdataid = $("#apID").val()
        console.log("apID: " + configdataid)
        configdataid = String(configdataid)

        var index = store.index("apID");
        var request = index.get(configdataid);
        request.onerror = function (event) {
            // Handle errors!
            alert("there was error getting store id")
        };
        request.onsuccess = function (event) {
            // Get the exist value that we want to update
            var data = request.result;
            //    console.log("request.result " + JSON.stringify(data))

            try {
                // apply editable data fields on form to the data object
                data.ap_name = ap_name
                data.ap_HolidayName = ap_HolidayName
                data.ap_HolidayDate = ap_HolidayDate
                data.apStatusDate = apStatusDate
                data.apTS_Start = apTS_Start
                data.apHideCompletedBarsYN = apHideCompletedBarsYN
                data.apTS_WeeklyOrMonthly = apTS_WeeklyOrMonthly
                data.apTS_WeeklyPxFactor = apTS_WeeklyPxFactor
                data.apTS_MonthlyPxFactor = apTS_MonthlyPxFactor
                data.apHelpCoordTop = apHelpCoordTop
                data.apHelpCoordLeft = apHelpCoordLeft
                data.totalNumberOfPeople = totalNumberOfPeople
                data.numberOfPeoplePerLine = numberOfPeoplePerLine
                data.pxPerPerson = pxPerPerson
                data.apFilteredFrom = apFilteredFrom
                data.apFilteredFromTbID = apFilteredFromTbID
                data.apSchMethod = apSchMethod
                data.apTbVisibleLevel = apTbVisibleLevel
                data.hideCompletedBarsYN = hideCompletedBarsYN
                data.apCustomerID = apCustomerID
                data.apAzureID = apAzureID


            } catch (e) {
                console.log("saveFormDataConfigdata - fail")
                throw e;
            }
            // Put this updated object back into the database.
            var requestUpdate = store.put(data);
            requestUpdate.onerror = function (event) {
                // Do something with the error
                alert("there was error updating progress in idb")
            };
            requestUpdate.onsuccess = function (event) {

                console.log("Success - Form is updated!")

                // $("#tbid").text(apID);  // universal input on all forms used on focdcommon
                $(`#ap_name-${configdataid}`).text(ap_name);
                $(`#ap_HolidayName-${configdataid}`).text(ap_HolidayName);
                $(`#ap_HolidayDate-${configdataid}`).text(ap_HolidayDate);
                $(`#apStatusDate-${configdataid}`).text(apStatusDate);
                $(`#apTS_Start-${configdataid}`).text(apTS_Start);
                $(`#apHideCompletedBarsYN-${configdataid}`).text(apHideCompletedBarsYN);
                $(`#apTS_WeeklyOrMonthly-${configdataid}`).text(apTS_WeeklyOrMonthly);
                $(`#apTS_WeeklyPxFactor-${configdataid}`).text(apTS_WeeklyPxFactor);
                $(`#apTS_MonthlyPxFactor-${configdataid}`).text(apTS_MonthlyPxFactor);
                $(`#apHelpCoordTop-${configdataid}`).text(apHelpCoordTop);
                $(`#apHelpCoordLeft-${configdataid}`).text(apHelpCoordLeft);
                $(`#totalNumberOfPeople-${configdataid}`).text(totalNumberOfPeople);
                $(`#numberOfPeoplePerLine-${configdataid}`).text(numberOfPeoplePerLine);
                $(`#pxPerPerson-${configdataid}`).text(pxPerPerson);
                $(`#apFilteredFrom-${configdataid}`).text(apFilteredFrom);
                $(`#apFilteredFromTbID-${configdataid}`).text(apFilteredFromTbID);
                $(`#apSchMethod-${configdataid}`).text(apSchMethod);
                $(`#apTbVisibleLevel-${configdataid}`).text(apTbVisibleLevel);
                $(`#hideCompletedBarsYN-${configdataid}`).text(hideCompletedBarsYN);
                $(`#apCustomerID-${configdataid}`).text(apCustomerID);
                $(`#apAzureID-${configdataid}`).text(apAzureID);

            };
        }; // end onsuccess
    });
};



function initDtConfigdata() {
    $('#dataTableConfigdata').DataTable({
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',  // f = search 
        "dom": '<"top"f>rt<"bottom"Bilp>',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        select: true,
        colReorder: true,  // change what columns show first, second etc.
        responsive: true,
        fixedHeader: true,
        destroy: true,

    });

};



$('#clearForm').on('click', function () {
    console.log("clicked reset configdata")
    resetConfigdataInput()
});



/* show or hide brief descriptn because it is truncated */
setTimeout(() => {
    $('.showhide').on('dblclick', function () {

        console.log("clicked showhide")

        if ($('.showhide').hasClass('truncate')) {
            $('.showhide').removeClass('truncate');
            $('.showhide').addClass('truncated');
        } else {
            $('.showhide').removeClass('truncated');
            $('.showhide').addClass('truncate');

        }
    });
}, 500);



const resetConfigdataInput = () => {

    $("#apID").val();
    $("#ap_name").val();
    $("#ap_HolidayName").val();
    $("#ap_HolidayDate").val();
    $("#apStatusDate").val();
    $("#apTS_Start").val();
    $("#apHideCompletedBarsYN").val();
    $("#apTS_WeeklyOrMonthly").val();
    $("#apTS_WeeklyPxFactor").val();
    $("#apTS_MonthlyPxFactor").val();
    $("#apHelpCoordTop").val();
    $("#apHelpCoordLeft").val();
    $("#totalNumberOfPeople").val();
    $("#numberOfPeoplePerLine").val();
    $("#pxPerPerson").val();
    $("#apFilteredFrom").val();
    $("#apFilteredFromTbID").val();
    $("#apSchMethod").val();
    $("#apTbVisibleLevel").val();
    $("#hideCompletedBarsYN").val();
    $("#apCustomerID").val();
    $("#apAzureID").val();
}

const buildIdsConfigdata = (itemid) => {
    return {
        editID: "edit_" + itemid,
        deleteID: "delete_" + itemid,
        listItemID: "listItem_" + itemid,
        configdataID: "configdata_" + itemid,
        bpselectorID: "bpselector_" + itemid
    }
}

const displayConfigdata = (data) => {
    data.forEach((Item) => {
        let ids = buildIdsConfigdata(Item.apID);
        //  console.log("displayConfigdata ids: " + JSON.stringify(ids))
        // looks like this ids: {"editID":"edit_83","deleteID":"delete_83","listItemID":"listItem_83","configdataID":"configdata_83","bpselectorID":"bpselector_83"}
        displayDT.append(generateDatatableConfigdata(Item, ids));
        fillConfigdataForm(Item, ids.configdataID, ids.editID);
        deleteConfigdata(Item, ids.listItemID, ids.deleteID);
    });
}

