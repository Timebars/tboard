// was the todo app from noob coder
// was doc datatableuniversal
// refactored to use tb and idb


/* idb code */
import { openIDBGetStore, setTbidBasedOnKeyUniversal, deleteRowFromIDBUniversal, importJsonDataUniversal, clearObjectStore, createJsonFileSimple } from './tbdatabase';
import { displayMessage } from './focdcommon';



// global variables
const displayDT = $("#displayDtDocuments");
//const message = $("#msgReporting");
const storeName = 'tbDocuments'



$('#btnBackupDocumentData').on('click', function () {
    //  console.log("clicked btnBackupDocument")
    createJsonFileSimple(storeName)
});

$('#docTab').on('click', function () {
    //  console.log("clicked Document")
    getDocuments()
    setTimeout(() => {
        //   console.log("page load get Document")
        initDtDocuments()

    }, 300);
});

// appends to dataTableDocuments
const generateDatatableDocuments = (Item, ids) => {
    return `<tr id="${ids.listItemID}">
    <td><a title="Edit this document" id="${ids.editID}"><i class="material-icons tiny">edit</i></a></td>
        <td id="tbDocID-${Item.tbDocID}"><div>${Item.tbDocID}</div></td>
        <td id="tbDocName-${Item.tbDocID}"><div style="width:200px">${Item.tbDocName}</div></td>
        
        <td id="tbDocType-${Item.tbDocID}"><div>${Item.tbDocType}</div></td>
        <td id="tbDocPurpose-${Item.tbDocID}"><div style="width:75px">${Item.tbDocPurpose}</div></td>
        <td id="tbDocOwner-${Item.tbDocID}"><div style="width:75px">${Item.tbDocOwner}</div></td>
        <td id="tbDocStatus-${Item.tbDocID}"><div style="width:50px">${Item.tbDocStatus}</div></td>
        <td id="tbDocGroup-${Item.tbDocID}"><div style="width:75px">${Item.tbDocGroup}</div></td>
        <td id="tbDocSortOrder-${Item.tbDocID}"><div style="width:50px">${Item.tbDocSortOrder}</div></td>
        <td id="tbDocPopular-${Item.tbDocID}"><div style="width:40px">${Item.tbDocPopular}</div></td>
        <td id="tbDocWrittenBy-${Item.tbDocID}"><div style="width:75px">${Item.tbDocWrittenBy}</div></td>

        <td><button title="Delete this Document" type="button" class="btn-flat" id="${ids.deleteID}"><i class="material-icons">delete_forever</i></button></td>
        </tr>`;
}
// Fields removed July 27 2019
// <td id="tbDocApprovedBy-${Item.tbDocID}"><div style="width:75px">${Item.tbDocApprovedBy}</div></td>     
// <td id="tbDocNameShort-${Item.tbDocID}"><div style="width:150px">${Item.tbDocNameShort}</div></td>
// <td><button title="Generate PDF Document" class="btn-flatxxx" id="${ids.printID}"><i class="material-icons">picture_as_pdf</i></button></td>
// <td id="batchPrintSelector-${Item.tbDocID}"> <label title="Select many and print in bulk" ><input  id="${ids.bpselectorID}"  type="checkbox" ${Item.batchPrintSelector} /><span></span></label> </td>
// <td id="tbDocBriefDescription-${Item.tbDocID}"><div class="truncate showhide" style="width:200px">${Item.tbDocBriefDescription}</div> </td>

/* START CKE Editor area */

function initEditor(editorData) {

    ClassicEditor
        .create(document.querySelector('#editor'))
        .then(editor => {

            editor.setData(editorData);

            window.editor = editor;
        })
        .catch(error => {
            console.error(error);
        });
    // console.log('editorData ' + editorData);
}


function getEditorData() {

    var thedata = window.editor.getData()
    console.log('Got editor data', thedata);
    return thedata
}


$('#closeEditor').on('click', function () {

    console.log("destroy editor")
    window.editor.destroy()
        .catch(error => {
            console.log(error);
        });

});

/* END CKE editor area */



// new code get all items and display them
const getDocuments = () => {

    returnAllDocuments(function (data) {

        displayDocuments(data);
    })
}

// master very simple function to get all rows in a table store  // later send store name as var
function returnAllDocuments(callbackDocuments) {
    openIDBGetStore(storeName, 'readwrite', function (store) {
        var request = store.getAll();
        request.onsuccess = function (evt) {
            var tb = evt.target.result
            // console.log(data)
            callbackDocuments(tb)
        };
    })
};


// univ form materialize javascript
function launchDocumentsForm(documentid) {
    //  var editorData = ""
    //  initEditor(editorData)

    // set data-tbid on form for later actions
    var myform = document.getElementById('launchDocumentsForm')
    var myform = myform.setAttribute('data-tbid', documentid)

    var elem = document.querySelector('#launchDocumentsForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    //  console.log("launchEmptyDocumentsForm finished")

}


// univ form
$('#btnLaunchEmptyDocumentsForm').on('click', function () {    // open modal first
    launchEmptyDocumentsForm()
});

// univ form materialize javascript
function launchEmptyDocumentsForm() {

    var editorData = ""
    initEditor(editorData)

    var elem = document.querySelector('#launchDocumentsForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();



    setTimeout(() => {
        resetDocumentsInput()
    }, 300);
    //  console.log("launchEmptyDocumentsForm finished")

}

// not used, we only create Document rows from the app
$('#btnCreateDocument').on('click', function () {

    confirm("Are you sure, this will create a new Document. Must be unique Document Name")
    // remove any cached data in the form
    createDocument()
});

// not used, we only create Document rows from the app when creating new timebar
function createDocument() {

    // let tbDocID = $('#tbDocID').val();  // is auto generated by idb
    let tbDocName = $("#tbDocName").val()
    let tbDocCustomerID = $("#tbDocCustomerID").val()
    let tbDocBriefDescription = $("#tbDocBriefDescription").val()
    let tbDocType = $("#tbDocType").val()
    let tbDocNameShort = $("#tbDocNameShort").val()

    /* account for CK Editor */
    let tbDocHTMLContent = getEditorData()
    //console.log("thedata" + tbDocHTMLContent)

    let tbDocGroup = $("#tbDocGroup").val()
    let tbDocPurpose = $("#tbDocPurpose").val()
    let tbDocOwner = $("#tbDocOwner").val()
    let tbDocPopular = $("#tbDocPopular").val()
    let tbDocSortOrder = $("#tbDocSortOrder").val()
    let tbDocLastModified = $("#tbDocLastModified").val()
    let tbDocApprovedBy = $("#tbDocApprovedBy").val()
    let tbDocStatus = $("#tbDocStatus").val()
    let tbDocWrittenBy = $("#tbDocWrittenBy").val()

    let nameCheck = $("#tbDocName").val()
    console.log("Name check " + nameCheck)
    if (nameCheck == '' || nameCheck == ' ') {
        alert("You must enter a Document Name!")
        return
    }

    /* build up an object */
    var obj = {

        tbDocName: tbDocName,
        tbDocCustomerID: tbDocCustomerID,
        tbDocBriefDescription: tbDocBriefDescription,
        tbDocType: tbDocType,
        tbDocNameShort: tbDocNameShort,
        tbDocHTMLContent: tbDocHTMLContent,
        tbDocGroup: tbDocGroup,
        tbDocPurpose: tbDocPurpose,
        tbDocOwner: tbDocOwner,
        tbDocPopular: tbDocPopular,
        tbDocSortOrder: tbDocSortOrder,
        tbDocLastModified: tbDocLastModified,
        tbDocApprovedBy: tbDocApprovedBy,
        tbDocStatus: tbDocStatus,
        tbDocWrittenBy: tbDocWrittenBy,
    };

    let newItemId = ""
    /*  add the Document */
    openIDBGetStore(storeName, 'readwrite', function (store) {

        var request;
        try {
            request = store.add(obj);
        } catch (e) {
            displayMessage(true, "Some Problem While adding. System error: " + e.error);
            throw e;
        }
        request.onsuccess = function (e) {

            // return the id of the newly added item
            newItemId = request.result

            // now fetch the new record, and generate template stuff
            var request2 = store.get(newItemId);
            request2.onsuccess = function (e) {

                var data = e.target.result;

                console.log("data " + JSON.stringify(data))

                let ids = buildIdsDocuments(data.id);

                displayDT.append(generateDatatableDocuments(data, ids));
                fillDocumentsForm(data, ids.documentID, ids.editID); //not sure why this was here ??, maybe for insterting new into dom
                deleteDocument(data, ids.listItemID, ids.deleteID);
                // update tbDocID field to match indexedb key id
                let fieldName = "tbDocID"
                let newVal = String(data.id)
                let uid = Number(data.id)  // keys are numbers in idb, we find the new record by key and update the tbid
                setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal)

                // console.log("ids " + JSON.stringify(ids))
                displayMessage(true, "createDocument Result: " + JSON.stringify(tbDocCustomerID) + " added!");

            }
        };
        request.onerror = function () {

            displayMessage(true, "May be duplicate document name. System error: " + this.error);
        };
    });

}

// do not delete rows from here, they get deleted if deleting timebars rows
const deleteDocument = (document, listItemID, deleteID) => {
    // note may have to modify this to suit documentid as string for now tbDocID and built in id must match
    //  console.log("document.tbDocID: " + document.tbDocID + " check!")
    var indexName = "tbDocID"
    var documentid = document.tbDocID
    // console.log("documentid " + documentid)

    let deleteBtn = $(`#${deleteID}`);
    deleteBtn.click(() => {

        deleteRowFromIDBUniversal(storeName, indexName, documentid)

        $(`#${listItemID}`).remove();
        displayMessage(true, "deleteDocument Result: " + listItemID + " deleted!");

    });
}

//  get one fill form first, put show details button, then edit button
const fillDocumentsForm = (document, documentID, editID) => {

    //  console.log("fillDocumentsForm - document.tbDocID " + document.tbDocID)  // returns the id without underscore
    //  console.log("fillDocumentsForm - documentID " + documentID)  // document_70
    // console.log("fillDocumentsForm - editID " + editID)  //edit_70

    let editBtn = $(`#${editID}`);
    editBtn.click(() => {

        openIDBGetStore(storeName, 'readonly', function (store) {

            // var request = store.get(document.tbDocID);
            var documentid = String(document.tbDocID)

            var index = store.index("tbDocID");
            var request = index.get(documentid);

            request.onerror = function (event) {
                alert("there was error getting store id");
            };
            request.onsuccess = function (event) {

                // here we are returning data to the form
                var data = request.result;
                if (!data) {
                    alert("doc not a valid key, check for string");
                    return;
                }
                // populate the form fields

                $("#tbDocID").val(data.tbDocID);
                $("#tbDocID").val(data.tbDocID);
                $("#tbDocName").val(data.tbDocName);
                $("#tbDocCustomerID").val(data.tbDocCustomerID);
                $("#tbDocBriefDescription").val(data.tbDocBriefDescription);
                $("#tbDocType").val(data.tbDocType);
                $("#tbDocNameShort").val(data.tbDocNameShort);

                initEditor(data.tbDocHTMLContent)

                $("#tbDocGroup").val(data.tbDocGroup);
                $("#tbDocPurpose").val(data.tbDocPurpose);
                $("#tbDocOwner").val(data.tbDocOwner);
                $("#tbDocPopular").val(data.tbDocPopular);
                $("#tbDocSortOrder").val(data.tbDocSortOrder);
                $("#tbDocLastModified").val(data.tbDocLastModified);
                $("#tbDocApprovedBy").val(data.tbDocApprovedBy);
                $("#tbDocStatus").val(data.tbDocStatus);
                $("#tbDocWrittenBy").val(data.tbDocWrittenBy);

                //  console.log("data sent to form: " + JSON.stringify(data))
                displayMessage(true, "Form opened, ready for editing!");

                launchDocumentsForm(documentid)

                // activate the labels on the form
                M.updateTextFields()
                // open or resize textarea fields
                M.textareaAutoResize($('#tbDocBriefDescription'));
                // to make this work, simply add class data-length="250" to text area or input
                $('input#tbDocNameShort, textarea#tbDocBriefDescription').characterCounter();
            };
        });
    });


}


$('#btnSaveFormDataDocument').on('click', function () {
    saveFormDataDocument()
});

// save the edit - only certain fields are editable, not all
const saveFormDataDocument = () => {
    // first get form data
    let tbDocID = $("#tbDocID").val()
    let tbDocName = $("#tbDocName").val()
    let tbDocCustomerID = $("#tbDocCustomerID").val()
    let tbDocBriefDescription = $("#tbDocBriefDescription").val()
    let tbDocType = $("#tbDocType").val()
    let tbDocNameShort = $("#tbDocNameShort").val()

    let tbDocHTMLContent = getEditorData()

    let tbDocGroup = $("#tbDocGroup").val()
    let tbDocPurpose = $("#tbDocPurpose").val()
    let tbDocOwner = $("#tbDocOwner").val()
    let tbDocPopular = $("#tbDocPopular").val()
    let tbDocSortOrder = $("#tbDocSortOrder").val()
    let tbDocLastModified = $("#tbDocLastModified").val()
    let tbDocApprovedBy = $("#tbDocApprovedBy").val()
    let tbDocStatus = $("#tbDocStatus").val()
    let tbDocWrittenBy = $("#tbDocWrittenBy").val()

    openIDBGetStore(storeName, 'readwrite', function (store) {
        var documentid = $("#tbDocID").val()
        console.log("tbDocID: " + documentid)
        documentid = String(documentid)

        var index = store.index("tbDocID");
        var request = index.get(documentid);
        request.onerror = function (event) {
            // Handle errors!
            alert("there was error getting store id")
        };
        request.onsuccess = function (event) {
            // Get the exist value that we want to update
            var data = request.result;
            //    console.log("request.result " + JSON.stringify(data))

            try {
                // apply editable data fields on form to the data object
                data.tbDocID = tbDocID
                data.tbDocName = tbDocName
                data.tbDocCustomerID = tbDocCustomerID
                data.tbDocBriefDescription = tbDocBriefDescription
                data.tbDocType = tbDocType
                data.tbDocNameShort = tbDocNameShort
                data.tbDocHTMLContent = tbDocHTMLContent
                data.tbDocGroup = tbDocGroup
                data.tbDocPurpose = tbDocPurpose
                data.tbDocOwner = tbDocOwner
                data.tbDocPopular = tbDocPopular
                data.tbDocSortOrder = tbDocSortOrder
                data.tbDocLastModified = tbDocLastModified
                data.tbDocApprovedBy = tbDocApprovedBy
                data.tbDocStatus = tbDocStatus
                data.tbDocWrittenBy = tbDocWrittenBy



            } catch (e) {
                console.log("saveFormDataDocument - fail")
                throw e;
            }
            // Put this updated object back into the database.
            var requestUpdate = store.put(data);
            requestUpdate.onerror = function (event) {
                // Do something with the error
                alert("there was error updating progress in idb")
            };
            requestUpdate.onsuccess = function (event) {

                console.log("Success - Form is updated!")

                // $("#tbid").text(tbDocID);  // universal input on all forms used on focdcommon
                $(`#tbDocID-${documentid}`).text(tbDocID);
                $(`#tbDocName-${documentid}`).text(tbDocName);
                $(`#tbDocCustomerID-${documentid}`).text(tbDocCustomerID);
                $(`#tbDocBriefDescription-${documentid}`).text(tbDocBriefDescription);
                $(`#tbDocType-${documentid}`).text(tbDocType);
                $(`#tbDocNameShort-${documentid}`).text(tbDocNameShort);
                $(`#tbDocHTMLContent-${documentid}`).text(tbDocHTMLContent);
                $(`#tbDocGroup-${documentid}`).text(tbDocGroup);
                $(`#tbDocPurpose-${documentid}`).text(tbDocPurpose);
                $(`#tbDocOwner-${documentid}`).text(tbDocOwner);
                $(`#tbDocPopular-${documentid}`).text(tbDocPopular);
                $(`#tbDocSortOrder-${documentid}`).text(tbDocSortOrder);
                $(`#tbDocLastModified-${documentid}`).text(tbDocLastModified);
                $(`#tbDocApprovedBy-${documentid}`).text(tbDocApprovedBy);
                $(`#tbDocStatus-${documentid}`).text(tbDocStatus);
                $(`#tbDocWrittenBy-${documentid}`).text(tbDocWrittenBy);

            };
        }; // end onsuccess
    });
};



function initDtDocuments() {
    $('#dataTableDocuments').DataTable({
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',  // f = search 
        "dom": '<"top"f>rt<"bottom"Bilp>',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        select: true,
        colReorder: true,  // change what columns show first, second etc.
        responsive: true,
        fixedHeader: true,
        destroy: true,

    });

};



$('#clearForm').on('click', function () {
    console.log("clicked reset document")
    resetDocumentsInput()
});



/* show or hide brief descriptn because it is truncated */
setTimeout(() => {
    $('.showhide').on('dblclick', function () {

        console.log("clicked showhide")

        if ($('.showhide').hasClass('truncate')) {
            $('.showhide').removeClass('truncate');
            $('.showhide').addClass('truncated');
        } else {
            $('.showhide').removeClass('truncated');
            $('.showhide').addClass('truncate');

        }
    });
}, 500);



const resetDocumentsInput = () => {


    $("#tbDocID").val();
    $("#tbDocID").val();
    $("#tbDocName").val();
    $("#tbDocCustomerID").val();
    $("#tbDocBriefDescription").val();
    $("#tbDocType").val();
    $("#tbDocNameShort").val();
    $("#tbDocHTMLContent").val();
    $("#tbDocGroup").val();
    $("#tbDocPurpose").val();
    $("#tbDocOwner").val();
    $("#tbDocPopular").val();
    $("#tbDocSortOrder").val();
    $("#tbDocLastModified").val();
    $("#tbDocApprovedBy").val();
    $("#tbDocStatus").val();
    $("#tbDocWrittenBy").val();

}

const buildIdsDocuments = (itemid) => {
    return {
        editID: "edit_" + itemid,
        deleteID: "delete_" + itemid,
        listItemID: "listItem_" + itemid,
        documentID: "document_" + itemid,
        bpselectorID: "bpselector_" + itemid
    }
}

const displayDocuments = (data) => {
    data.forEach((Item) => {
        let ids = buildIdsDocuments(Item.tbDocID);
        //  console.log("displayDocuments ids: " + JSON.stringify(ids))
        // looks like this ids: {"editID":"edit_83","deleteID":"delete_83","listItemID":"listItem_83","documentID":"document_83","bpselectorID":"bpselector_83"}
        displayDT.append(generateDatatableDocuments(Item, ids));
        fillDocumentsForm(Item, ids.documentID, ids.editID);
        deleteDocument(Item, ids.listItemID, ids.deleteID);
    });
}

