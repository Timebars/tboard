// template literals awesomeness!!!

export function statusFormBarHeaderHTML(targetDiv, tbID, tbName, tbType) {
    console.log("tbType: " + tbType);
    let bgColor = "";
    let textColor = "";

    if (tbType === "Portfolio") {
        textColor = "White";
        bgColor = "black";
    }
    if (tbType === "Project") {
        textColor = "white";
        bgColor = "rgba(50, 119, 44, 0.89)";
    }
    if (tbType === "Sub-Project") {
        textColor = "Black";
        bgColor = "rgb(228, 210, 163)";
    }
    if (tbType === "Task") {
        textColor = "white";
        bgColor = "rgb(37, 25, 204)";
    }
    if (tbType === "Allocation") {
        textColor = "black";
        bgColor = "gold";
    }
    if (tbType === "Release") {
        textColor = "whitesmoke";
        bgColor = "rgb(138, 111, 111);";
    }

    let td = $(targetDiv);
    let statusFormBarHeader = `
    <div class="statusFormBarHeader">
        <a title="Close Form" id="btnCloseStatusForm" style="margin-top: 5px;"><i
                class="material-icons right">close</i></a>
        <div id="headerForStatusFormBar" data-tbid="${tbID}">
            <div style="position: relative; left: 2px; top: 5px; width: 370px;">
                <div style="height:34px; background-color:${bgColor}; padding: 5px; color: ${textColor};">
                    ${tbName}
                </div>
            </div>
        </div>
    </div>
    `;
    td.append(statusFormBarHeader);
}

export function statusFormIndicatorsHTML(targetDiv, tbID) {
    let td = $(targetDiv);
    let statusFormIndicators = `
    <!-- start button bar -->
    <div style="margin-top: 10px;">
    <a href="#modalBurndown1" class="modal-trigger blue-grey-text darken-4" title="Launch Burndown Chart" id="btnLaunchBurndownFromStatusForm">View Burndown</a>
    <a title="Launch Burndown Chart" id="btnView2ColumMetaDataTable"> | View Metadata</a>
    </div>
<div style="margin-top: 8px;">Indicators</div>
    <!-- start indicators section on status form sf -->

        <table id="statusFormIndicatorTable" data-tbid="${tbID}" class="statusFormIndicatorTable" style="margin-top: 5px; pointer-events: auto">
            <thead>
                <tr>
                    <th>Overall</th>
                    <th>Schedule</th>
                    <th>Cost</th>
                    <th>Hours</th>
                    <th>Risk</th>
                    <th>Issues</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div id="tbMDHealthOverall_sf" class="StopLightTB mcttUpdatablev1" data-formkey="md-statusForm-Health Indicators">  
                        </div>
                    </td>
                    <td>
                        <div id="tbMDHealthSchedule_sf" class="StopLightTB mcttUpdatablev1" data-formkey="md-statusForm-Health Indicators">
                        </div>
                    </td>
                    <td>
                        <div id="tbMDHealthCost_sf" class="StopLightTB mcttUpdatablev1" data-formkey="md-statusForm-Health Indicators">
                        </div>
                    </td>
                    <td>
                        <div id="tbMDHealthScope_sf" class="StopLightTB mcttUpdatablev1" data-formkey="md-statusForm-Health Indicators">
                        </div>
                    </td>
                    <td>
                        <div id="tbMDHealthRisk_sf" class="StopLightTB mcttUpdatablev1" data-formkey="md-statusForm-Health Indicators">
                        </div>
                    </td>
                    <td>
                        <div id="tbMDHealthIssues_sf" class="StopLightTB mcttUpdatablev1" data-formkey="md-statusForm-Health Indicators">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        `;
    td.append(statusFormIndicators);
}

export function statusFormFieldsHTML(
    targetDiv,
    tbID,
    startP,
    finishP,
    durP,
    costP,
    remCostP,
    hrsP,
    remHrsP,
    pctComplP,
    start,
    finish,
    durF,
    costF,
    hrsF,
    startA,
    finishA,
    durA,
    remDurA,
    costA,
    remCost,
    hrsA,
    remHrs,
    pctCompl,
    deltaStart,
    deltaFinish,
    deltaDur,
    deltaCost,
    deltaHrs,
    deltaStartBg,
    deltaFinishBg,
    deltaDurBg,
    deltaCostBg,
    deltaHrsBg
) {
    let td2 = $(targetDiv);
    let statusFormFields = `

    <!-- Start the status fields section -->
    <div class="statusFormFields">
        <!-- sets width -->
        <table class="allocSchEngTable">
            <thead>
                <tr>
                    <th>
                    </th>
                    <th style="width:85px">Start</th>
                    <th style="width:85px">Finish</th>
                    <th>Dur.</th>
                    <th>Rem Dur.</th>
                    <th>Cost</th>
                    <th>Cost to Go</th>
                    <th>Hours</th>
                    <th>Hours to go</th>
                    <th>% Compl.</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td title="Baseline dates, copy of Forecast (current) dates when baseline is set."
                        class="allocSchEngFormPFA"> Planned <br> <input id="setBaselineFromStatusPopupForm"
                            data-tbid="${tbID}" style="font-size: 10px;" type="button" value="Set Baseline!"
                            title="Set baseline for this Timbar and Timebars linked below it!">
                    </td>
                    <td>
                        <div id="fldStartP_sf">${startP}</div>
                    </td>
                    <td>
                        <div id="fldFinishP_sf">${finishP}</div>
                    </td>
                    <td>
                        <div id="fldDurP_sf"> ${durP}</div>
                    </td>
                    <td>
                        <div id="fldRemDurP_sf">${hrsP} </div>
                    </td>
                    <td>
                        <div id="fldCostP_sf">${costP}</div>
                    </td>
                    <td>
                        <div id="fldRemCostP_sf">${remCostP}</div>
                    </td>
                    <td>
                        <div id="fldHrsP_sf">${hrsP}</div>
                    </td>
                    <td>
                        <div id="fldRemHrsP_sf">${remHrsP}</div>
                    </td>
                    <td>
                        <div id="fldPctComplP_sf">${pctComplP}</div>
                    </td>
                </tr>
                <tr>
                    <td class="allocSchEngFormPFA">
                        Forcast</td>
                    <td>
                        <div id="fldStartF_sf">${start}</div>
                    </td>
                    <td>
                        <div id="fldFinishF_sf">${finish}</div>
                    </td>
                    <td>
                        <div id="fldDurF_sf">${durF}</div>
                    </td>
                    <td>
                        <div id="fldRemDurF_sf">na</div>
                    </td>
                    <td>
                        <div id="fldCostF_sf">${costF}</div>
                    </td>
                    <td>
                        <div id="fldRemCostF_sf">na</div>
                    </td>
                    <td>
                        <div id="fldHrsF_sf">${hrsF}</div>
                    </td>
                    <td>
                        <div id="fldRemHrsF_sf">na</div>
                    </td>
                    <td>
                        <div id="fldPctComplF_sf">na</div>
                    </td>
                </tr>
                <tr>
                    <td class="allocSchEngFormPFA">
                        Actual</td>
                    <td>
                        <div id="fldStartA_sf">${startA}</div>
                    </td>
                    <td>
                        <div id="fldFinishA_sf">${finishA}</div>
                    </td>
                    <td>
                        <div id="fldDurA_sf">${durA}</div>
                    </td>
                    <td>
                        <div id="fldRemDurA_sf">${remDurA}</div>
                    </td>
                    <td>
                        <div id="fldCostA_sf">${costA}</div>
                    </td>
                    <td>
                        <div id="fldRemCostA_sf">${remCost}</div>
                    </td>
                    <td>
                        <div id="fldHrsA_sf">${hrsA}</div>
                    </td>
                    <td>
                        <div id="fldRemHrsA_sf">${remHrs}</div>
    
                    </td>
                    <td>
                        <div id="fldPctComplA_sf">${pctCompl}</div>
                    </td>
                </tr>
                </tr>    
                <tr>
                    <td title="Red indicates late dates comparing Forecast to Planned (aka Current and Baseline)"
                        class="allocSchEngFormPFA">Delta (baseline comparison)</td>
                    <td>
                        <div id="fldDeltaStart_sf" style="color: ${deltaStartBg};">${deltaStart}</div>
                    </td>
                    <td>
                        <div id="fldDeltaFinish_sf" style="color: ${deltaFinishBg};">${deltaFinish}</div>
                    </td>
                    <td>
                        <div id="fldDeltaDur_sf" style="color: ${deltaDurBg};">${deltaDur}</div>
                    </td>
                    <td>
                        <div id="fldDeltaRemDur_sf">na</div>
                    </td>
                    <td>
                        <div id="fldDeltaCost_sf" style="color: ${deltaCostBg};">${deltaCost}</div>
                    </td>
                    <td>
                        <div id="fldDeltaRemCost_sf">na</div>
                    </td>
                    <td>
                        <div id="fldDeltaHrs_sf" style="color: ${deltaHrsBg};">${deltaHrs}</div>
                    </td>
                    <td>
                        <div id="fldDeltaRemHrs_sf">na</div>
                    </td>
                    <td>
                        <div id="fldDeltaPctCompl_sf">na</div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
        `;
    td2.append(statusFormFields);
}

// portfolio bars
export function barPortfolioHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration
) {
    if (tbType === "Portfolio") {
        let showTimeBars = $("#show-timebars");

        var duplicateName = "";
        barWidthPx = Number(barWidthPx);
        if (barWidthPx > 460) {
            duplicateName = tbName;
        } else {
            duplicateName = "";
        }

        let pfHtml = `
        <div class="tbBarDraggable tbBarStackingPf1" id="key${tbID}" style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
            <div class="tbBarResizable">
                <div id="${tbID}" class="tbBarStyleCommon tbBarStylePf" data-bardurfromidb="${tbDuration}" data-tbtype="Portfolio">
                    <p contenteditable="true" class="P1pf P1" id="p1${tbID}">${tbName}</p>
                    <p data-target="slideOutRight" class="P2 sidenav-trigger" id="p2${tbID}">Pf:${tbID} | </p>
                    <p class="P5"  id="p5${tbID}"> Status |</p>
                    <p class="P6 PfMdPopup"  id="p6${tbID}"> Details </p>
                    <p class="PfDupeName">${duplicateName}</p>
                </div>
            </div>
        </div>
        `;
        showTimeBars.append(pfHtml);
    } //end draw pf bands
}

//  project bars
export function barProjectHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration
) {
    let showTimeBars = $("#show-timebars");
    if (tbType === "Project") {
        var duplicateName = "";
        barWidthPx = Number(barWidthPx);
        if (barWidthPx > 460) {
            duplicateName = tbName;
        } else {
            duplicateName = "";
        }
        let pjpgHtml = `
        <div class="tbBarDraggable tbBarStackingProj2" data-selfkey2="${tbSelfKey2}" id="key${tbID}" style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
        <div class="tbBarResizable">
            <div id="${tbID}" class="tbBarStyleCommon tbBarStyleProj" data-bardurfromidb="15" data-tbType="${tbType}">
                <p data-tbid="${tbID}" contenteditable="true" class="P1pj P1" id="p1${tbID}">${tbName}</p>
                <p data-target="slideOutRight" class="P2 sidenav-trigger" id="p2${tbID}">P:${tbID}|</p>
                <p class="P5 PjMdPopup" id="p5${tbID}">Status |</p>
                <p class="P6" id="p6${tbID}">Details |</p>
                <div class="P10pj">Parent:${tbSelfKey2}</div>
                <p class="PjDupeName">${duplicateName}</p>
            </div>
        </div>
    </div>
         `;
        showTimeBars.append(pjpgHtml);
    }
}

// sub project bars
export function barSubProjectHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration
) {
    let showTimeBars = $("#show-timebars");

    var duplicateName = "";
    barWidthPx = Number(barWidthPx);
    if (barWidthPx > 460) {
        duplicateName = tbName;
    } else {
        duplicateName = "";
    }

    if (tbType === "Sub-Project") {
        let subpjHtml = `
        <div class="tbBarDraggable tbBarStackingSubProj2" data-selfkey2="${tbSelfKey2}" id="key${tbID}"
        style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
        <div class="tbBarResizable">
            <div id="${tbID}" class="tbBarStyleCommon tbBarStyleSubProj" data-bardurfromidb="${tbDuration}" data-tbtype="${tbType}">
                <p class="P1spj P1" contenteditable="true" id="p1${tbID}">${tbName}</p>
                <p class="P2spj sidenav-trigger" data-target="slideOutRight" id="p2${tbID}">P:${tbID} | </p>
                <p class="P5 P5spj" id="p1${tbID}"> Status |</p>
                <p class="P6 P6spj SubPjMdPopup" id="p6${tbID}"> Details | </p>
                <p class="P10spj"> Parent:${tbSelfKey2}</p>
                <p class="PspjDupeName">${duplicateName}</p>
            </div>
        </div>
    </div>
     `;
        showTimeBars.append(subpjHtml);
    }
}

// reease
export function barReleaseHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration
) {
    let showTimeBars = $("#show-timebars");
    var duplicateName = "";
    barWidthPx = Number(barWidthPx);
    if (barWidthPx > 460) {
        duplicateName = tbName;
    } else {
        duplicateName = "";
    }
    if (tbType === "Release") {
        let releaseHtml = `
        <div class="tbBarDraggable tbBarStackingRel3" data-selfkey2="${tbSelfKey2}" id="key${tbID}"
        style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
        <div class="tbBarResizable">
            <div id="${tbID}" class="tbBarStyleCommon tbBarStyleRel" data-bardurfromidb="${tbDuration}" data-tbtype="${tbType}">
                <p class="P1rel P1" contenteditable="true" id="p1${tbID}">${tbName}</p>
                <p class="P2rel sidenav-trigger" data-target="slideOutRight" id="p2${tbID}">P:${tbID} | </p>
                <p class="P5 P5rel" id="p1${tbID}"> Status |</p>
                <p class="P6 P6rel releaseMdPopup" id="p6${tbID}"> Details | </p>
                <p class="P10rel"> Parent:${tbSelfKey2}</p>
                <p class="PjDupeName">${duplicateName}</p>
            </div>
        </div>
    </div>
        `;
        showTimeBars.append(releaseHtml);
    }
}

//  Task and MS bars
export function barTaskHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration,
    tbBarColor,
    tbTextColor
) {
    let showTimeBars = $("#show-timebars");

    if (tbType === "Task") {
        var duplicateName = "";
        barWidthPx = Number(barWidthPx);
        if (barWidthPx > 460) {
            duplicateName = tbName;
        } else {
            duplicateName = "";
        }
        let taskHtml = `
        <div class="tbBarDraggable tbBarStackingTask4" data-selfkey2="${tbSelfKey2}" id="key${tbID}"
        style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
        <div class="tbBarResizable">
                <div id="${tbID}" class="tbBarStyleCommon tbBarStyleTaskResDrop" data-bardurfromidb="${tbDuration}"
                data-tbtype="${tbType}">
                    <p contenteditable class="P1t P1" id="p1${tbID}">${tbName}</p>
                    <div data-target="slideOutRight" class="P2t sidenav-trigger" id="p2${tbID}">T:${tbID} | </div>
                    <p class="P5 P5t" id="p5${tbID}"> Status |</p>
                    <p class="P6 P6t" id="p6${tbID}"> Details | </p>
                    <p class="P10"> Parent:${tbSelfKey2} </p>
                    <p class="PjDupeName">${duplicateName}</p>
                </div>
            </div>
        </div>
    </div>
        `;
        tbBarColor = "";
        tbTextColor = "";

        showTimeBars.append(taskHtml);
    }

    // Draw Milestones
    if (tbType === "Milestone") {
        barWidthPx = 40;

        let msHtml = `
        <!-- Milestone -->
        <div class="tbBarDraggable tbBarStackingMile5 taskAndRelDrop" data-selfkey2="${tbSelfKey2}" id="key${tbID}"
            style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
            <div id="${tbID}" class="tbBarStyleMilestone" data-bardurfromidb="${tbDuration}" data-tbtype="${tbType}">
                <p contenteditable="true" class="P1m P1" id="p1${tbID}">${tbName}</p><img class="P6"
                    title="ID: ${tbID}, Parent: ${tbSelfKey2}" src="images/diamondblack.png">
            </div>
            </div>
            `;
        showTimeBars.append(msHtml);
    }
    // gate
    if (tbType === "Gate") {
        barWidthPx = 60;
        let gateHtml = `
        <!-- Gate -->
        <div class="tbBarDraggable tbBarStackingGate5 taskAndRelDrop" data-selfkey2="${tbSelfKey2}" id="key${tbID}"
                style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
                <div id="${tbID}" class="tbBarStyleGate" data-bardurfromidb="${tbDuration}" data-tbtype="${tbType}">
                    <p contenteditable="true" class="P1m P1" id="p1${tbID}">${tbName}</p><img class="P6" title="ID: ${tbID}, Parent: 8"
                        src="images/gateclosedsmall.png">
            </div>
                </div>
                `;
        showTimeBars.append(gateHtml);
    }
}

//  Alloc bars
export function barAllocationHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration,
    aStart
) {
    var showTimeBars = $("#show-timebars");

    var duplicateName = "";
    barWidthPx = Number(barWidthPx);
    if (barWidthPx > 460) {
        duplicateName = tbName;
    } else {
        duplicateName = "";
    }
    if (tbType === "Allocation") {
        let allocHtml = `
        <div data-tbtype="Allocation" data-astart="${aStart}" class="tbBarDraggable tbBarStackingAlloc6" data-selfkey2="${tbSelfKey2}" id="key${tbID}"
                    style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
                    <div class="tbBarResizable">
                        <div id="${tbID}" class="tbBarStyleAlloc" data-bardurfromidb="${tbDuration}" data-selfkey="${tbSelfKey2}" data-tbtype="${tbType}">
                            <p class="P1a" id="p1${tbID}">${tbName}</p>
                            <p class="P7 pUpAllocStatusForm" id="pupForm${tbID}">A:${tbID} Details</p>
                            <p class="PjDupeName">${duplicateName}</p>
                            <p class="P10a"> Parent:${tbSelfKey2} </p>


                        </div>
                    </div>
                </div>
                `;
        showTimeBars.append(allocHtml);
    }
}

//  Tiles
export function barTileHTML(
    tbID,
    tbName,
    tbCoordTop,
    tbSelfKey2,
    tbType,
    barStartPx,
    barWidthPx,
    tbDuration
) {
    var showTimeBars = $("#show-timebars");
    if (tbType === "Tile") {
        let tileHtml = `
        <div class="tbBarDraggable tbBarStackingTile6" id="key${tbID}" style="left: ${barStartPx}px; top:${tbCoordTop}px; width: ${barWidthPx}px;">
                    <div class="tbBarResizable">
                        <div id="${tbID}" class="tbBarStyleTile" data-bardurfromidb="${tbDuration}" data-tbtype="${tbType}">
                            <p contenteditable="true" class="P1tile P1 tbResizable" id="p1${tbID}">${tbName}</p>
                        </div>
                    </div>
                </div>
                `;
        showTimeBars.append(tileHtml);
    }
}
