
// was the todo app from noob coder
// was doc datatableuniversal
// refactored to use tb and idb


/* idb code */
import { openIDBGetStore, setTbidBasedOnKeyUniversal, deleteRowFromIDBUniversal, importJsonDataUniversal, clearObjectStore, createJsonFileSimple } from './tbdatabase';
import { displayMessage } from './focdcommon';



// global variables
const displayDT = $("#displayDtTags");
//const message = $("#msgReporting");
const storeName = 'tbTags'



$('#btnBackupTagData').on('click', function () {
    //  console.log("clicked btnBackupMetadata")
    createJsonFileSimple(storeName)
});



$('#tagTab').on('click', function () {
    console.log("clicked Tag")
    getTags()
    setTimeout(() => {
        console.log("page load get Tag")
        initDtTags()

    }, 300);
});

// appends to dataTableTags
const generateDatatableTags = (Item, ids) => {
    return `<tr id="${ids.listItemID}">
    <td><a title="Edit this Tag" id="${ids.editID}"><i class="material-icons tiny">edit</i></a></td>
        <td id="tbTagID-${Item.tbTagID}"><div>${Item.tbTagID}</div></td>
        <td id="tbTagName-${Item.tbTagID}"><div style="width:200px">${Item.tbTagName}</div></td>
        <td id="tbTagGroup-${Item.tbTagID}"><div style="width:150px">${Item.tbTagGroup}</div></td>
        <td id="tbTagNameShort-${Item.tbTagID}"><div style="width:100px">${Item.tbTagNameShort}</div></td>
        <td id="tbTagPurpose-${Item.tbTagID}"><div style="width:200px">${Item.tbTagPurpose}</div></td>
        
        <td id="tbTagOwner-${Item.tbTagID}">${Item.tbTagOwner}</div></td>
        <td id="tbTagPopular-${Item.tbTagID}">${Item.tbTagPopular}</div></td>
        <td id="tbTagSortOrder-${Item.tbTagID}">${Item.tbTagSortOrder}</div></td>

        <td><button title="Delete this print job" type="button" class="btn-flat" id="${ids.deleteID}"><i class="material-icons">delete_forever</i></button></td>
        </tr>`;
}

// feields removed July 27 2019
// <td id="tbTagDescription-${Item.tbTagID}"><div style="width:150px">${Item.tbTagDescription}</div></td>
//         <td id="tbTagCustomerID-${Item.tbTagID}">${Item.tbTagCustomerID}</div></td>
// <td id="tbTagAzureID-${Item.tbTagID}">${Item.tbTagAzureID}</div></td> 

// new code get all items and display them
const getTags = () => {

    returnAllTags(function (data) {

        displayTags(data);
    })
}

// master very simple function to get all rows in a table store  // later send store name as var
function returnAllTags(callbackTags) {

    openIDBGetStore(storeName, 'readwrite', function (store) {
        var request = store.getAll();
        request.onsuccess = function (evt) {
            var tb = evt.target.result
            // console.log(data)
            callbackTags(tb)
        };
    })
};


// univ form materialize javascript
function launchTagsForm(tagid) {

    // set data-tbid on form for later actions
    var myform = document.getElementById('launchTagsForm')
    var myform = myform.setAttribute('data-tbid', tagid)


    var elem = document.querySelector('#launchTagsForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    //  console.log("launchEmptyTagsForm finished")

}


// univ form
$('#btnLaunchEmptyTagsForm').on('click', function () {
    // open modal first

    launchEmptyTagsForm()
});

// univ form materialize javascript
function launchEmptyTagsForm() {


    var elem = document.querySelector('#launchTagsForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    setTimeout(() => {
        resetTagsInput()
    }, 300);

}


$('#btnCreateTag').on('click', function () {

    confirm("Are you sure, this will create a new Tag. It will be a duplicate.")
    createTag()
});


/*   ----------  START Revised CRUD Operations   ----------- */

function createTag() {
    // let tbTagID = $('#tbTagID').val();
    let tbTagName = $('#tbTagName').val();
    let tbTagGroup = $('#tbTagGroup').val();
    let tbTagNameShort = $('#tbTagNameShort').val();
    let tbTagPurpose = $('#tbTagPurpose').val();
    let tbTagDescription = $('#tbTagDescription').val();

    let tbTagOwner = $('#tbTagOwner').val();
    let tbTagPopular = $('#tbTagPopular').val();
    let tbTagSortOrder = $('#tbTagSortOrder').val();
    let tbTagCustomerID = $('#tbTagCustomerID').val();
    let tbTagAzureID = $('#tbTagAzureID').val();



    let nameCheck = $("#tbTagName").val()
    console.log("Name check " + nameCheck)
    if (nameCheck == '' || nameCheck == ' ') {
        alert("You must enter a Tag Name!")
        return
    }

    /* build up an object */
    var obj = {
        //  tbTagID: tbTagID,
        tbTagName: tbTagName,
        tbTagGroup: tbTagGroup,
        tbTagNameShort: tbTagNameShort,
        tbTagPurpose: tbTagPurpose,
        tbTagDescription: tbTagDescription,

        tbTagOwner: tbTagOwner,
        tbTagPopular: tbTagPopular,
        tbTagSortOrder: tbTagSortOrder,
        tbTagCustomerID: tbTagCustomerID,
        tbTagAzureID: tbTagAzureID,
    };

    let newItemId = ""
    /*  add the Tag */
    openIDBGetStore(storeName, 'readwrite', function (store) {

        var request;
        try {
            request = store.add(obj);
        } catch (e) {
            displayFailureMessage("Some Problem While adding");
            throw e;
        }
        request.onsuccess = function (e) {

            // return the id of the newly added item
            newItemId = request.result

            // now fetch the new record, and generate template stuff
            var request2 = store.get(newItemId);
            request2.onsuccess = function (e) {

                var data = e.target.result;

                console.log("data " + JSON.stringify(data))

                let ids = buildIdsTags(data.id);

                displayDT.append(generateDatatableTags(data, ids));
                fillTagsForm(data, ids.tagID, ids.editID); //not sure why this was here ??, maybe for insterting new into dom
                deleteTag(data, ids.listItemID, ids.deleteID);
                // update tbTagID field to match indexedb key id
                let fieldName = "tbTagID"
                let newVal = String(data.id)
                let uid = Number(data.id)  // keys are numbers in idb, we find the new record by key and update the tbid
                setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal)

                // console.log("ids " + JSON.stringify(ids))
                displayMessage(true, "createTag Result: " + JSON.stringify(tbTagName) + " added!");

            }
        };
        request.onerror = function () {

            displayMessage(true, "createTag Result: " + this.error);
        };
    });

}


const deleteTag = (tag, listItemID, deleteID) => {
    // note may have to modify this to suit tagid as string for now tbTagID and built in id must match
    //  console.log("tag.tbTagID: " + tag.tbTagID + " check!")
    var indexName = "tbTagID"
    var tagid = tag.tbTagID
    // console.log("tagid " + tagid)

    let deleteBtn = $(`#${deleteID}`);
    deleteBtn.click(() => {

        deleteRowFromIDBUniversal(storeName, indexName, tagid)

        $(`#${listItemID}`).remove();
        displayMessage(true, "deleteTag Result: " + listItemID + " deleted!");

    });
}

//  get one fill form first, put show details button, then edit button
const fillTagsForm = (tag, tagID, editID) => {

    //  console.log("fillTagsForm - tag.tbTagID " + tag.tbTagID)  // returns the id without underscore
    //  console.log("fillTagsForm - tagID " + tagID)  // tag_70
    // console.log("fillTagsForm - editID " + editID)  //edit_70

    let editBtn = $(`#${editID}`);
    editBtn.click(() => {

        openIDBGetStore(storeName, 'readonly', function (store) {

            // var request = store.get(tag.tbTagID);
            var tagid = String(tag.tbTagID)

            var index = store.index("tbTagID");
            var request = index.get(tagid);

            request.onerror = function (event) {
                alert("there was error getting store id");
            };
            request.onsuccess = function (event) {

                // here we are returning data to the form
                var data = request.result;
                if (!data) {
                    alert("tag not a valid key, check for string");
                    return;
                }
                /// here for data
                console.log("tbTagName: " + data.tbTagName)

                // populate the form fields

                $("#tbid").val(data.tbTagID);  // universal input on all forms used on focdcommon
                $("#tbTagName").val(data.tbTagName);
                $("#tbTagGroup").val(data.tbTagGroup);
                $("#tbTagNameShort").val(data.tbTagNameShort);
                $("#tbTagPurpose").val(data.tbTagPurpose);
                $('#tbTagPopular').val(data.tbTagPopular);
                $('#tbTagSortOrder').val(data.tbTagSortOrder);
                $('#tbTagDescription').val(data.tbTagDescription);
                $('#tbTagOwner').val(data.tbTagOwner);
                $('#tbTagCustomerID').val(data.tbTagCustomerID);
                $('#tbTagAzureID').val(data.tbTagAzureID);



                //  console.log("data sent to form: " + JSON.stringify(data))
                displayMessage(true, "Form opened, ready for editing!");
                launchTagsForm(tagid)
                // activate the labels on the form
                M.updateTextFields()
                // open or resize textarea fields
                ///    M.textareaAutoResize($('#zzz'));
                // to make this work, simply add class data-length="250" to text area or input
                ///   $('input#zzz, textarea#zzz').characterCounter();

            };
        });
    });


}


$('#btnSaveFormDataTag').on('click', function () {

    saveFormDataTag()
});

// save the edit - only certain fields are editable, not all
const saveFormDataTag = () => {
    // first get form data
    let tbTagName = $("#tbTagName").val()
    let tbTagGroup = $("#tbTagGroup").val()
    let tbTagNameShort = $("#tbTagNameShort").val()
    let tbTagPurpose = $("#tbTagPurpose").val()
    let tbTagDescription = $("#tbTagDescription").val()
    let tbTagOwner = $("#tbTagOwner").val()
    let tbTagPopular = $("#tbTagPopular").val()
    let tbTagSortOrder = $("#tbTagSortOrder").val()
    let tbTagCustomerID = $("#tbTagCustomerID").val()
    let tbTagAzureID = $("#tbTagAzureID").val()

    openIDBGetStore(storeName, 'readwrite', function (store) {

        var tagid = $("#tbid").val()
        console.log("tagid: " + tagid)
        tagid = String(tagid)

        var index = store.index("tbTagID");
        var request = index.get(tagid);
        request.onerror = function (event) {
            // Handle errors!
            alert("there was error getting store id")
        };
        request.onsuccess = function (event) {
            // Get the exist value that we want to update
            var data = request.result;
            //    console.log("request.result " + JSON.stringify(data))

            try {
                // apply editable data fields on form to the data object
                data.tbTagName = tbTagName;
                data.tbTagGroup = tbTagGroup;
                data.tbTagNameShort = tbTagNameShort;
                data.tbTagPurpose = tbTagPurpose;

                data.tbTagDescription = tbTagDescription;
                data.tbTagOwner = tbTagOwner;
                data.tbTagPopular = tbTagPopular;
                data.tbTagSortOrder = tbTagSortOrder;
                data.tbTagCustomerID = tbTagCustomerID;
                data.tbTagAzureID = tbTagAzureID;


            } catch (e) {
                console.log("saveFormDataTag - fail")

                throw e;
                //  return;
            }
            // Put this updated object back into the database.
            var requestUpdate = store.put(data);
            requestUpdate.onerror = function (event) {
                // Do something with the error

                // throw event;
                alert("there was error updating progress in idb")
            };
            requestUpdate.onsuccess = function (event) {

                console.log("Success - Form is updated!")

                // $("#tbid").text(tbTagID);  // universal input on all forms used on focdcommon
                $(`#tbTagName-${tagid}`).text(tbTagName);
                $(`#tbTagGroup-${tagid}`).text(tbTagGroup);
                $(`#tbTagNameShort-${tagid}`).text(tbTagNameShort);
                $(`#tbTagPurpose-${tagid}`).text(tbTagPurpose);
                $(`#tbTagDescription-${tagid}`).text(tbTagDescription);
                $(`#tbTagOwner-${tagid}`).text(tbTagOwner);
                $(`#tbTagPopular-${tagid}`).text(tbTagPopular);
                $(`#tbTagSortOrder-${tagid}`).text(tbTagSortOrder);
                $(`#tbTagCustomerID-${tagid}`).text(tbTagCustomerID);
                $(`#tbTagAzureID-${tagid}`).text(tbTagAzureID);

            };
        }; // end onsuccess
    });
};



function initDtTags() {
    $('#dataTableTags').DataTable({
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',  // f = search 
        "dom": '<"top"f>rt<"bottom"Bilp>',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        select: true,
        colReorder: true,  // change what columns show first, second etc.
        responsive: true,
        fixedHeader: true,
        destroy: true,

    });

};



$('#clearForm').on('click', function () {
    console.log("clicked reset tag")
    resetTagsInput()
});



/* show or hide brief descriptn because it is truncated */
setTimeout(() => {
    $('.showhide').on('dblclick', function () {

        console.log("clicked showhide")

        if ($('.showhide').hasClass('truncate')) {
            $('.showhide').removeClass('truncate');
            $('.showhide').addClass('truncated');
        } else {
            $('.showhide').removeClass('truncated');
            $('.showhide').addClass('truncate');
        }
    });
}, 500);



const resetTagsInput = () => {

    $('#tbTagID').val();
    $("#tbTagName").val('');
    $("#tbTagGroup").val('');
    $("#tbTagNameShort").val('');
    $("#tbTagPurpose").val('');
    $('#tbTagDescription').val('');
    $('#tbTagOwner').val('');
    $('#tbTagPopular').val('');
    $('#tbTagSortOrder').val('');
    $('#tbTagCustomerID').val('');
    $('#tbTagAzureID').val('');

}

const buildIdsTags = (itemid) => {
    return {
        editID: "edit_" + itemid,
        deleteID: "delete_" + itemid,
        listItemID: "listItem_" + itemid,
        tagID: "tag_" + itemid,
        bpselectorID: "bpselector_" + itemid
    }
}




const displayTags = (data) => {


    data.forEach((Item) => {

        //   console.log("displayTags tbTagID: " + Item.tbTagID)
        //   console.log("displayTags Item: " + JSON.stringify(Item))

        let ids = buildIdsTags(Item.tbTagID);

        //   console.log("displayTags ids: " + JSON.stringify(ids))
        // looks like this ids: {"editID":"edit_83","deleteID":"delete_83","listItemID":"listItem_83","tagID":"tag_83","bpselectorID":"bpselector_83"}

        displayDT.append(generateDatatableTags(Item, ids));
        fillTagsForm(Item, ids.tagID, ids.editID);
        deleteTag(Item, ids.listItemID, ids.deleteID);

    });
}


