// Copyright Â© 2018 Timebars Ltd.  All rights reserved.

//---------- START Datatable Editable Reports   ----------

// this has old and new v2 functions for reports and forms etc????
// how to make a split???  maybe this will be legacy, just pull what i need

// how to add new report step by step
// 1. make duplicate of the schedule example code
// 2. set store to which to get data from
// 3. set field names to match index names in your store
// 4. set titles for names of fields to appear in the report instead of field names
// 5. set a link in the Reporting html file, id to match the name of the on click event in line one
// 6. optionally set the target frame and dtID (not implemented fully yet)
// '','','','','','','','','','','','','','','','','','','','',''

/* need to change out the exported field names
"tbID", wdays","sWeekNo","tbWeekNo","tbResID","wkSumHours","wkSumCost"
tbResCalcTbID
tbResCalcTbResID
tbResCalcWdays
tbResCalcStartWeekNo
tbResCalcTbWeekNo
tbResCalcWeekSumHours
tbResCalcWeekSumCost
*/

import {
  getMonday,
  getAdminPanelData,
  drawTimescale,
  openIDBGetStore,
  createJsonFileSimple,
  clearObjectStore,
  importJsonDataUniversal,
  updateOneFieldUniversal
} from "../scripts/tbdatabase";
import moment, { now } from "moment";
import Chart from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";

// START new March 2020 for burndown

/* 

// inut fields to indicate if plan exists or not yet, default place holder is not created, use this to say created...
// first reduce rescalcs down and fill these 5 fields

    bdIsCreatedCurrent   bdIsCreatedPlanned  bdIsCreatedRPlan1 bdIsCreatedRPlan2 bdIsCreatedRPlan3 


// radio buttons, only one active, used for launching graph with correct series planned, r1, 2...
bdRadioPlanned   bdRadioRPlan1   bdRadioRPlan2 bdRadioRPlan3
*/




// step 1 create or recreate rescalcs first, but dont delete oplan nor rplans , then calcl latest or current. run chart curr, if not created detail rescalc rows they will be created
$(document).on("click", ".bdCreateCurrent, .runCurrentForecast", function () {
  runCurrentForecastChart();
  setTimeout(() => {
    fillBurndownCreatedStatus();
  }, 2000);
});

// step 2, we can now create planned and revised plans btnSetCumForecast btnSetCumPlanned    btnSetCumRPlan1
$(document).on("click", ".bdCreatePlanned", function () {
  let seriesName = "Planned";
  generateHrsCostByWeekPerSeries(seriesName);
  setTimeout(() => {
    fillBurndownCreatedStatus();
  }, 2000);
});

$(document).on("click", ".bdCreateRPlan1", function () {
  let seriesName = "RPlan1";
  generateHrsCostByWeekPerSeries(seriesName);
  setTimeout(() => {
    fillBurndownCreatedStatus();
  }, 2000);
});

$(document).on("click", ".bdCreateRPlan2", function () {
  let seriesName = "RPlan2";
  generateHrsCostByWeekPerSeries(seriesName);
  setTimeout(() => {
    fillBurndownCreatedStatus();
  }, 2000);
});

$(document).on("click", ".bdCreateRPlan3", function () {
  let seriesName = "RPlan3";
  generateHrsCostByWeekPerSeries(seriesName);
  setTimeout(() => {
    fillBurndownCreatedStatus();
  }, 2000);
});

// START delete  baselines other than current

$(document).on("click", ".bdDeletePlanned", function () {
  console.log("here");
  let seriesName = "Planned";
  deleteResCalcCummRowsForecastplannedRevised(seriesName);
  $("#bdIsCreatedPlanned").val("Deleted!");
});

$(document).on("click", ".bdDeleteRPlan1", function () {
  let seriesName = "RPlan1";
  deleteResCalcCummRowsForecastplannedRevised(seriesName);
  $("#bdIsCreatedRPlan1").val("Deleted!");
});

$(document).on("click", ".bdDeleteRPlan2", function () {
  let seriesName = "RPlan2";
  deleteResCalcCummRowsForecastplannedRevised(seriesName);
  $("#bdIsCreatedRPlan2").val("Deleted!");
});

$(document).on("click", ".bdDeleteRPlan3", function () {
  let seriesName = "RPlan3";
  deleteResCalcCummRowsForecastplannedRevised(seriesName);
  $("#bdIsCreatedRPlan3").val("Deleted!");
});

// end deleting baselines

//when user clicks a radio button it is persised, default is planned
$(document).on("click", ".bdSetSeries", function () {
  // now re run the chart with new series name
  // get errors with this on
  // runCurrentForecastChart();

  persistChosenSeriesToIDB();

  setTimeout(() => {
    runCurrentForecastChart();
  }, 200);
});

// run chart based on radio buttons selected

function runCurrentForecastChart() {
  deleteResCalcDetailRows();

  // re calculate forecast (deletes detail rescalc rows and replaces with new timebar calcs)
  setTimeout(function () {
    let calcEntity = "Allocation"; // can be tbType Alloc, task, sub-project, project, or Pf
    generateWeeklyDistributions(calcEntity);
  }, 300);

  // create forecast series data
  let seriesName = "Forecast";
  setTimeout(function () {
    generateHrsCostByWeekPerSeries(seriesName);
  }, 1000);

  // determine chart series per radio buttons - pass series name
  // radio in tbMDOther5 on the project
  //bdRadioPlanned   bdRadioRPlan1   bdRadioRPlan2 bdRadioRPlan3
  //let seriesName2 = "RPlan2";

  // set sch visible levels
  var sp = document.getElementById("bdRadioPlanned").checked;
  var sr1 = document.getElementById("bdRadioRPlan1").checked;
  var sr2 = document.getElementById("bdRadioRPlan2").checked;
  var sr3 = document.getElementById("bdRadioRPlan3").checked;

  if (sp === true) {
    var sername = "Planned";
  } else if (sr1 === true) {
    var sername = "RPlan1";
  } else if (sr2 === true) {
    var sername = "RPlan2";
  } else if (sr3 === true) {
    var sername = "RPlan3";
  }
  //console.log("sername " + sername);

  // prep and run the chart
  setTimeout(function () {
    reduceResCalcsForChartDrawBurndown(sername);
  }, 1500);
}

// click radios
function persistChosenSeriesToIDB() {
  var sp = document.getElementById("bdRadioPlanned").checked;
  var sr1 = document.getElementById("bdRadioRPlan1").checked;
  var sr2 = document.getElementById("bdRadioRPlan2").checked;
  var sr3 = document.getElementById("bdRadioRPlan3").checked;

  if (sp === true) {
    var sername = "Planned";
  } else if (sr1 === true) {
    var sername = "RPlan1";
  } else if (sr2 === true) {
    var sername = "RPlan2";
  } else if (sr3 === true) {
    var sername = "RPlan3";
  }

  //console.log("sername " + sername);

  // persist the series value in the db
  // start new burndonw map reduce etc
  let storeName = "tbTimebars";
  let indexName = "tbID";
  var statusFormTbId = document.getElementById("headerForStatusFormBar");
  let tbid = statusFormTbId.getAttribute("data-tbid");
  let fieldName = "tbAzureID";
  let newVal = sername;
  updateOneFieldUniversal(storeName, indexName, tbid, fieldName, newVal);
}

// draw the chart in two steps
// reduce the entire rescalc data set down in one shot
// this is final step, after planned and forecast or rev plan are cummulated data already.
function reduceResCalcsForChartDrawBurndown(seriesName2) {
  // get status data and store with oplan
  getAdminPanelData(function (
    APName,
    apID,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_WeeklyPxFactor,
    apTS_MonthlyPxFactor,
    apStatusDate,
    apCustomerID
  ) {
    let nd = new Date(apStatusDate);
    let stdate = moment(nd).format("DD-MMM-YY");
    let cummsum1 = 0;
    let cummsum2 = 0;
    //  console.log("st date: " + stdate);
    openIDBGetStore("tbResCalcs", "readonly", function (store) {
      let request;
      request = store.getAll();
      request.onsuccess = function (e) {
        let objArr = e.target.result;

        // getPlannedArray(objArr);
        let data = objArr.filter(item => item.tbResCalcName === seriesName2);
        const plannedArray = data.map(item => item.tbResCalcWeekSumHours);
        // remove dupes
        let pvalueArray = plannedArray.reduce(function (
          accumulator1,
          currentValue1
        ) {
          cummsum1 += currentValue1;
          accumulator1.push(cummsum1);

          return accumulator1;
        },
          []);
        //  console.log("pvalueArray array: " + JSON.stringify(pvalueArray));

        // getForecastArray(objArr);
        data = objArr.filter(item => item.tbResCalcName === "Forecast");
        const fcastArray = data.map(item => item.tbResCalcWeekSumHours);
        // remove dupes
        let fvalueArray = fcastArray.reduce(function (
          accumulator2,
          currentValue2
        ) {
          // if (accumulator.indexOf(currentValue) === 1) {
          cummsum2 += currentValue2;
          accumulator2.push(cummsum2);

          // console.log("cummsum: " + JSON.stringify(cummsum));
          // }
          return accumulator2;
        },
          []);
        // console.log("fvalueArray: " + JSON.stringify(fvalueArray));

        // send arrays to draw the burndonw
        drawBurndown1(pvalueArray, fvalueArray, stdate, seriesName2);
      };
      request.onerror = function () {
        //    console.log("failed updateResCalcsFromJSON() " + JSON.stringify(jsonRescalcObjectWeekly.tbResCalcs[0]))
      };
    }); // end get store
  }); // end admin panel
}


// generate cummulative hrs by week, in idb as new data set
// make it universal to store planned, forecast and revised planned
// pass in Oplan1 for original planned, CPlan1 as current plan and RPlanx as revised plans, use in charts as seriesName
// valid series names - Planned, Forecast, RPlanx (x is sequential number, can be many Rplans)
function generateHrsCostByWeekPerSeries(seriesName) {
  // get status data and store with oplan
  getAdminPanelData(function (
    APName,
    apID,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_WeeklyPxFactor,
    apTS_MonthlyPxFactor,
    apStatusDate,
    apCustomerID
  ) {
    let nd = new Date(apStatusDate);
    let stdate = moment(nd).format("DD-MMM-YYYY");
    //  console.log("st date: " + stdate);
    openIDBGetStore("tbResCalcs", "readonly", function (store) {
      let request;
      // request = store.getAll();
      var index = store.index("tbResCalcName");
      request = index.getAll("WeeklyHoursAndCost");

      request.onsuccess = function (e) {
        let objectArray = e.target.result;
        // console.log("all: " + JSON.stringify(objectArray));

        function groupByweek1(objectArray, property) {
          return objectArray.reduce(function (acc, obj) {
            let key = obj[property];
            if (!acc[key]) {
              acc[key] = [];
            }
            acc[key].push(obj);
            return acc;
          }, {});
        }
        let groupedweeks = groupByweek1(objectArray, "tbResCalcWeek");
        //  console.log("groupedweeks: " + JSON.stringify(groupedweeks));
        let allweeks = groupedweeks;
        // my sum function to produce one row per week for hrs and cost burndown
        for (var i in allweeks) {
          // console.log(allweeks[i].length);
          // console.log("i " + i);
          let j = 0;
          let hsum = 0;
          let csum = 0;
          let rcname = seriesName; // this controls if planned, fcast or rev fcast
          let rcid = rcname + "-" + allweeks[i][j].tbResCalcID;
          let custid = allweeks[i][j].tbResCalcCustomerID;
          let wk = allweeks[i][j].tbResCalcWeek;
          // cummulate here
          for (var k in allweeks[i]) {
            let hrs = allweeks[i][k].tbResCalcWeekSumHours;
            hsum += hrs;
            let cst = allweeks[i][k].tbResCalcWeekSumCost;
            csum += cst;
            j = j + 1;
          } // end for each
          // now put each into IDB
          if (wk) {
            updateResCalcsBySeriesName(
              rcname,
              custid,
              wk,
              hsum,
              csum,
              rcid,
              stdate
            );
          }
        }
      };
      request.onerror = function () {
        //    console.log("failed updateResCalcsFromJSON() " + JSON.stringify(jsonRescalcObjectWeekly.tbResCalcs[0]))
      };
    }); // end get store
  }); // end admin panel
}

// plan created status
function fillBurndownCreatedStatus() {
  //  console.log("st date: " + stdate);
  openIDBGetStore("tbResCalcs", "readonly", function (store) {
    let request;
    request = store.getAll();
    request.onsuccess = function (e) {
      let objArr = e.target.result;

      const existPlansArray = objArr.map(item => item.tbResCalcName);
      // remove dupes
      let accumulator = [];
      let jdata = existPlansArray.reduce(function (accumulator, currentValue) {
        if (accumulator.indexOf(currentValue) === -1) {
          accumulator.push(currentValue);
        }
        return accumulator;
      }, []);
      //  console.log("existPlansArray: " + JSON.stringify(jdata));

      for (let i = 0; i < jdata.length; i++) {
        //   console.log(jdata[i]);

        if (jdata[i] === "Forecast") {
          // fill the input field with Exists
          $("#bdIsCreatedCurrent").val("Forecast Created! (current)");
        } else if (jdata[i] === "Planned") {
          $("#bdIsCreatedPlanned").val("Planned Created!");
        } else if (jdata[i] === "RPlan1") {
          $("#bdIsCreatedRPlan1").val("RPlan1 Created!");
        } else if (jdata[i] === "RPlan2") {
          $("#bdIsCreatedRPlan2").val("RPlan2 Created!");
        } else if (jdata[i] === "RPlan3") {
          $("#bdIsCreatedRPlan3").val("RPlan3 Created!");
        }
      }
    };
    request.onerror = function () {
      //    console.log("failed updateResCalcsFromJSON() " + JSON.stringify(jsonRescalcObjectWeekly.tbResCalcs[0]))
    };
  }); // end get store
}

// new submit the json weekly row to the idb, one row at a time (not big summary row)
function updateResCalcsBySeriesName(
  rcname,
  custid,
  wk,
  hsum,
  csum,
  rcid,
  stdate
) {
  //console.log(wklyBurnDownObj);
  let wklyBurnDownObj = {
    tbResCalcID: rcid,
    tbResCalcName: rcname,
    tbResCalcCustomerID: custid,
    tbResCalcWeek: wk, //weekending
    tbResCalcWeekSumHours: hsum,
    tbResCalcWeekSumCost: csum,
    tbResCalcLastModified: stdate
  };
  openIDBGetStore("tbResCalcs", "readwrite", function (store) {
    var request;
    request = store.put(wklyBurnDownObj);
    wklyBurnDownObj = {};
    request.onsuccess = function (evnt) {
      //     console.log("success adding details hours cost")
    };
    request.onerror = function () {
      //    console.log("failed updateResCalcsFromJSON() " + JSON.stringify(jsonRescalcObjectWeekly.tbResCalcs[0]))
    };
  }); // end get store
}

/// end new burndown map reduce etc

// Actual bd chart Get the context of the Chart canvas element we want to select

// END new March 2020 for burndown

// -----------  res calcs by week new universal ----------

// get all allocs and begin looping to pivot and sum hours and cost by week for later reducing
export function generateWeeklyDistributions(calcEntity) {
  // need status date to so we can draw histogram starting at status date
  getAdminPanelData(function (
    APName,
    apID,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_WeeklyPxFactor,
    apTS_MonthlyPxFactor,
    apStatusDate,
    apCustomerID
  ) {
    // this is magic, set running totals in these two json objects (one for hrs and one for cost) then update idb
    var jsonRescalcObject = {};

    // we shall append summ of cost by week to this json object
    jsonRescalcObject = {
      tbResCalcs: [
        {
          tbResCalcID: "10000000",
          tbResCalcCustomerID: "44",
          tbResCalcAzureID: "2",
          tbResCalcName: "HoursSummaryRow",
          tbResCalcWeek1: 0,
          tbResCalcWeek2: 0,
          tbResCalcWeek3: 0,
          tbResCalcWeek4: 0,
          tbResCalcWeek5: 0,
          tbResCalcWeek6: 0,
          tbResCalcWeek7: 0,
          tbResCalcWeek8: 0,
          tbResCalcWeek9: 0,
          tbResCalcWeek10: 0,
          tbResCalcWeek11: 0,
          tbResCalcWeek12: 0,
          tbResCalcWeek13: 0,
          tbResCalcWeek14: 0,
          tbResCalcWeek15: 0,
          tbResCalcWeek16: 0,
          tbResCalcWeek17: 0,
          tbResCalcWeek18: 0,
          tbResCalcWeek19: 0,
          tbResCalcWeek20: 0,
          tbResCalcWeek21: 0,
          tbResCalcWeek22: 0,
          tbResCalcWeek23: 0,
          tbResCalcWeek24: 0,
          tbResCalcWeek25: 0,
          tbResCalcWeek26: 0,
          tbResCalcWeek27: 0,
          tbResCalcWeek28: 0,
          tbResCalcWeek29: 0,
          tbResCalcWeek30: 0,
          tbResCalcWeek31: 0,
          tbResCalcWeek32: 10,
          tbResCalcLastModified: ""
        },
        {
          tbResCalcID: "200000000",
          tbResCalcCustomerID: "44",
          tbResCalcAzureID: "2",
          tbResCalcName: "CostSummaryRow",
          tbResCalcWeek1: 0,
          tbResCalcWeek2: 0,
          tbResCalcWeek3: 0,
          tbResCalcWeek4: 0,
          tbResCalcWeek5: 0,
          tbResCalcWeek6: 0,
          tbResCalcWeek7: 0,
          tbResCalcWeek8: 0,
          tbResCalcWeek9: 0,
          tbResCalcWeek10: 0,
          tbResCalcWeek11: 0,
          tbResCalcWeek12: 0,
          tbResCalcWeek13: 0,
          tbResCalcWeek14: 0,
          tbResCalcWeek15: 0,
          tbResCalcWeek16: 0,
          tbResCalcWeek17: 0,
          tbResCalcWeek18: 0,
          tbResCalcWeek19: 0,
          tbResCalcWeek20: 0,
          tbResCalcWeek21: 0,
          tbResCalcWeek22: 0,
          tbResCalcWeek23: 0,
          tbResCalcWeek24: 0,
          tbResCalcWeek25: 0,
          tbResCalcWeek26: 0,
          tbResCalcWeek27: 0,
          tbResCalcWeek28: 0,
          tbResCalcWeek29: 0,
          tbResCalcWeek30: 0,
          tbResCalcWeek31: 0,
          tbResCalcWeek32: 10,
          tbResCalcLastModified: ""
        }
      ]
    };

    openIDBGetStore("tbTimebars", "readwrite", function (store) {
      var index = store.index("tbType");
      //>>>>>>>>>>>>>>>>>>>> we could do calcs on sub projects for example also
      // var tbType = "Sub-Project"
      var tbType = calcEntity;
      var range = IDBKeyRange.only(tbType);

      let nd = new Date(apStatusDate);
      let stdate = moment(nd).format("DD-MMM-YYYY");
      var statusDate = new Date(apStatusDate);
      var YrWk = String(getWeekNumber(statusDate));

      YrWk = YrWk.split(",");
      var sdYear = Number(YrWk[0]);
      var sdWeek = Number(YrWk[1]);

      //console.log("Status Date YrWk: " + YrWk);

      if (sdWeek == 52) {
        console.log("reached week 52");
      }
      //  console.log("iterating bar type: " + tbType + " note statusDate year : " + sdYear + " statusDate week: " + sdWeek)

      // console.log("iterating sub projects for timescale start: " + apTS_Start);

      /*           var statusDate = new Date(apStatusDate);
                      var sdYear = Number(moment(statusDate).year())
                      var sdWeek = Number(moment(statusDate).week())
                      var sdDay = Number(moment(statusDate).day()) */

      index.openCursor(range).onsuccess = function (event) {
        var cursor = event.target.result;
        if (cursor) {
          var tbID = cursor.value.tbID;
          var tbResID = cursor.value.tbResID;
          var tsStart = new Date(apTS_Start);
          var tsStartWeekNo = Number(moment(tsStart).week());
          var tbStart = new Date(cursor.value.tbStart);
          var tbFinish = new Date(cursor.value.tbFinish);

          var hrs = Number(cursor.value.tbWork);
          var jcost = Number(cursor.value.tbCost);

          var workingDays = Number(getWorkingDays(tbStart, tbFinish)); // will not include holdiays later
          var costPerDay = jcost / workingDays;
          var hoursPerDay = hrs / workingDays;
          var calDays = moment(tbFinish).diff(moment(tbStart), "days") + 1;

          // first root out finished bars and set inflight bars start date to = status date

          if (tbFinish > statusDate) {
            if (tbStart <= statusDate) {
              //  console.log(tbID + ": is inflight so start date is status date " )
              tbStart = statusDate;
              distributeWeeklyStep1(
                tbID,
                tbStart,
                tbFinish,
                costPerDay,
                hoursPerDay,
                calDays,
                sdWeek,
                sdYear,
                workingDays,
                tbResID,
                stdate
              );
            } else if (tbStart >= statusDate) {
              //  console.log(tbID + ": is in future")
              distributeWeeklyStep1(
                tbID,
                tbStart,
                tbFinish,
                costPerDay,
                hoursPerDay,
                calDays,
                sdWeek,
                sdYear,
                workingDays,
                tbResID,
                stdate
              );
            }
          } else {
            console.log(tbID + ": bar is in past, will not roll it up");
          }
          cursor.continue();
        } else {
          //   console.log("1 jsonRescalcObject: " + JSON.stringify(jsonRescalcObject));
          updateResCalcsFromJSON(jsonRescalcObject); // iteration stops and done pushing to array so can update idb

          //    console.log(myData)
        }
      }; // end open cursor
    }); // end get store
    //};

    // pass in one bar at a time, figure out what week each day is in and push to arr

    // i am going to try to push this to an array and pop into idb
    var myData = [];

    // actually pivots data with javascript objects
    function distributeWeeklyStep1(
      tbID,
      tbStart,
      tbFinish,
      costPerDay,
      hoursPerDay,
      calDays,
      sdWeek,
      sdYear,
      workingDays,
      tbResID,
      stdate
    ) {
      var tbStartTemp = new Date(tbStart);
      var YrWk2 = String(getWeekNumber(tbStartTemp));
      YrWk2 = YrWk2.split(",");
      var sYearNo = Number(YrWk2[0]);
      var sWeekNo = Number(YrWk2[1]);

      var rDayNo = Number(moment(tbStart).day());
      var rDate = moment(tbStart);
      var tbWeekNo = 1;
      var weeklySumArrCost = [];
      var weeklySumArrHours = [];
      var wkSumCost = 0;
      var wkSumHours = 0;
      var uid = 1;
      //   console.log("tbID:" + tbID + ", tbWeekNo:" + tbWeekNo + ", hoursPerDay:" + hoursPerDay + ", costPerDay:" + costPerDay + ", rDate:" + rDate + ", tbStart:" + tbStart)
      //  console.log("calDays:" + calDays + ", costPerDay:" + costPerDay)

      if (sYearNo > sdYear) {
        // bar start is in next year so need sWeekNo will be 1
        sWeekNo = sWeekNo + 51;
      }

      // knowing how many weeks a bar may be in, we loop and inspect each week for qty days billable, then calc cost/hours for that week
      for (var i = 0; i < calDays; i++) {
        if (rDayNo != 0 && rDayNo != 6) {
          // need to push tbWeekNo and sum $/day to array
          //lets do both cost and hours here AnimationPlaybackEvent, need two wksum running values
          wkSumHours += hoursPerDay;
          wkSumCost += costPerDay;

          //  console.log("tbWeekNo: " + tbWeekNo + ", rDayNo: " + rDayNo)
          if (rDayNo == 5) {
            // push the weekly total on fridays
            /////////// bug found here (have to subtract current week)

            // new issue found with this method, need to know year now
            weeklySumArrHours.push(
              String(sWeekNo - sdWeek + 1) + ":" + String(wkSumHours.toFixed(4))
            );
            weeklySumArrCost.push(
              String(sWeekNo - sdWeek + 1) + ":" + String(wkSumCost.toFixed(4))
            );

            let weekending = moment()
              .year(2020)
              .week(String(sWeekNo - 1))
              .day("Friday")
              .format("D-MMM-YYYY");

            var timeByWeek =
              "tbID: " +
              tbID +
              ", wdays: " +
              workingDays +
              ", sWeekNo: " +
              sWeekNo +
              ", tbWeekNo: " +
              tbWeekNo +
              ", tbResID " +
              tbResID +
              ", wkSumHours " +
              wkSumHours +
              ", wkSumCost " +
              wkSumCost; //+ "}"
            // console.log(timeByWeek)

            var tbResCalcID = tbID + ":" + tbResID + ":" + uid;
            var myObj = {
              tbResCalcTbResID: tbID,
              tbResCalcID: tbResCalcID,
              tbResCalcName: "WeeklyHoursAndCost",
              tbResCalcCustomerID: "44",
              tbResCalcWeek: weekending,
              tbResCalcWdays: workingDays,
              tbResCalcStartWeekNo: sWeekNo,
              tbResCalcTbWeekNo: tbWeekNo,
              tbResCalcTbResID: tbResID,
              tbResCalcWeekSumHours: wkSumHours,
              tbResCalcWeekSumCost: wkSumCost,
              tbResCalcLastModified: stdate
            };

            myData.push(myObj);

            var jsonRescalcObjectWeekly =
              '{"tbResCalcs": [' + JSON.stringify(myObj) + "]}";
            //  console.log(JSON.parse(jsonRescalcObjectWeekly).tbResCalcs[0])

            updateResCalcs(JSON.parse(jsonRescalcObjectWeekly));

            wkSumHours = 0;
            wkSumCost = 0;
            uid += 1;
          }
        }

        if (rDayNo == 6) {
          // 6 us sunday, end of week so increment by 1 for the next week
          tbWeekNo = tbWeekNo + 1; // go to next tb week where status date is week 1
          sWeekNo = sWeekNo + 1; // scheduled week no per start date
        }

        rDate = moment(rDate).add(1, "days"); // running date
        rDayNo = Number(moment(rDate).day()); // running day number where sunday is 6 last day of week
      } // end for loop

      makeJSONForHoursUpdateIdbRow(weeklySumArrHours, sdWeek, stdate);
      makeJSONForCostUpdateIdbRow(weeklySumArrCost, sdWeek, stdate);
    }

    // takes the sums by week in an array, splits into json for updating
    function makeJSONForHoursUpdateIdbRow(weeklySumArrHours, sdWeek, stdate) {
      //  console.log("weeklySumArrHours: " + weeklySumArrHours)

      for (var i = 0; i < weeklySumArrHours.length; i++) {
        var col = weeklySumArrHours[i].split(":");
        var weekno = col[0]; //- (sdWeek + 1) // the + 1 is temporary till fix draw function
        var hVal = col[1];
        setHrsCalcs(weekno, hVal);
      }

      // running total of weekly values inside the json object
      function setHrsCalcs(weekno, hVal) {
        //   console.log("jsonRescalcObject: " + JSON.stringify(jsonRescalcObject));

        //    console.log("weekno " + weekno + ", hVal " + hVal)

        for (var i = 0; i < jsonRescalcObject.tbResCalcs.length; i++) {
          if (
            jsonRescalcObject.tbResCalcs[0]["tbResCalcName"] ==
            "HoursSummaryRow"
          ) {
            //alert(jsonRescalcObject.tbResCalcs[0]['tbResCalcWeek' + (wk)] )
            weekno = String(weekno);
            jsonRescalcObject.tbResCalcs[0]["tbResCalcWeek" + weekno] =
              Number(
                jsonRescalcObject.tbResCalcs[0]["tbResCalcWeek" + weekno]
              ) + Number(hVal);
            // march 12 2020 set modif date as status date from admin panel
            jsonRescalcObject.tbResCalcs[0]["tbResCalcLastModified"] = stdate;
            // return;
          }
        }
      }
    }

    // takes the sums by week in an array, splits into json for updating
    function makeJSONForCostUpdateIdbRow(weeklySumArrCost, sdWeek, stdate) {
      //  console.log("arr: " + weeklySumArrCost)
      for (var i = 0; i < weeklySumArrCost.length; i++) {
        var col = weeklySumArrCost[i].split(":");
        var weekno = col[0]; //- (sdWeek + 1) // the + 1 is temporary till fix draw function
        var cVal = col[1];
        setCostCalcs(weekno, cVal);
      }
      // running total of weekly values inside the json object
      function setCostCalcs(weekno, cVal) {
        //   console.log("jsonRescalcObject: " + JSON.stringify(jsonRescalcObject));
        // console.log("weekno " + weekno + ", cVal " + cVal)

        for (var i = 0; i < jsonRescalcObject.tbResCalcs.length; i++) {
          if (
            jsonRescalcObject.tbResCalcs[1]["tbResCalcName"] == "CostSummaryRow"
          ) {
            weekno = String(weekno);
            jsonRescalcObject.tbResCalcs[1]["tbResCalcWeek" + weekno] =
              Number(
                jsonRescalcObject.tbResCalcs[1]["tbResCalcWeek" + weekno]
              ) + Number(cVal);
            // march 12 2020 set modif date
            jsonRescalcObject.tbResCalcs[1]["tbResCalcLastModified"] = stdate;
          }
        }
      }
    }

    // submit the json summary to the idb, one row at a time
    function updateResCalcs(jsonRescalcObjectWeekly) {
      openIDBGetStore("tbResCalcs", "readwrite", function (store) {
        var request;

        request = store.put(jsonRescalcObjectWeekly.tbResCalcs[0]);

        request.onsuccess = function (evnt) {
          //     console.log("success adding details hours cost")
        };
        request.onerror = function () {
          //    console.log("failed updateResCalcsFromJSON() " + JSON.stringify(jsonRescalcObjectWeekly.tbResCalcs[0]))
        };
      }); // end get store
    }

    // temp final step to submit the json summary to the idb
    function updateResCalcsFromJSON(jsonRescalcObject) {
      openIDBGetStore("tbResCalcs", "readwrite", function (store) {
        var request;

        request = store.put(jsonRescalcObject.tbResCalcs[0]);

        request.onsuccess = function (evnt) {
          console.log("success adding hours resCalcData set json object");
        };
        request.onerror = function () {
          console.log("failed updateResCalcsFromJSON() ");
        };
      }); // end get store

      // bad practice, use one funciton later
      openIDBGetStore("tbResCalcs", "readwrite", function (store) {
        var request;

        request = store.put(jsonRescalcObject.tbResCalcs[1]);

        request.onsuccess = function (evnt) {
          console.log("success adding cost resCalcData set json object to 0?");
        };
        request.onerror = function () {
          console.log("failed updateResCalcsFromJSON() ");
        };
      }); // end get store
    }
  }); // end get admin panel

  //alert("Demand calculating, see bottom left links to show demand graphs")
}

// delete all current rescalcs
export function deleteResCalcDetailRows() {
  openIDBGetStore("tbResCalcs", "readwrite", function (store) {
    var rowWithResCalcsForBarGraph = "WeeklyHoursAndCost";

    store.openCursor().onsuccess = function (event) {
      var cursor = event.target.result;
      if (cursor) {
        var rcid = cursor.value.tbResCalcID;
        rcid = String(rcid);

        if (
          // this is important, we always recalculate Forecast, so we delete, refresh, recalc the following
          (cursor.value.tbResCalcName === "WeeklyHoursAndCost") |
          (cursor.value.tbResCalcName === "CostSummaryRow") |
          (cursor.value.tbResCalcName === "HoursSummaryRow") |
          (cursor.value.tbResCalcName === "Forecast")
        ) {
          deleteResCalcByID(rcid);
        }

        cursor.continue();
      } else {
      }
    };
  }); // end get store
}

// delete all rescalcs
export function deleteResCalcCummRowsForecastplannedRevised(seriesName) {
  openIDBGetStore("tbResCalcs", "readwrite", function (store) {
    store.openCursor().onsuccess = function (event) {
      var cursor = event.target.result;
      if (cursor) {
        var rcid = cursor.value.tbResCalcID;
        rcid = String(rcid);

        if (cursor.value.tbResCalcName === seriesName) {
          deleteResCalcByID(rcid);
        }

        cursor.continue();
      } else {
      }
    };
  }); // end get store
}

// delete a ResCalc item by id
function deleteResCalcByID(tbResCalcID) {
  var tbResCalcID = tbResCalcID;
  openIDBGetStore("tbResCalcs", "readwrite", function (store) {
    var index = store.index("tbResCalcID");
    var request = index.get(tbResCalcID);

    request.onsuccess = function (evnt) {
      var data = event.target.result;
      //var del = delete (data.id)
      store.delete(data.id);
    };
    request.onerror = function (evnt) {
      console.log(
        "error deleting deleteResCalcByID, but will add the value now to fix error"
      );
    };
  });
}

// get bar graph settings - Call this function like this getBarGraphSettings(function (NumberOfPeoplePerLine){ });
function getBarGraphSettings(callbackBarGraphSettings) {
  //first get db and open store
  openIDBGetStore("tbAdminPanel", "readwrite", function (store) {
    var index = store.index("ap_name");
    var request = index.get("Bar Graph Settings");

    request.onerror = function (event) {
      //   Handle errors!
      alert("there was error getting std config settings from ");
    };
    request.onsuccess = function (event) {
      var data = request.result;
      //// note.innerHTML += '<li>Success config settings ' + data.ap_name + ' From app core for getValidAPID </li>';
      // need try catch here when db does not exist yet.

      var numberOfPeoplePerLine = data.numberOfPeoplePerLine;
      var pxPerPerson = data.pxPerPerson;

      callbackBarGraphSettings(numberOfPeoplePerLine, pxPerPerson);
    };
  });
}

// --- end res calcshhh

// start main ui portion, put on top later

// make dm form draggable
$(document).on("dblclick", ".dmDropArea", function (e) {
  $("#divDataManagementArea").draggable();
});

// disable draggable double click on gray tabs area
$(document).on("dblclick", "#dmTabsContainerTop", function (e) {
  $("#divDataManagementArea").draggable({
    disabled: true
  });
});

// open/close data management component windows
$(document).on("click", ".btnopenDataManagementWindow", function (e) {
  // openDataManagementWindow();
  showHideDmTabs()
});

// control show hide of dm tabs
export function showHideDmTabs() {
  var x = document.getElementById("divDataManagementArea");
  if (x.style.display === "none") {
    x.style.display = "block";
    makeDmTabsResizable()
  } else {
    x.style.display = "none";
  }
}


// close dm tabs
$(document).on("click", "#btnCloseDMWindow", function (e) {
  //  document.getElementById("divDataManagementArea").style.display = "none";
  showHideDmTabs()
});


// make ti draggable resizable
function makeDmTabsResizable() {
  // document.getElementById("divDataManagementArea").style.display = "block";

  /*   setTimeout(function () {
      $("#divDataManagementAreaxxx").draggable({
        grid: [10, 10],
        snap: true,
        opacity: 0.75,
        cursor: "move",
        start: function (event, ui) { },
        stop: function (event, ui) { }
      }); //end draggable
    }, 300);
   */
  setTimeout(function () {
    $("#divDataManagementArea").resizable({
      grid: [10, 10],
      handles: "e, w, n, s",
      distance: 9, // prevents accidental sizing
      stop: function (event, ui) {
        var offset = $(this).offset();
      }
    });
  }, 600);
}
// end main ui portion

// ---------- START Draw bar graph html5   ---------

// v2 dashboard link (Executive Summary) pop up in draggable window
$(document).on("click", "#rptPowerBIDashboard", function (e) {
  var dtTargetArea = "pbiTargetArea";
  var embedCode =
    '<iframe width="933" height="700" src="https://app.powerbi.com/view?r=eyJrIjoiZjA3MjI2OTAtZTdiMS00NDU3LWFhZjktZTMwN2IwOWM0NjEwIiwidCI6IjE3N2M2ODUyLTcxZGMtNDI4Mi05NTY2LWNkYmJlY2JhYjlhOSJ9" frameborder="0" allowFullScreen="true"></iframe>';
  embedPowerBI(dtTargetArea, embedCode);

  makePbiReportDraggle();
});

function makePbiReportDraggle() {
  document.getElementById("pbiTargetArea").style.visibility = "visible";

  setTimeout(function () {
    $("#pbiTargetArea").draggable({
      grid: [10, 10],
      snap: true,
      opacity: 0.75,
      cursor: "move",

      start: function (event, ui) { },

      stop: function (event, ui) { }
    }); //end draggable
  }, 300);
}

// burndown chart v2 sidenav
$(document).on("click", "#rptPowerBIProjectsDashboard", function (e) {
  var dtTargetArea = "pbiTargetArea";
  var embedCode =
    '<iframe width="1100" height="800" src="https://app.powerbi.com/view?r=eyJrIjoiMDEyNzlhZWMtYjc1ZC00M2RiLWE0ZDYtZmVlYzQ3ODgzMDY0IiwidCI6IjE3N2M2ODUyLTcxZGMtNDI4Mi05NTY2LWNkYmJlY2JhYjlhOSJ9" frameborder="0" allowFullScreen="true"></iframe>';
  embedPowerBI(dtTargetArea, embedCode);
  makePbiReportDraggle();
});
//

// below is not used in v2

$(document).on("click", "#rptPowerBISupplyDemand", function (e) {
  var link =
    "https://app.powerbi.com/groups/6fc1ab1a-bf48-4838-8738-946c0e00acb3/list/reports";
  window.open(link, "_blank");
});

function embedPowerBI(dtTargetArea, embedCode) {
  var targetDiv = "";
  dtTargetArea = "#" + dtTargetArea;

  // initial html for the datatable
  var strVar = "";
  strVar += '    <div class="boxPbiReport" >';
  // strVar += "        <div id=\"divForReportTopTitle\" style=\"height: 10px; text-align: left; font-size: large;\">Timebars Lt.<\/div>";
  strVar += embedCode;
  strVar += "    </div>";
  // append the html to the target div in main report page
  //alert(dtTargetArea)
  targetDiv = $(dtTargetArea);
  targetDiv.empty().append(strVar);
}

// V2 schedule example
$(document).on("click", "#btnCoreDatatable", function (e) {
  var storeTableName = "tbTimebars";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableM3";
  var fields = [
    "tbID",
    "tbSelfKey2",
    "tbName",
    "tbType",
    "tbStart",
    "tbFinish",
    "tbWork",
    "tbAWork",
    "tbWorkRemaining",
    "tbDuration",
    "tbRemainingDuration",
    "tbAStart",
    "tbAFinish",
    "tbPercentComplete",
    "tbCalendar",
    "tbResID"
  ];
  var titles = [
    "tbID",
    "Parent",
    "Name",
    "Type",
    "Start",
    "Finish",
    "Hours",
    "AHours",
    "Rem Hours",
    "Dur",
    "Rem Dur",
    "AStart",
    "AFinish",
    "% Compl.",
    "Calendar",
    "ResID"
  ];
  // var titles = ['tbID', 'Parent', 'Name', 'Type', 'Owner', 'Start', 'Finish', 'Duration', 'Remaining Dur', 'AStart', 'AFinish', '% Complete', 'Calendar', 'Resource ID']
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Schedule Report ");
}); // end schedule example

// draw the table fields, no data
function rptUniversalTableHtml(fields, titles, dtTargetArea, dtID) {
  var targetDiv = "";
  dtTargetArea = "#" + dtTargetArea;

  // initial html for the datatable
  var strVar = "";
  strVar += '    <div class="boxAutoxx" >';
  strVar +=
    '        <div class="reportTopTitle"id="divForReportTopTitle">Timebars Data </div>';
  strVar += '        <table id="';
  strVar += dtID; //"tbTableM3"
  strVar += '"cellspacing ="0">';
  strVar += "            <thead>";
  strVar += "                <tr>";

  // field name title from above array automation here
  for (var i = 0, l = titles.length; i < l; i++) {
    // console.log(titles[i]);
    var row = "<th>" + titles[i] + "</th>";
    //  console.log(row)
    strVar += row;
  }

  // final header html for the datatable
  strVar += "                </tr>";
  strVar += "            </thead>";
  strVar += "            <tfoot>";
  strVar += "                <tr>";

  // footer field automation here

  for (var i = 0, l = titles.length; i < l; i++) {
    //console.log(titles[i]);
    var row = "<th>" + titles[i] + "</th>";
    // console.log(row)
    strVar += row;
  }
  // final footer html for the datatable
  strVar += "                </tr>";
  strVar += "            </tfoot>";
  strVar += "        </table>";
  strVar += "    </div>";
  // append the html to the target div in main report page
  //alert(dtTargetArea)
  targetDiv = $(dtTargetArea);

  targetDiv.empty().append(strVar);
}

// generate the field array as expected by datatables
function rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID) {
  //console.log("fields: " + fields[0])
  var arrFields = [];
  for (var i = 0, l = fields.length; i < l; i++) {
    arrFields.push({
      data: fields[i]
    });
  }

  {
  }

  returnAllTimebars(storeTableName, function (timebars) {
    dtID = "#" + dtID;
    var table = $(dtID).DataTable({
      aaData: timebars,

      aoColumns: arrFields,

      deferRender: false,
      dom: "Bfrtip",
      buttons: ["copy", "excel", "pdf"],
      ordering: true,
      order: [[0, "dsc"]],
      select: true,
      fixedHeader: true,
      fixedColumns: {
        leftColumns: 4
      },
      colReorder: true, // change what columns show first, second etc.
      responsive: true
    }); // end datatables

    // this draws the vertical hover
    $(dtID + " tbody").on("mouseenter", "td", function () {
      var colIdx = table.cell(this).index().column;

      $(table.cells().nodes()).removeClass("highlight");
      $(table.column(colIdx).nodes()).addClass("highlight");
    });

    // use this standard to open a form
    $("#tbTableDoc tbody").on("click", "tr", function () {
      var data = table.row(this).data();
      // console.log("You clicked on " + data["tbDocID"] + "'s row");
      var tbDocID = data["tbDocID"];
      var tbID = String(tbDocID);

      // launchUniversalForm() // not using univ form now, see universaldatatables.js

      drawDocForm(tbID);
      populateDocForm(tbID);
      $("#tbDocForm").css("visibility", "visible");
    });

    // open metadata form use this standard to open a form
    $("#tbTableMD tbody").on("click", "tr", function () {
      var data = table.row(this).data();
      //  console.log("tbTableMD You clicked on " + data["tbMDID"] + "'s row");
      var tbMDID = data["tbMDID"];
      tbMDID = String(tbMDID);

      var tbID = tbMDID;
      initMetadataForm(tbID);

      $("#mdFormAdvL1").css("visibility", "visible");
    });
  });
}

// very simple function to get all rows in a table store
function returnAllTimebars(storeTableName, callbackTimebars) {
  //storeTableName = 'tbTimebars'
  openIDBGetStore(storeTableName, "readwrite", function (store) {
    var request = store.getAll();
    request.onsuccess = function (evt) {
      var tb = evt.target.result;
      // console.log(data)
      callbackTimebars(tb);
    };
  });
}

/* --------   START draw doc form */

function drawDocForm(tbID) {
  // console.log("from drawDocForm tbID " + tbID);
  var strVar = "";
  strVar += "  <!-- doc form -->";
  strVar += '  <div id="docform1" class="docformContainerL2" data-formtbid="';
  strVar += tbID;
  strVar += '-doc">';
  /* start html here */
  strVar +=
    '    <div style="Display: inline" class="docformHeaderBand1">Document</div> ';
  strVar += '    <Span id="tbID"></Span>';
  strVar +=
    '    <input class="tbButtonSmall" type="button" id="docCreateNew" value="Create New Document" />';
  strVar +=
    '    <input class="tbButtonSmall" type="button" id="docformDraggable" value="Make Draggable" />';
  strVar +=
    '    <input class="tbButtonSmall" type="button" id="docformRefresh" value="Make Editable" />';
  strVar +=
    '    <input class="tbButtonSmall" type="button" id="docformDelete" value="Delete" />';
  strVar +=
    '    <input class="tbButtonSmall" type="button" id="docformClose" value="Close" />';
  strVar += "    <hr>";
  strVar += '    <div class="docformHeaderBand2">Department Information </div>';
  strVar += "";
  strVar += "    <!--  first set of fields -->";
  strVar += '    <div class="docformDiv2">';
  strVar += "      <!-- first field -->";
  strVar += '      <div class="docformDiv2">';
  strVar += '        <div class="docformDiv2 docformDiv2LabelArea">';
  strVar += "          Document Name:";
  strVar += "        </div>";
  strVar +=
    '        <div id="tbDocName" class="docformDiv2 docformDiv2FieldArea updatableStd" contenteditable="true" data-formkey="1-doc-NA">';
  strVar += "          enter data here";
  strVar += "        </div>";
  strVar += "      </div>";
  strVar += '      <div class="docformDiv2">';
  strVar += '        <div class="docformDiv2 docformDiv2LabelArea">';
  strVar += "          Document ID:";
  strVar += "        </div>";
  strVar +=
    '        <div id="tbDocID" class="docformDiv2 docformDiv2FieldArea"  data-formkey="1-doc-NA">';
  strVar += "          enter ID here";
  strVar += "        </div>";
  strVar += "      </div>";
  strVar += '      <div class="docformDiv2">';
  strVar += '        <div class="docformDiv2LabelArea docformDiv2">';
  strVar += "          Brief Description:";
  strVar += "        </div>";
  strVar +=
    '        <div id="tbDocBriefDescription" class="docformDiv2 docformDiv2FieldArea updatableStd" contenteditable="true" data-formkey="1-doc-na">';
  strVar += "          enter brier here";
  strVar += "        </div>";
  strVar += "      </div>";
  strVar += "    </div>";
  strVar +=
    '  <div><a href="https://wordhtml.com/" target="_blank" >Click here to clean HTML</a></div>';
  strVar +=
    '       <div id="tbDocHTMLContent" class="docformDiv2 docformDiv2FieldArea updatableEditor" data-formkey="1-doc-na">';
  strVar += "          enter brier here";
  strVar += "        </div> ";
  strVar += "";
  strVar += "    <!-- </div> -->";
  strVar += '    <div class="jspacer"></div>';
  strVar += '    <div class="docformDiv6">';
  strVar += '      <div class="docformDiv6">';
  strVar +=
    '        <div title="Enter short name to match metadata values. See help file." class="docformDiv6 docformDiv6LabelArea">Name Key:</div>';
  strVar +=
    '        <div id="tbDocNameShort" class="docformDiv6 docformDiv6FieldArea updatableStd" contenteditable="true" data-formkey="1-doc-Document Type">enter data here</div>';
  strVar +=
    '        <div class="docformDiv6 docformDiv6LabelArea">Status:</div>';
  strVar +=
    '        <div id="tbDocStatus" class="docformDiv6 docformDiv6FieldArea mcttUpdatable" contenteditable="false" data-formkey="1-doc-Document Status">enter data here</div>';
  strVar +=
    '        <div class="docformDiv6 docformDiv6LabelArea">Purpose:</div>';
  strVar +=
    '        <div id="tbDocPurpose" class="docformDiv6 docformDiv6FieldArea mcttUpdatable" contenteditable="false" data-formkey="1-doc-Document Purpose">enter data here</div>';
  strVar += "      </div>";
  strVar += "";
  strVar += "    </div>";
  strVar += " ";
  strVar += "    <!--  third set of fields -->";
  strVar += "    <hr>";
  strVar += '    <div class="docformHeaderBand2">Approvals and Sign Off</div>';
  strVar += '    <div class="docformDiv2">';
  strVar += '      <div class="docformDiv2">';
  strVar += '        <div class="docformDiv2LabelArea docformDiv2">';
  strVar += "          Written by:";
  strVar += "        </div>";
  strVar +=
    '        <div id="tbDocWrittenBy" class="docformDiv2 docformDiv2FieldArea mcttUpdatable" contenteditable="true" data-formkey="1-doc-Business Advisors">';
  strVar += "          enter who wrote by data here";
  strVar += "        </div>";
  strVar += "      </div>";
  strVar += "";
  strVar += '      <div class="docformDiv2">';
  strVar += '        <div class="docformDiv2 docformDiv2LabelArea">';
  strVar += "          Approved by:";
  strVar += "        </div>";
  strVar +=
    '        <div id="tbDocApprovedBy" class="docformDiv2 docformDiv2FieldArea mcttUpdatable" contenteditable="false" data-formkey="1-doc-Business Owners">';
  strVar += "          enter data here";
  strVar += "        </div>";
  strVar += "      </div>";
  strVar += '      <div class="docformDiv2">';
  strVar += '        <div class="docformDiv2 docformDiv2LabelArea">';
  strVar += "          Last modifed by:";
  strVar += "        </div>";
  strVar +=
    '        <div id="tbDocLastModified" class="docformDiv2 docformDiv2FieldArea mcttUpdatable" contenteditable="false" data-formkey="1-doc-Business Advisors">';
  strVar += "          enter data here";
  strVar += "        </div>";
  strVar += "      </div>";
  strVar += "    </div>";
  strVar += "    <hr>";
  strVar += '    <div class="docformHeaderBand2">';
  strVar +=
    '      <input class="tbButtonSmall" type="button" id="docformClose" value="Close" />';
  strVar += "    </div>";
  strVar += "  </div>";

  /* end html here */

  var showDocForm = $("#tbDocForm");
  showDocForm.empty();
  showDocForm.append(strVar);
  strVar = "";
}

// fill doc form with data
function populateDocForm(tbDocID) {
  console.log("tbDocID " + tbDocID);

  var tbDocID = String(tbDocID);
  openIDBGetStore("tbDocuments", "readonly", function (store) {
    var index = store.index("tbDocID");
    var request = index.get(tbDocID);

    request.onerror = function (event) {
      // Handle errors!
      alert("there was error getting store id");
    };
    request.onsuccess = function (event) {
      // Get the old value that we want to update
      var data = request.result;
      if (!data) {
        alert("doc form pop not a valid key, check for string");
        return;
      }
      // this is where we fill form
      document.querySelector("#tbDocID").innerHTML = data.tbDocID;
      document.querySelector("#tbID").innerHTML = "ID:" + data.tbDocID;
      document.querySelector("#tbDocName").innerHTML = data.tbDocName;
      document.querySelector("#tbDocBriefDescription").innerHTML =
        data.tbDocBriefDescription;
      document.querySelector("#tbDocNameShort").innerHTML = data.tbDocNameShort;
      document.querySelector("#tbDocHTMLContent").innerHTML =
        data.tbDocHTMLContent;
      /*  document.querySelector('#tbDocOwner').innerHTML = data.tbDocOwner */
      document.querySelector("#tbDocApprovedBy").innerHTML =
        data.tbDocApprovedBy;
      document.querySelector("#tbDocStatus").innerHTML = data.tbDocStatus;
      document.querySelector("#tbDocWrittenBy").innerHTML = data.tbDocWrittenBy;
      document.querySelector("#tbDocPurpose").innerHTML = data.tbDocPurpose;
      document.querySelector("#tbDocLastModified").innerHTML =
        data.tbDocLastModified;
    };
  });
}

/* END draw doc form */

// schedule Report example
$(document).on("click", "#rptSchedule1", function (e) {
  var storeTableName = "tbTimebars";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableM3";
  var fields = [
    "tbID",
    "tbSelfKey2",
    "tbName",
    "tbType",
    "tbStart",
    "tbFinish",
    "tbWork",
    "tbAWork",
    "tbWorkRemaining",
    "tbDuration",
    "tbRemainingDuration",
    "tbAStart",
    "tbAFinish",
    "tbPercentComplete",
    "tbCalendar",
    "tbResID"
  ];
  var titles = [
    "tbID",
    "Parent",
    "Name",
    "Type",
    "Start",
    "Finish",
    "Hours",
    "AHours",
    "Rem Hours",
    "Dur",
    "Rem Dur",
    "AStart",
    "AFinish",
    "% Compl.",
    "Calendar",
    "ResID"
  ];
  // var titles = ['tbID', 'Parent', 'Name', 'Type', 'Owner', 'Start', 'Finish', 'Duration', 'Remaining Dur', 'AStart', 'AFinish', '% Complete', 'Calendar', 'Resource ID']
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Schedule Report ");
}); // end schedule example

// td meta data  REport
$(document).on("click", "#rptMetaData1", function (e) {
  var storeTableName = "tbMetaData";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableMD";
  var fields = [
    "tbMDID",
    "tbMDCategory",
    "tbMDContact",
    "tbMDDepartment",
    "tbMDExSponsor",
    "tbMDHealth",
    "tbMDLocation",
    "tbMDName",
    "tbMDNotes",
    "tbMDPM",
    "tbMDPhase",
    "tbMDPriority",
    "tbMDProduct",
    "tbMDProjectNumber",
    "tbMDProjectType",
    "tbMDResponsibility",
    "tbMDSeverity",
    "tbMDShowIn",
    "tbMDSortOrder",
    "tbMDStage",
    "tbMDState",
    "tbMDStatus",
    "tbMDWBS",
    "tbMDWeighting",
    "tbMDYesNoSelector",
    "tbMDtbLastModified"
  ]; //'tbMDDescription',
  var titles = [
    "tbID",
    "Category",
    "Contact",
    "Department",
    "ExSponsor",
    "Health",
    "Location",
    "Name",
    "Notes",
    "PM",
    "Phase",
    "Priority",
    "Product",
    "Project Number",
    "Project Type",
    "Responsibility",
    "Severity",
    "ShowIn",
    "SortOrder",
    "Stage",
    "State",
    "Status",
    "WBS",
    "Weighting",
    "YesNoSelector",
    "Last Modified"
  ];
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Metadata Report ");
});

// tags display report
$(document).on("click", "#rptTags1", function (e) {
  var storeTableName = "tbTags";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableM3";
  var fields = [
    "tbTagID",
    "tbTagName",
    "tbTagNameShort",
    "tbTagPopular",
    "tbTagPurpose",
    "tbTagSortOrder",
    "tbTagGroup"
  ];
  var titles = [
    "TagID",
    "Tag Name",
    "Name Short",
    "Is Popular",
    "Tag Purpose",
    "Sort Order",
    "Tag Group"
  ];
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Tag Listing ");
});

// tbResources Report
$(document).on("click", "#rptResources", function (e) {
  var storeTableName = "tbResources";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableM3";
  var fields = [
    "tbResID",
    "tbResName",
    "tbResNameShort",
    "tbResDepartment",
    "tbResManager",
    "tbResPartTimeFullTime",
    "tbResCostCode",
    "tbResPayRate",
    "tbResResourceCalendar",
    "tbResPercentGeneralAvailability",
    "tbResPrimaryRole",
    "tbResPrimarySkill",
    "tbResResourceClass",
    "tbResResourceType",
    "tbResSupervisor",
    "tbResTeam",
    "tbResTeamLeader"
  ];
  var titles = [
    "ResID",
    "Name",
    "Name Short",
    "Department",
    "Manager",
    "Parttime/Fulltime",
    "Cost Code",
    "Pay Rate",
    "Resource Calendar",
    "% General Availability",
    "Primary Role",
    "Primary Skill",
    "Class",
    "Resource Type",
    "Supervisor",
    "Team",
    "Team Leader"
  ];
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Resource Listing ");
});

// tbconfig Admin Panel
$(document).on("click", "#rptScheduleConfig", function (e) {
  var storeTableName = "tbAdminPanel";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableM3";
  var fields = [
    "apID",
    "ap_name",
    "apTS_WeeklyPxFactor",
    "apTS_WeeklyOrMonthly",
    "apTS_Start",
    "apStatusDate",
    "ap_HolidayDate"
  ];
  // , 'apSchMethod', 'apFilteredFromTbID', , 'apFilteredFrom' , 'apTbVisibleLevel'
  var titles = [
    "apID",
    "Name",
    "Pixel Factor",
    "Weekly Or Monthly",
    "Timescale Start",
    "Status Date",
    "Holiday Date"
  ];
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("App Config Listing ");
});

// tbTimebars Cost report
$(document).on("click", "#rptCostReport1", function (e) {
  var storeTableName = "tbTimebars";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableM3";
  var fields = [
    "tbID",
    "tbSelfKey2",
    "tbName",
    "tbType",
    "tbCost",
    "tbACost",
    "tbCostRemaining",
    "tbPercentComplete",
    "tbCostType",
    "tbOwner",
    "tbResID",
    "tbPayRate"
  ];
  var titles = [
    "ID",
    "Parent",
    "Name",
    "Type",
    "Cost",
    "Acost",
    "Cost Rmg",
    "% Compl",
    "Cost Type",
    "Owner",
    "Resource ID",
    "Rate of Pay"
  ];
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Cost Report ");
});

// docs display
$(document).on("click", "#rptDocs1", function (e) {
  showDocsDataTable();
});

function showDocsDataTable() {
  var storeTableName = "tbDocuments";
  var dtTargetArea = "dtTargetArea1";
  var dtID = "tbTableDoc";
  var fields = [
    "tbDocID",
    "tbDocName",
    "tbDocNameShort",
    "tbDocStatus",
    "tbDocPurpose",
    "tbDocWrittenBy",
    "tbDocBriefDescription"
  ];
  var titles = [
    "DocID",
    "Doc Name",
    "Name Key",
    "Status",
    "Doc Purpose",
    "Written By",
    "Brief Description"
  ];
  rptUniversalTableHtml(fields, titles, dtTargetArea, dtID);
  rptMakeUniversalTimebarsDatatable(storeTableName, fields, dtID);

  // button here to add new document
  var strVar =
    '<div"><input class="btn btn-default buttons-excel buttons-html5" type="button" id="docCreateNewFromReportPage" value="Create NewDocument"/></div>';
  var targetDiv = $(divForReportTopTitle);
  targetDiv.text("Document Listing ");
  targetDiv.append(strVar);
}

//old
// non automated fields datatable, cloned this for universal reporting system
$(document).on("click", "#rptSch1x", function (e) {
  rptTableHtml();
  makeTimebarsDatatable();
});

function makeTimebarsDatatable() {
  returnAllTimebars(function (timebars) {
    //console.log(timebars)
    // add search box to each column on top
    //$('#tbTableM3 thead th').each(function (i) {
    //    var title0 = $('#tbTableM3 thead th').eq($(this).index()).text();
    //    $(this).html('<input type="text" placeholder="Search ' + title0 + '" data-index="' + i + '" />');
    //});

    var table = $("#tbTableM3").DataTable({
      aaData: timebars,

      aoColumns: [
        {
          data: "tbID"
        },
        {
          data: "tbName"
        },
        {
          data: "tbType"
        },
        {
          data: "tbSelfKey2"
        },
        {
          data: "tbStart"
        },
        {
          data: "tbFinish"
        },
        {
          data: "tbDuration"
        },
        {
          data: "tbCoordTop"
        },
        {
          data: "tbCoordLeft"
          //}, {
          //   "data": "tbResID"
        },
        {
          data: "tbCalendar"
        },
        {
          data: "tbPercentTimeOn"
          //  }, {
          //      "data": "tbCustomerID"
        },
        {
          data: "tbRemainingDuration"
        },
        {
          data: "tbOwner"
          //}, {
          //   "data": "tbMetaDataID"
          // }, {
          //   "data": "tbBLID"
          // }, {
          //    "data": "tbPredecessor"
        },
        {
          data: "tbAStart"
        },
        {
          data: "tbAFinish"
        },
        {
          data: "tbWork"
        },
        {
          data: "tbAWork"
        },
        {
          data: "tbWorkRemaining"
        },
        {
          data: "tbPercentComplete"
          // }, {
          //    "data": "tbConstraintType"
          // }, {
          //      "data": "tbConstraintDate"
          //   }, {
          //      "data": "tbFloat"
          //  }, {
          //     "data": "tbFreeFloat"
        },
        {
          data: "tbExpHoursPerWeek"
          //  }, {
          //      "data": "tbCostID"
          //}, {
          //    "data": "tbCost"
          //}, {
          //    "data": "tbCostRemaining"
          //}, {
          //    "data": "tbACost"
        },
        {
          data: "tbPayRate"
          //}, {
          //    "data": "tbCostType"
          //}, {
          //    "data": "canvasNo"
          //}, {
          //    "data": "tbAzureID"
          //}, {
          //    "data": "id"
        }
      ],

      pageLength: 1000,
      lengthChange: true,
      responsive: false,
      fixedHeader: false,
      fixedColumn: false,
      scrollX: false,

      deferRender: false,
      dom: "liBrt",
      buttons: [
        {
          extend: "colvis",
          text: "Visibility"
        },
        {
          extend: "excelHtml5",
          text: "Export to Excel"
        },
        {
          extend: "csvHtml5",
          text: "Export to CSV"
        },
        {
          extend: "copyHtml5",
          text: "Copy to Clipboard"
        }
      ]
    }); // end datatables

    // Apply the search boxes
    //table.columns().every(function () {
    //    var that = this;

    //    $('input', this.header()).on('keyup change', function () {
    //        if (that.search() !== this.value) {
    //            that
    //                .search(this.value)
    //                .draw();
    //        }
    //    });
    //});
  }); // end returnAllTimebars
}
//old
function rptTableHtml() {
  var strVar = "";
  strVar += '    <div class="boxAutoxxx" id="divForTableM1">';
  strVar +=
    '        <div style="text-align: left; font-size:large;">Timebars Raw Data</div>';
  strVar +=
    '        <table width="100%" class="displayxx" id="tbTableM3" cellspacing="0">';
  strVar += "            <thead>";
  strVar += "                <tr>";
  strVar += "                    <th>ID</th>";
  strVar += "                    <th>Name</th>";
  strVar += "                    <th>Type</th>";
  strVar += "                    <th>Parent</th>";
  strVar += "                    <th>Start</th>";
  strVar += "                    <th>Finish</th>";
  strVar += "                    <th>Duration</th>";
  strVar += "                    <th>tbCoordTop</th>";
  strVar += "                    <th>tbCoordLeft</th>";
  strVar += "                    <!--<th>tbResID</th>-->";
  strVar += "                    <th>tbCalendar</th>";
  strVar += "                    <th>tbPercentTimeOn</th>";
  strVar += "                    <!--<th>tbCustomerID</th>-->";
  strVar += "                    <th>tbRemainingDuration</th>";
  strVar += "                    <th>tbOwner</th>";
  strVar += "                    <!--<th>tbMetaDataID</th>";
  strVar += "<th>tbBLID</th>";
  strVar += "<th>tbPredecessor</th>-->";
  strVar += "                    <th>tbAStart</th>";
  strVar += "                    <th>tbAFinish</th>";
  strVar += "                    <th>tbWork</th>";
  strVar += "                    <th>tbAWork</th>";
  strVar += "                    <th>tbWorkRemaining</th>";
  strVar += "                    <th>tbPercentComplete</th>";
  strVar += "                    <!--<th>tbConstraintType</th>";
  strVar += "<th>tbConstraintDate</th>";
  strVar += "<th>tbFloat</th>";
  strVar += "<th>tbFreeFloat</th>-->";
  strVar += "                    <th>tbExpHoursPerWeek</th>";
  strVar += "                    <!--<th>tbCostID</th>";
  strVar += "<th>tbCost</th>";
  strVar += "<th>tbCostRemaining</th>";
  strVar += "<th>tbACost</th>";
  strVar += "<th>tbPayRate</th>";
  strVar += "<th>tbCostType</th>";
  strVar += "<th>canvasNo</th>";
  strVar += "<th>tbAzureID</th>";
  strVar += "<th>id</th>-->";
  strVar += "                </tr>";
  strVar += "            </thead>";
  strVar += "            <tfoot>";
  strVar += "                <tr>";
  strVar += "                    <th>ID</th>";
  strVar += "                    <th>Name</th>";
  strVar += "                    <th>Type</th>";
  strVar += "                    <th>Parent</th>";
  strVar += "                    <th>Start</th>";
  strVar += "                    <th>Finish</th>";
  strVar += "                    <th>Duration</th>";
  strVar += "                    <th>tbCoordTop</th>";
  strVar += "                    <th>tbCoordLeft</th>";
  strVar += "                    <!--<th>tbResID</th>-->";
  strVar += "                    <th>tbCalendar</th>";
  strVar += "                    <th>tbPercentTimeOn</th>";
  strVar += "                    <!--<th>tbCustomerID</th>-->";
  strVar += "                    <th>tbRemainingDuration</th>";
  strVar += "                    <th>tbOwner</th>";
  strVar += "                    <!--<th>tbMetaDataID</th>";
  strVar += "<th>tbBLID</th>";
  strVar += "<th>tbPredecessor</th>-->";
  strVar += "                    <th>tbAStart</th>";
  strVar += "                    <th>tbAFinish</th>";
  strVar += "                    <th>tbWork</th>";
  strVar += "                    <th>tbAWork</th>";
  strVar += "                    <th>tbWorkRemaining</th>";
  strVar += "                    <th>tbPercentComplete</th>";
  strVar += "                    <!--<th>tbConstraintType</th>";
  strVar += "<th>tbConstraintDate</th>";
  strVar += "<th>tbFloat</th>";
  strVar += "<th>tbFreeFloat</th>-->";
  strVar += "                    <th>tbExpHoursPerWeek</th>";
  strVar += "                    <!--<th>tbCostID</th>";
  strVar += "<th>tbCost</th>";
  strVar += "<th>tbCostRemaining</th>";
  strVar += "<th>tbACost</th>";
  strVar += "<th>tbPayRate</th>";
  strVar += "<th>tbCostType</th>";
  strVar += "<th>canvasNo</th>";
  strVar += "<th>tbAzureID</th>";
  strVar += "<th>id</th>-->";
  strVar += "                </tr>";
  strVar += "            </tfoot>";
  strVar += "        </table>";
  strVar += "    </div>";

  var targetDiv = $("#rptTable");
  targetDiv.append(strVar);
}

//---------- END Datatable Editable Reports   ----------

// ------------ START timephases cost and hours  --------

$(document).on("click", "#showHoursGraph", function () {
  var canvasId = "tbCanvas";
  var canvasHeight = window.innerHeight;
  var tsCurrentWidth = window.innerWidth * 3;
  var canvas = document.getElementById(canvasId),
    context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height);

  let lastScrollY = 0;
  drawTimescale(lastScrollY);

  getAdminPanelData(function (
    APName,
    apID,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_WeeklyPxFactor,
    apTS_MonthlyPxFactor,
    apStatusDate,
    apCustomerID
  ) {
    drawHoursBarGraph(
      apTS_Start,
      apTS_WeeklyOrMonthly,
      apTS_WeeklyPxFactor,
      apTS_MonthlyPxFactor,
      apStatusDate
    );
  });
});

$(document).on("click", "#showCostGraph", function () {
  var canvasId = "tbCanvas";
  var canvasHeight = window.innerHeight;
  var tsCurrentWidth = window.innerWidth * 3;
  var canvas = document.getElementById(canvasId),
    context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height);

  let lastScrollY = 0;
  drawTimescale(lastScrollY);

  getAdminPanelData(function (
    APName,
    apID,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_WeeklyPxFactor,
    apTS_MonthlyPxFactor,
    apStatusDate,
    apCustomerID
  ) {
    drawCostBarGraph(
      apTS_Start,
      apTS_WeeklyOrMonthly,
      apTS_WeeklyPxFactor,
      apTS_MonthlyPxFactor,
      apStatusDate
    );
  });
});

// this is launched from red chart icon on tool bar
$(document).on("click", "#btnDemand", function () {
  var canvasId = "tbCanvas";
  var canvasHeight = window.innerHeight;
  var tsCurrentWidth = window.innerWidth * 3;
  var canvas = document.getElementById(canvasId),
    context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height);

  let lastScrollY = 0;
  drawTimescale(lastScrollY);
  // does not delete planned and revised plans
  deleteResCalcDetailRows();

  let calcEntity = "Allocation"; // can be tbType Alloc, task, sub-project, project, or Pf

  setTimeout(function () {
    generateWeeklyDistributions(calcEntity);
  }, 500);

  setTimeout(function () {
    getAdminPanelData(function (
      APName,
      apID,
      apTS_WeeklyOrMonthly,
      apTS_Start,
      apTS_Finish,
      apTS_WeeklyPxFactor,
      apTS_MonthlyPxFactor,
      apStatusDate,
      apCustomerID
    ) {
      drawHoursBarGraph(
        apTS_Start,
        apTS_WeeklyOrMonthly,
        apTS_WeeklyPxFactor,
        apTS_MonthlyPxFactor,
        apStatusDate
      );
    });
  }, 1000);

  // put this in sch engine

  //  rollUpAll()
});

$(document).on("click", "#clearGraphs", function () {
  var canvasId = "tbCanvas";
  var canvasHeight = window.innerHeight;
  var tsCurrentWidth = window.innerWidth * 3;
  var canvas = document.getElementById(canvasId),
    context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height);

  let lastScrollY = 0;
  drawTimescale(lastScrollY);
});

// Show BarGraph controller,double click BarGraph to launch dialog to change  scale
$("#barGraphDoubleClickArea").dblclick(function () {
  //alert("here")
  document.getElementById("barGraphController").style.visibility = "visible";
  $(function () {
    var options = {};
    $("#barGraphController").toggle("blind", options, 500);
  });
});

function getWeekNumber(d) {
  // Copy date so don't modify original
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
  // Get first day of year
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  // Calculate full weeks to nearest Thursday
  var weekNo = Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
  // Return array of year and week number
  return [d.getUTCFullYear(), weekNo];
}

// html 5 draw cost bar graph
function drawCostBarGraph(
  apTS_Start,
  dailyWeeklyMonthlyValue,
  apTS_WeeklyPxFactor,
  apTS_MonthlyPxFactor,
  apStatusDate
) {
  console.log("draw cost bar graph");

  var apTS_Start = apTS_Start;
  var dailyWeeklyMonthlyValue = dailyWeeklyMonthlyValue;
  var apTS_WeeklyPxFactor = apTS_WeeklyPxFactor;
  var apTS_MonthlyPxFactor = apTS_MonthlyPxFactor;

  // figure out px from left where status date starts so we can start first bar graph
  apTSStart = getMonday(new Date(apTS_Start));
  statusDate = getMonday(new Date(apStatusDate));
  statusDateDaysFromLeft = parseInt(
    (statusDate - apTSStart) / (24 * 3600 * 1000)
  );
  statusDatePxFromLeft = statusDateDaysFromLeft * apTS_WeeklyPxFactor;

  // get the canvas element using the DOM
  var canvas = document.getElementById("tbCanvas");
  if (canvas.getContext) {
    var ctx = canvas.getContext("2d");
    var bgVertLineInterval = apTS_WeeklyPxFactor * 7;
    // var pxBottom = window.innerHeight; oct 9 2017 to move up the bar graph

    var pxBottom = window.innerHeight - 41;
    var bgLineWidth = 0.5;
    var bgLinecolor = "blue";
    var maxWidth = window.innerWidth * 3;
    var maxBars = Number(maxWidth) / Number(bgVertLineInterval);

    // start draw 3 "over allocatin" horiz lines

    var pxFromLeft = 5;
    var scaleInDollars = 20; // or "20K" per line;
    var pxPerLine = 20;
    var pxLineNext = 0; //pxPerLine
    var maxDollarValue = 400;
    var numberOfHorizLines = maxDollarValue / pxPerLine;

    // draw max burn rate line
    ctx.font = "normal 10pt Calibri";
    ctx.fillStyle = "#67720e";
    ctx.lineWidth = 1;
    ctx.strokeStyle = "red";
    ctx.fillText("Max Burn Rate", 40, pxBottom - scaleInDollars * 4 - 2);
    ctx.beginPath();
    ctx.moveTo(pxFromLeft, pxBottom - scaleInDollars * 4);
    ctx.lineTo(maxWidth, pxBottom - scaleInDollars * 4);
    ctx.stroke();

    ctx.lineWidth = bgLineWidth;
    ctx.strokeStyle = bgLinecolor;

    for (var i = 1; i < numberOfHorizLines + 1; i++) {
      ctx.beginPath();
      ctx.moveTo(pxFromLeft, pxBottom - pxLineNext);
      ctx.lineTo(maxWidth, pxBottom - pxLineNext);

      // draw scale y axis 15k, 30k etc
      ctx.font = "normal 10pt Calibri";
      ctx.fillStyle = "#67720e";
      ctx.fillText(pxLineNext + "K", 10, pxBottom - 2 - pxLineNext);
      ctx.stroke();
      pxLineNext = pxLineNext + pxPerLine;
    }
    // need to ddraw line at 80,000

    // it draws left then top
    //  ctx.closePath(); take back to 0,0 position
    openIDBGetStore("tbResCalcs", "readwrite", function (store) {
      var index = store.index("tbResCalcName");
      var tbResCalcSrow = "CostSummaryRow";
      //var req = index.get(tbResCalcID);

      var req = index.openCursor(IDBKeyRange.only(tbResCalcSrow)); // will need a variable for weeks

      req.onsuccess = function (evnt) {
        var cursor = evnt.target.result;
        var CalcWeekValue = "";
        if (cursor) {
          //console.log("tbResCalcID: " + tbResCalcID)

          for (var i = 1; i < maxBars; i++) {
            var CalcWeek = "tbResCalcWeek" + String(i);

            //  console.log("Week: " + CalcWeek);

            CalcWeekValue = cursor.value[CalcWeek];

            // change hours to K, divide by the coxt in a week 1000 typically
            CalcWeekValue = Number(CalcWeekValue) / 1000;

            var pxTopOfBar = CalcWeekValue; //* pxPerPerson

            //   console.log("pxCalcWeekValue: " + pxCalcWeekValue)

            var bgEdge = bgVertLineInterval * i + statusDatePxFromLeft - 45;

            //   console.log("bgEdge: " + bgEdge)

            var bgVertLineWidth = bgEdge + 20;

            ctx.lineWidth = bgLineWidth;
            ctx.strokeStyle = bgLinecolor;
            ctx.fillStyle = "red";
            ctx.beginPath();
            ctx.moveTo(bgEdge, pxBottom);
            ctx.lineTo(bgEdge, pxBottom - pxTopOfBar);
            ctx.lineTo(bgVertLineWidth, pxBottom - pxTopOfBar);
            ctx.lineTo(bgVertLineWidth, pxBottom);

            //    console.log("pxTopOfBar: " + pxTopOfBar + " # Of People: " + pxTopOfBar / pxPerPerson)

            ctx.closePath();
            ctx.stroke();
            ctx.fill();
          } // end of for
          cursor.continue();
        } // end of if cursor
      }; // end of req.onsuccess
    }); // end of open store

    //   }); //  end get admin panel bar graph settings
  } // end of canvas if statement
}

// html 5 draw hours bar graph
function drawHoursBarGraph(
  apTS_Start,
  dailyWeeklyMonthlyValue,
  apTS_WeeklyPxFactor,
  apTS_MonthlyPxFactor,
  apStatusDate
) {
  console.log("draw hours bar graph");

  // get the canvas element using the DOM
  var canvas = document.getElementById("tbCanvas");
  if (canvas.getContext) {
    var ctx = canvas.getContext("2d");
    var bgVertLineInterval = apTS_WeeklyPxFactor * 7;
    // var pxBottom = window.innerHeight; oct 9 2017 to move up the bar graph

    var pxBottom = window.innerHeight - 41;
    var bgLineWidth = 0.5;
    var bgLinecolor = "blue";
    var maxWidth = window.innerWidth * 3;
    var maxBars = Number(maxWidth) / Number(bgVertLineInterval);

    // start draw 3 "over allocatin" horiz lines

    getBarGraphSettings(function (numberOfPeoplePerLine, pxPerPerson) {
      var numberOfPeoplePerLine = numberOfPeoplePerLine;
      var pxPerPerson = pxPerPerson;
      console.log(
        "pxPerPerson " +
        pxPerPerson +
        " numberOfPeoplePerLine " +
        numberOfPeoplePerLine
      );

      var numberOfPeopleLine1 = Number(numberOfPeoplePerLine) * 1;
      var numberOfPeopleLine2 = Number(numberOfPeoplePerLine) * 2;
      var numberOfPeopleLine3 = Number(numberOfPeoplePerLine) * 3;

      var pxFromLeft = 5;
      var numberOfPeopleTextLine1 = numberOfPeopleLine1 + " People";
      var numberOfPeopleTextLine2 = numberOfPeopleLine2 + " People";
      var numberOfPeopleTextLine3 = numberOfPeopleLine3 + " People";

      var pxLine1 = numberOfPeopleLine1 * pxPerPerson;
      var pxLine2 = numberOfPeopleLine2 * pxPerPerson;
      var pxLine3 = numberOfPeopleLine3 * pxPerPerson;

      // figure out px from left where status date starts so we can start first bar graph
      let apTSStart = getMonday(new Date(apTS_Start));
      let statusDate = getMonday(new Date(apStatusDate));
      let statusDateDaysFromLeft = parseInt(
        (statusDate - apTSStart) / (24 * 3600 * 1000)
      );
      let statusDatePxFromLeft = statusDateDaysFromLeft * apTS_WeeklyPxFactor;

      //  console.log("st " + statusDatePxFromLeft)

      ctx.lineWidth = bgLineWidth;
      ctx.strokeStyle = bgLinecolor;
      // ctx.fillStyle = "red";
      ctx.beginPath();
      ctx.moveTo(pxFromLeft, pxBottom - pxLine1);
      ctx.lineTo(maxWidth, pxBottom - pxLine1);
      ctx.moveTo(pxFromLeft, pxBottom - pxLine2);
      ctx.lineTo(maxWidth, pxBottom - pxLine2);
      ctx.moveTo(pxFromLeft, pxBottom - pxLine3);
      ctx.lineTo(maxWidth, pxBottom - pxLine3);
      ctx.stroke();

      ctx.font = "normal 10pt Calibri";
      ctx.fillStyle = "#67720e";
      ctx.fillText(numberOfPeopleTextLine1, 10, pxBottom - 22);
      ctx.fillText(numberOfPeopleTextLine2, 10, pxBottom - 42);
      ctx.fillText(numberOfPeopleTextLine3, 10, pxBottom - 62);

      // it draws left then top
      //  ctx.closePath(); take back to 0,0 position
      openIDBGetStore("tbResCalcs", "readwrite", function (store) {
        var index = store.index("tbResCalcName");
        var tbResCalcSrow = "HoursSummaryRow";
        //var req = index.get(tbResCalcID);

        var req = index.openCursor(IDBKeyRange.only(tbResCalcSrow)); // will need a variable for weeks

        req.onsuccess = function (evnt) {
          var cursor = evnt.target.result;
          var CalcWeekValue = "";
          if (cursor) {
            //console.log("tbResCalcID: " + tbResCalcID)

            for (var i = 1; i < maxBars; i++) {
              var CalcWeek = "tbResCalcWeek" + String(i);

              //  console.log("Week: " + CalcWeek);

              CalcWeekValue = cursor.value[CalcWeek];

              // change hours to FTE, divide by the hours in a week 37.5 typically
              CalcWeekValue = Number(CalcWeekValue) / 37.5;

              var pxTopOfBar = (CalcWeekValue * pxPerPerson) / 2; // not sure why had to divide by 2 here, fix later

              //   console.log("pxCalcWeekValue: " + pxCalcWeekValue)

              // var bgEdge = (bgVertLineInterval * i) + 25;
              var bgEdge = bgVertLineInterval * i + statusDatePxFromLeft - 45;

              //   console.log("bgEdge: " + bgEdge)

              var bgVertLineWidth = bgEdge + 20;

              ctx.lineWidth = bgLineWidth;
              ctx.strokeStyle = bgLinecolor;
              ctx.fillStyle = "red";
              ctx.beginPath();
              ctx.moveTo(bgEdge, pxBottom);
              ctx.lineTo(bgEdge, pxBottom - pxTopOfBar);
              ctx.lineTo(bgVertLineWidth, pxBottom - pxTopOfBar);
              ctx.lineTo(bgVertLineWidth, pxBottom);

              //    console.log("pxTopOfBar: " + pxTopOfBar + " # Of People: " + pxTopOfBar / pxPerPerson)

              ctx.closePath();
              ctx.stroke();
              ctx.fill();
            } // end of for
            cursor.continue();
          } // end of if cursor
        }; // end of req.onsuccess
      }); // end of open store
    }); //  end get admin panel bar graph settings
  } // end of canvas if statement
} // end of function drawBarGraph

// -------------   END Rar graphs and res calcs  ----------------
