// important notes about new changes in tbV2. This file will host database specific
// code to create db, tables and populate config data only. config data is in data folder
// as json file. It will populate admin panel, doc table, res table and medata data tables
// with standard minimal data.
// Detailed Customer data json files are stored in backend api.

// todo at line 1535 - need to make the rest universal for any table, pass in table name

/* setup */
//import $ from "jquery"; dont need, now in as linked resources from cdn
//import jQuery from "jquery";
//import "jquery-ui";

const json2csv = require("json2csv").parse;
// import the default json app data and limited demo data
import {
  getMetadataJsonData,
  getResourcesJsonData,
  getDocumentJsonData,
  getTagsJsonData
} from "./tbdata";



import moment from "moment";

const fileSaver = require("file-saver");
const DB_NAME = "TimebarsIDB";
const DB_VERSION = 6;
const STORE_RESOURCES = "tbResources";
const STORE_TAGS = "tbTags";
const STORE_METADATA = "tbMetaData";
const STORE_DOCS = "tbDocuments";

// export to csv button on slide out and on button bar
$(document).on('click', '.btnExportAlltoCSV', function () {
  exportAllDataAsCSV()
});

// Toast success fail message for any process
export function displaySuccessMessageToast(msg) {
  $("#msgDB").append('<span class="actionSuccess">Success: ' + msg + "</span>");

  M.toast({
    html: msg,
    classes: "orange darken-3 rounded"
  });
}

export function displayFailureMessageToast(msg) {
  $("#msgDB").append('<span class="actionFailure">Error: ' + msg + "</span>");
  M.toast({
    html: `<div style="color:pink;">Error: ${msg}</div>`
  });
}

// std success fail message for db related tasks
export function displaySuccessMessageDB(msg) {
  $("#msgDB").append('<span class="actionSuccess">Success: ' + msg + "</span>");

  M.toast({
    html: msg,
    classes: "orange darken-3 rounded"
  });
}

export function displayFailureMessageDB(msg) {
  $("#msgDB").append('<span class="actionFailure">Error: ' + msg + "</span>");
  M.toast({
    html: `<div style="color:pink;">Error: ${msg}</div>`
  });
}



// END Toast success fail message for any process

// start export to csv files

export function createCsvFile(storeOrTable) {
  returnAllStoreRowsForBackupToCSV(storeOrTable, function (fnCallbackJSON) {
    const fields = ["tbSelfKey2", "tbName"];
    let fileName = storeOrTable + ".csv";
    let myjsondata = fnCallbackJSON;
    let myJsonarray = "[" + myjsondata + "]";
    //let myJsonarray = '{"' + storeOrTable + '":[' + myjsondata + ']}'
    // console.log(myJsonarray);

    myJsonarray = JSON.parse(myJsonarray);

    try {
      var mycsvdata = json2csv(myJsonarray, fields);
      // console.log(myJsonarray);
      displaySuccessMessageToast(
        "Successfully converted local data to CSV for Table"
      );
    } catch (err) {
      // Errors are thrown for bad options, or if the data is empty and no fields are provided.
      // Be sure to provide fields if it is possible that your data array will be empty.
      console.error(err);
      displayFailureMessageToast(
        "Failed to convert local data to CSV for Table, Error: " +
        err
      );
    }
    //  save to download folder
    let blob = new Blob([mycsvdata], { type: "text/plain" });
    saveAs(blob, fileName);
  });
}
function returnAllStoreRowsForBackupToCSV(storeOrTable, fnCallbackJsonAll) {
  let jsonString = ""; // this will by my array of json objects from idb
  let jsonStrings = "";

  openIDBGetStore(storeOrTable, "readonly", function (store) {
    store.openCursor().onsuccess = function (event) {
      let cursor = event.target.result;
      if (cursor) {
        jsonString = JSON.stringify(cursor.value);
        jsonStrings = jsonStrings += jsonString;
        cursor.continue();
        console.log(cursor.value.tbName);
        jsonStrings = jsonStrings += ",";
      } else {
        // remove the trailing comma
        jsonStrings = jsonStrings.replace(/,$/, "");
        // console.log("done looping through object store" + jsonStrings)
        fnCallbackJsonAll(jsonStrings);
      }
    }; // end open curso
  }); // end get store
}

// now create the files for each store
export function exportAllDataAsCSV() {
  createCsvFile(STORE_DOCS);
  createCsvFile(STORE_METADATA);
  createCsvFile(STORE_RESOURCES);
  createCsvFile(STORE_TAGS);

}

//Notes:
// onupgradeneeded is called when you change the db version : from no database to first version, first version to second version ...
// onsuccess is called each time you make a new request : even if the database schemas has not been changed, the database is ready,
// and there’s the “database object” in openRequest.result, that we should use it for further calls.

// App startup - init database
export function openDB() {
  // console.log("got here idb")

  let indexedDB =
    window.indexedDB ||
    window.webkitIndexedDB ||
    window.mozIndexedDB ||
    window.msIndexedDB;

  var openRequest = indexedDB.open(DB_NAME, DB_VERSION);
  openRequest.onsuccess = function (evt) {
    let db = openRequest.result;
    db.onversionchange = function () {
      db.close();
      alert("Database is being rebuild, please reload page twice.");
    };
    // console.log("DONE initializing db");
    // displaySuccessMessageDB("Initialized the Cache First Database. <br>");
  };
  openRequest.onerror = function (evt) {
    console.log(
      "Failure openDb:" + evt.target.errorCode + "err- " + this.error
    );
    displayFailureMessageDB(this.error);
  };

  openRequest.onblocked = function () {
    // there's another open connection to same database
    // and it wasn't closed after db.onversionchange triggered for them
    alert("Database is blocked");
  };

  openRequest.onupgradeneeded = function (evt) {
    displaySuccessMessageDB(
      "onupgradeneeded Creted the Cache First Database. <br> "
    );

    let db = openRequest.result;

    db.onerror = function (event) {
      console.log("Error loading database.");
      displayFailureMessageDB("upgrading problem: " + this.error);
    };

    console.log("fired onUpgradeNeeded, got to upgrade new and re-open db");

    // Create Docs Table
    let store = db.createObjectStore(STORE_DOCS, {
      keyPath: "id",
      autoIncrement: true
    });
    store.createIndex("tbDocID", "tbDocID", { unique: true });
    store.createIndex("tbDocName", "tbDocName", { unique: false });
    store.createIndex("tbDocCustomerID", "tbDocCustomerID", { unique: false });
    store.createIndex("tbDocBriefDescription", "tbDocBriefDescription", {
      unique: false
    });
    store.createIndex("tbDocType", "tbDocType", { unique: false });
    store.createIndex("tbDocNameShort", "tbDocNameShort", { unique: false });
    store.createIndex("tbDocHTMLContent", "tbDocHTMLContent", {
      unique: false
    });
    store.createIndex("tbDocGroup", "tbDocGroup", { unique: false });
    store.createIndex("tbDocPurpose", "tbDocPurpose", { unique: false });
    store.createIndex("tbDocOwner", "tbDocOwner", { unique: false });
    store.createIndex("tbDocPopular", "tbDocPopular", { unique: false });
    store.createIndex("tbDocSortOrder", "tbDocSortOrder", { unique: false });
    store.createIndex("tbDocLastModified", "tbDocLastModified", {
      unique: false
    });
    store.createIndex("tbDocApprovedBy", "tbDocApprovedBy", { unique: false });
    store.createIndex("tbDocStatus", "tbDocStatus", { unique: false });
    store.createIndex("tbDocWrittenBy", "tbDocWrittenBy", { unique: false });
    store.createIndex("tbDocL1", "tbDocL1", { unique: false });
    store.createIndex("tbDocL2", "tbDocL2", { unique: false });
    store.createIndex("tbDocL3", "tbDocL3", { unique: false });
    store.createIndex("tbDocL4", "tbDocL4", { unique: false });
    store.createIndex("tbDocL5", "tbDocL5", { unique: false });
    store.createIndex("tbDocCardImage", "tbDocCardImage", { unique: false });


    // Create Resource Table
    store = db.createObjectStore(STORE_RESOURCES, {
      keyPath: "id",
      autoIncrement: true
    });
    store.createIndex("tbResID", "tbResID", { unique: true });
    store.createIndex("tbResCustomerID", "tbResCustomerID", { unique: false });
    store.createIndex("tbResAzureID", "tbResAzureID", { unique: false });
    store.createIndex("tbResName", "tbResName", { unique: true });
    store.createIndex("tbResNameShort", "tbResNameShort", { unique: false });
    store.createIndex("tbResPrimarySkill", "tbResPrimarySkill", {
      unique: false
    });
    store.createIndex("tbResPrimaryRole", "tbResPrimaryRole", {
      unique: false
    });
    store.createIndex("tbResDepartment", "tbResDepartment", { unique: false });
    store.createIndex("tbResManager", "tbResManager", { unique: false });
    store.createIndex("tbResPayRate", "tbResPayRate", { unique: false });
    store.createIndex("tbResCoordTop", "tbResCoordTop", { unique: false });
    store.createIndex("tbResCoordLeft", "tbResCoordLeft", { unique: false });
    store.createIndex("tbResTeam", "tbResTeam", { unique: false });
    store.createIndex("tbResLocation", "tbResLocation", { unique: false });
    store.createIndex("tbResCostCode", "tbResCostCode", { unique: false });
    store.createIndex("tbResTeamLeader", "tbResTeamLeader", { unique: false });
    store.createIndex("tbResSupervisor", "tbResSupervisor", { unique: false });
    store.createIndex("tbResPartTimeFullTime", "tbResPartTimeFullTime", {
      unique: false
    });
    store.createIndex("tbResResourceType", "tbResResourceType", {
      unique: false
    });
    store.createIndex("tbResResourceClass", "tbResResourceClass", {
      unique: false
    });
    store.createIndex("tbResResourceCalendar", "tbResResourceCalendar", {
      unique: false
    });
    store.createIndex(
      "tbResPercentGeneralAvailability",
      "tbResPercentGeneralAvailability",
      { unique: false }
    );
    store.createIndex("tbResExtSystemResID", "tbResExtSystemResID", {
      unique: false
    });
    store.createIndex("tbResSortOrder", "tbResSortOrder", { unique: false });
    store.createIndex("tbResLastModified", "tbResLastModified", {
      unique: false
    });
    store.createIndex("tbResLabourType", "tbResLabourType", { unique: false });
    store.createIndex("tbResEmail", "tbResEmail", { unique: false });
    store.createIndex("tbResPasswordHash", "tbResPasswordHash", {
      unique: false
    });
    store.createIndex("tbResPIN", "tbResPIN", { unique: false });

    // 23 fields now, done in forms and tested

    // Create Tags Table
    store = db.createObjectStore(STORE_TAGS, {
      keyPath: "id",
      autoIncrement: true
    });
    store.createIndex("tbTagID", "tbTagID", { unique: true });
    store.createIndex("tbTagCustomerID", "tbTagCustomerID", { unique: false });
    store.createIndex("tbTagAzureID", "tbTagAzureID", { unique: false });
    store.createIndex("tbTagGroup", "tbTagGroup", { unique: false });
    store.createIndex("tbTagName", "tbTagName", { unique: false });
    store.createIndex("tbTagNameShort", "tbTagNameShort", { unique: false });
    store.createIndex("tbTagPurpose", "tbTagPurpose", { unique: false });
    store.createIndex("tbTagDescription", "tbTagDescription", {
      unique: false
    });
    store.createIndex("tbTagOwner", "tbTagOwner", { unique: false });
    store.createIndex("tbTagPopular", "tbTagPopular", { unique: false });
    store.createIndex("tbTagSortOrder", "tbTagSortOrder", { unique: false });
    store.createIndex("tbTagLastModified", "tbTagLastModified", {
      unique: false
    });
    store.createIndex("tbTagEntity", "tbTagEntity", { unique: false });

    // these 3 new tags are done and testind in forms

    // Create Metadata Table
    store = db.createObjectStore(STORE_METADATA, {
      keyPath: "id",
      autoIncrement: true
    });
    store.createIndex("tbMDID", "tbMDID", { unique: true }); // for storing the tbkey from Timebars Table to allow one and only one meta data row for each tb row
    store.createIndex("canvasNo", "canvasNo", { unique: false });
    store.createIndex("tbMDRefID", "tbMDRefID", { unique: false });
    store.createIndex("tbMDCustomerID", "tbMDCustomerID", { unique: false });
    store.createIndex("tbMDAzureID", "tbMDAzureID", { unique: false });
    store.createIndex("tbMDName", "tbMDName", { unique: false });
    store.createIndex("tbMDNameShort", "tbMDNameShort", { unique: false });
    store.createIndex("tbMDProjectNumber", "tbMDProjectNumber", {
      unique: false
    });
    store.createIndex("tbMDDescription", "tbMDDescription", { unique: false });
    store.createIndex("tbMDNotes", "tbMDNotes", { unique: false });
    store.createIndex("tbMDExtLink1", "tbMDExtLink1", { unique: false });
    store.createIndex("tbMDExtSystemID1", "tbMDExtSystemID1", {
      unique: false
    });
    store.createIndex("tbMDSortOrder", "tbMDSortOrder", { unique: false });
    store.createIndex("tbMDtbLastModified", "tbMDtbLastModified", {
      unique: false
    });
    store.createIndex("tbMDPriority", "tbMDPriority", { unique: false });
    store.createIndex("tbMDStatus", "tbMDStatus", { unique: false });
    store.createIndex("tbMDState", "tbMDState", { unique: false });
    store.createIndex("tbMDSeverity", "tbMDSeverity", { unique: false });
    store.createIndex("tbMDStage", "tbMDStage", { unique: false });
    store.createIndex("tbMDPhase", "tbMDPhase", { unique: false });
    store.createIndex("tbMDCategory", "tbMDCategory", { unique: false });
    store.createIndex("tbMDHealth", "tbMDHealth", { unique: false });
    store.createIndex("tbMDResponsibility", "tbMDResponsibility", {
      unique: false
    });
    store.createIndex("tbMDDepartment", "tbMDDepartment", { unique: false });
    store.createIndex("tbMDExSponsor", "tbMDExSponsor", { unique: false });
    store.createIndex("tbMDPM", "tbMDPM", { unique: false });
    store.createIndex("tbMDProjectType", "tbMDProjectType", { unique: false });
    store.createIndex("tbMDShowIn", "tbMDShowIn", { unique: false });
    store.createIndex("tbMDYesNoSelector", "tbMDYesNoSelector", {
      unique: false
    });
    store.createIndex("tbMDProduct", "tbMDProduct", { unique: false });
    store.createIndex("tbMDContact", "tbMDContact", { unique: false });
    store.createIndex("tbMDWBS", "tbMDWBS", { unique: false });
    store.createIndex("tbMDWeighting", "tbMDWeighting", { unique: false });
    store.createIndex("tbMDLocation", "tbMDLocation", { unique: false });
    store.createIndex("tbMDPrimarySkill", "tbMDPrimarySkill", {
      unique: false
    });
    store.createIndex("tbMDPrimaryRole", "tbMDPrimaryRole", { unique: false });
    store.createIndex("tbMDOther2", "tbMDOther2", { unique: false });
    store.createIndex("tbMDOther3", "tbMDOther3", { unique: false });
    store.createIndex("tbMDOther4", "tbMDOther4", { unique: false });
    store.createIndex("tbMDGate", "tbMDGate", { unique: false });
    store.createIndex("tbMDHealthOverall", "tbMDHealthOverall", {
      unique: false
    });
    store.createIndex("tbMDHealthScope", "tbMDHealthScope", { unique: false });
    store.createIndex("tbMDHealthCost", "tbMDHealthCost", { unique: false });
    store.createIndex("tbMDHealthIssues", "tbMDHealthIssues", {
      unique: false
    });
    store.createIndex("tbMDHealthRisk", "tbMDHealthRisk", { unique: false });
    store.createIndex("tbMDHealthSchedule", "tbMDHealthSchedule", {
      unique: false
    });
    store.createIndex("tbMDHealthHours", "tbMDHealthHours", { unique: false });
    store.createIndex("tbMDInvestmentCategory", "tbMDInvestmentCategory", {
      unique: false
    });
    store.createIndex("tbMDInvestmentInitiative", "tbMDInvestmentInitiative", {
      unique: false
    });
    store.createIndex("tbMDInvestmentObjective", "tbMDInvestmentObjective", {
      unique: false
    });
    store.createIndex("tbMDInvestmentStrategy", "tbMDInvestmentStrategy", {
      unique: false
    });
    store.createIndex("tbMDROMEstimate", "tbMDROMEstimate", { unique: false });
    store.createIndex("tbMDPortfolio", "tbMDPortfolio", { unique: false });
    store.createIndex("tbMDProgram", "tbMDProgram", { unique: false });
    store.createIndex(
      "tbMDProgActivityAlignment",
      "tbMDProgActivityAlignment",
      { unique: false }
    );
    store.createIndex("tbMDSize", "tbMDSize", { unique: false });
    store.createIndex("tbMDStageApprover", "tbMDStageApprover", {
      unique: false
    });
    store.createIndex("tbMDWrittenBy", "tbMDWrittenBy", { unique: false });
    store.createIndex("tbMDBusinessAdvisor", "tbMDBusinessAdvisor", {
      unique: false
    });
    store.createIndex("tbMDBusinessOwner", "tbMDBusinessOwner", {
      unique: false
    });
    store.createIndex("tbMDDeliveryManager", "tbMDDeliveryManager", {
      unique: false
    });
    store.createIndex("tbMDOrgManager", "tbMDOrgManager", { unique: false });
    store.createIndex("tbMDPriorityStrategic", "tbMDPriorityStrategic", {
      unique: false
    });
    store.createIndex("tbMDSponsoringDepartment", "tbMDSponsoringDepartment", {
      unique: false
    });
    store.createIndex("tbMDPrimaryContact", "tbMDPrimaryContact", {
      unique: false
    });
    store.createIndex("tbMDBenefitCostRatio", "tbMDBenefitCostRatio", {
      unique: false
    });
    store.createIndex("tbMDContactNumber", "tbMDContactNumber", {
      unique: false
    });
    store.createIndex("tbMDResponsibleTeam", "tbMDResponsibleTeam", {
      unique: false
    });
    store.createIndex(
      "tbMDRiskVsSizeAndComplexity",
      "tbMDRiskVsSizeAndComplexity",
      { unique: false }
    );
    store.createIndex("tbMDEcnomicValueAdded", "tbMDEcnomicValueAdded", {
      unique: false
    });
    store.createIndex("tbMDEstimationClass", "tbMDEstimationClass", {
      unique: false
    });
    store.createIndex("tbMDInternalRateOfReturn", "tbMDInternalRateOfReturn", {
      unique: false
    });
    store.createIndex("tbMDSprintName", "tbMDSprintName", { unique: false });
    store.createIndex("tbMDSunkCosts", "tbMDSunkCosts", { unique: false });
    store.createIndex("tbMDSyncNotes", "tbMDSyncNotes", { unique: false });
    store.createIndex("tbMDNotesProject", "tbMDNotesProject", {
      unique: false
    });
    store.createIndex("tbMDContractNumber", "tbMDContractNumber", {
      unique: false
    });
    store.createIndex("tbMDNetPresentValue", "tbMDNetPresentValue", {
      unique: false
    });
    store.createIndex("tbMDOpportunityCost", "tbMDOpportunityCost", {
      unique: false
    });
    store.createIndex("tbMDPaybackPeriod", "tbMDPaybackPeriod", {
      unique: false
    });
    store.createIndex(
      "tbMDPrimaryLineOfBusiness",
      "tbMDPrimaryLineOfBusiness",
      { unique: false }
    );
    store.createIndex("tbMDBackgroundInfo", "tbMDBackgroundInfo", {
      unique: false
    });
    store.createIndex("tbMDCapabilitiesNeeded", "tbMDCapabilitiesNeeded", {
      unique: false
    });
    store.createIndex("tbMDConsequence", "tbMDConsequence", { unique: false });
    store.createIndex("tbMDExpectedBenfits", "tbMDExpectedBenfits", {
      unique: false
    });
    store.createIndex("tbMDProblemOpportunity", "tbMDProblemOpportunity", {
      unique: false
    });
    store.createIndex(
      "tbMDConstraintsAssumptions",
      "tbMDConstraintsAssumptions",
      { unique: false }
    );
    store.createIndex("tbMDCostBenefitAnalysis", "tbMDCostBenefitAnalysis", {
      unique: false
    });
    store.createIndex("tbMDExecutiveSummary", "tbMDExecutiveSummary", {
      unique: false
    });
    store.createIndex(
      "tbMDSeniorLevelCommittment",
      "tbMDSeniorLevelCommittment",
      { unique: false }
    );
    store.createIndex(
      "tbMDStakeholderDescription",
      "tbMDStakeholderDescription",
      { unique: false }
    );
    store.createIndex("tbMDNotesWorkflow", "tbMDNotesWorkflow", {
      unique: false
    });
    store.createIndex("tbMDOther5", "tbMDOther5", { unique: false });
    store.createIndex("tbMDOther6", "tbMDOther6", { unique: false });
    store.createIndex("tbMDOther7", "tbMDOther7", { unique: false });
    store.createIndex("tbMDOther8", "tbMDOther8", { unique: false });
    store.createIndex("tbMDOther9", "tbMDOther9", { unique: false });
    store.createIndex("tbMDOther10", "tbMDOther10", { unique: false });
    store.createIndex("tbMDOther11", "tbMDOther11", { unique: false });
    store.createIndex("tbMDOther12", "tbMDOther12", { unique: false });

    // 100 fields  need to check though

    setTimeout(() => {
      populateMandatoryData();
    }, 500);

  };
}

// End App startup init database

//   Start common functions




// may need later add custom items to bars after load  
function getResName(tbResID, callbackResName) {
  //first get db and open store  
  openIDBGetStore('tbResources', 'readwrite', function (store) {

    var index = store.index("tbResID");
    tbResID = String(tbResID)
    var request = index.get(tbResID);

    request.onerror = function (event) {
      //   Handle errors!
      alert("there was error getting resid")
    };
    request.onsuccess = function (event) {
      var data = request.result;
      //// note.innerHTML += '<li>Success config settings ' + data.ap_name + ' From app core for getValidAPID </li>';
      // need try catch here when db does not exist yet.

      var resName = data.tbResName
      var resNameShort = data.tbResNameShort
      callbackResName(resName, resNameShort);
    };
  });
};



// always use whenever opening store
export function openIDBGetStore(storeName, mode, callback) {
  window.IDBTransaction = window.IDBTransaction ||
    window.webkitIDBTransaction ||
    window.msIDBTransaction || { READ_WRITE: "readwrite" }; // This line should only be needed if it is needed to support the object's constants for older browsers
  window.IDBKeyRange =
    window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
  // (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)

  if (!window.indexedDB) {
    alert("Sorry!Your browser doesn't support IndexedDB");
  }
  var db;
  var request = window.indexedDB.open(DB_NAME, DB_VERSION);
  //Handle error On db open
  request.onerror = function (event) {
    console.log(event.target.errorCode);
  };
  request.onsuccess = function (event) {
    // Handle success On db open, get the db object, then open store
    db = this.result;

    var tx = db.transaction(storeName, mode);
    let store = tx.objectStore(storeName);
    callback(store);
  };
}

// ts always starts on a monday, use this
export function getMonday(date) {
  var day = date.getDay() || 7;
  if (day !== 1) date.setHours(-24 * (day - 1));
  //  alert(date)
  return date;
}

//   end common functions

//********* start bulk data operations     ******************
// now includes metadata as mandatory
export function populateMandatoryData() {
  console.log("called populateMandatoryData");

  let tagdata = getTagsJsonData();
  tagdata = JSON.stringify(tagdata);
  importJsonDataUniversal(tagdata, STORE_TAGS);

  let resdata = getResourcesJsonData();
  resdata = JSON.stringify(resdata);
  importJsonDataUniversal(resdata, STORE_RESOURCES);

  let docdata = getDocumentJsonData();
  docdata = JSON.stringify(docdata);
  importJsonDataUniversal(docdata, STORE_DOCS);

  let mddata = getMetadataJsonData();
  mddata = JSON.stringify(mddata);
  importJsonDataUniversal(mddata, STORE_METADATA);

}

/* we only use tbMetaData table, no longer dependant on timebars data */
export function populateDemoData() {
  console.log("populated seed data, one investment");

  let mddata = getMetadataJsonData();
  mddata = JSON.stringify(mddata);
  importJsonDataUniversal(mddata, STORE_METADATA);

}

export function importJsonDataUniversal(jsonData, storeName) {

  let configData = JSON.parse(jsonData);

  openIDBGetStore(storeName, "readwrite", function (store) {
    var request;
    var i;
    try {
      // add rows
      for (i = 0; i < configData[storeName].length; i++) {
        request = store.put(configData[storeName][i]);
      }
    } catch (e) {
      displayFailureMessageDB(
        "Univ data populator: Some Problem While adding data, if data already exists, this will happen, clear data out then try again"
      );
      throw e;
    }
    request.onsuccess = function (evnt) {
      displaySuccessMessageDB(`App data added: ${storeName}.<br> `);
      console.log(`App data added: ${storeName} `);
    };
    request.onerror = function () {
      displayFailureMessageDB(
        "data must exist already, will not overwite it. to check hit F12 key > Application Tab > IndexedDB " +
        this.error
      );
      console.log("Error Default data must exist already! " + this.error);
    };
  });
}

export function reloadPage() {
  location.reload();
}

// start delete oberations
export function deleteDB() {
  console.log("called delete");
  var request = indexedDB.deleteDatabase(DB_NAME);
  request.onsuccess = function (evt) {
    console.log("delete  db done");
    // location.reload();
    displaySuccessMessageDB(`Deleted IndexedDB: ${DB_NAME}. <br> `);
  };
  request.onerror = function (evt) {
    alert("error deleting db");
  };
}

export function clearProjectDataTbBlMD() {

  var txt;
  var r = confirm("We will clear out data, use this carefully.");
  if (r == true) {
    txt = "You pressed OK, clearing all Investments!";
    console.log(txt);

    clearObjectStore(STORE_METADATA);

    $("#msgDB").append('<span class="actionSuccess">Stores cleared: ' + txt + "</span>");
  } else {
    txt = "You pressed Cancel!";
    console.log(txt);
    $("#msgDB").append('<span class="actionSuccess">Nothing happened: ' + txt + "</span>");
  }

}

export function deletetbMdFromIDB(tbID, tbName) {
  var tbID = String(tbID);
  openIDBGetStore("tbMetaData", "readwrite", function (store) {
    var index = store.index("tbMDID");
    var request = index.get(tbID);

    request.onsuccess = function (evnt) {
      var data = request.result;
      //var del = delete (data.id)
      store.delete(data.id);
      console.log("successfully deleted associated metadata per ID: " + tbID);
      // note.innerHTML += '<li>successfully deleted ' + data.tbName + '</li>';
    };
    request.onerror = function (evnt) {
      console.log("error deleting timebar");
      // note.innerHTML += '<li>error deleting timebar ' + data.tbName + '</li>';
    };
  }); // end open stpre
}

// need to finish universal delete see next fn - used in FOCD forms
function deleteDocFromDB(tbID) {
  var tbID = String(tbID);
  openIDBGetStore("tbDocuments", "readwrite", function (store) {
    var index = store.index("tbDocID");
    var request = index.get(tbID);

    request.onsuccess = function (evnt) {
      var data = request.result;
      store.delete(data.id);
      console.log("successfully deleted timebar: " + tbID);
    };
    request.onerror = function (evnt) {
      console.log("error deleting timebar");
    };
  });
}

export function deleteRowFromIDBUniversal(storeName, indexName, tbID) {
  var tbID = String(tbID);

  console.log("tbID: " + JSON.stringify(tbID));
  openIDBGetStore(storeName, "readwrite", function (store) {
    var index = store.index(indexName);
    var request = index.get(tbID);

    request.onsuccess = function (evnt) {
      var data = request.result;

      //   console.log("data: " + JSON.stringify(data))

      store.delete(data.id);
      console.log("successfully deleted timebar: " + tbID);
    };
    request.onerror = function (evnt) {
      console.log("error deleting timebar");
    };
  });
}

// use this when drag and drop new json data file to clear rows first
export function clearObjectStore(tbStoreName) {
  var tbStoreName = tbStoreName;
  openIDBGetStore(tbStoreName, "readwrite", function (store) {
    var req = store.clear();
    req.onsuccess = function (evnt) {
      console.log("done: " + tbStoreName);
      //displayResCalcs();
    };
    req.onerror = function (evnt) {
      console.log("Did not clear: " + tbStoreName);
    };
  });
}


export function returnOneItemUniversal(tbid, storeName, indexName, callbackTbData) {

  // parentID = "18"
  openIDBGetStore(storeName, "readonly", function (store) {
    var index = store.index(indexName);
    var request = index.get(tbid);
    request.onsuccess = function (event) {
      var data = request.result;
      // var tbID = data.tbID
      // var skey = data.tbSelfKey2
      callbackTbData(data);
      //  console.log("parentID/curid/curselfkey " + parentID + " / " + tbID + " / " + skey)
    };
  }); // end openIDBGetStore
} //  end returnTimebarData

// how to use it
/* //returnOneItemUniversal(wiid, function (callbackTbData) {
    var tbObj = callbackTbData
    tbname = tbObj.tbName
 
    console.log(" 1 tbname: " + tbname)
 
// });
*/

// start export to json files */

export function exportAllData() {

  createJsonFileSimple(STORE_DOCS);
  createJsonFileSimple(STORE_METADATA);
  createJsonFileSimple(STORE_RESOURCES);
  createJsonFileSimple(STORE_TAGS);

}
export function createJsonFileSimple(storeOrTable) {
  returnAllStoreRowsForBackup(storeOrTable, function (fnCallbackJSON) {
    var fileName = storeOrTable + ".js";
    // assemble the json objects into an array like so [{},{}]
    var myJsonFile = fnCallbackJSON; //.trimRight("\W") // myJsonRow

    myJsonFile = '{"' + storeOrTable + '":[' + myJsonFile + "]}";

    //  save to download folder
    var blob = new Blob([myJsonFile], { type: "text/plain" });
    saveAs(blob, fileName);
  });
}
function returnAllStoreRowsForBackup(storeOrTable, fnCallbackJsonAll) {
  var storeOrTable = storeOrTable;
  // alert(storeOrTable)

  var jsonString = ""; // this will by my array of json objects from idb
  var jsonStrings = "";

  openIDBGetStore(storeOrTable, "readonly", function (store) {
    store.openCursor().onsuccess = function (event) {
      var cursor = event.target.result;
      if (cursor) {
        jsonString = JSON.stringify(cursor.value);
        jsonStrings = jsonStrings += jsonString;
        cursor.continue();
        // alert(cursor.value.tbID)
        jsonStrings = jsonStrings += ",";
      } else {
        // remove the trailing comma
        jsonStrings = jsonStrings.replace(/,$/, "");
        // alert("done looping through object store" + jsonStrings)
        fnCallbackJsonAll(jsonStrings);
      }
    }; // end open curso
  }); // end get store
}

// END export to json files

// this works but did not work well integrated into getadminpaneldata - get Admin Panel key, it hosts config setting,  get the apID of "Standard config settings" row with get by index name.
export function getCustomerID(callbackCustomerID) {
  //first get db and open store
  openIDBGetStore(STORE_ADMINPANEL, "readwrite", function (store) {
    var index = store.index("ap_name");
    var request = index.get("Standard Config Settings");

    request.onerror = function (event) {
      //   Handle errors!
      alert("there was error getting std config settings from ");
    };
    request.onsuccess = function (event) {
      var data = request.result;
      //// note.innerHTML += '<li>Success config settings ' + data.ap_name + ' From app core for getValidAPID </li>';
      // need try catch here when db does not exist yet.

      var apCustomerID = data.apCustomerID;

      callbackCustomerID(apCustomerID);
    };
  });
}

////////////////// Universal  CRUD Operatons   /////////


// this is called everytime to add an associated md record this way I always have my associated metadata using
// reference between Timebars table and metadata table is to store tbId in metadata table field tbMDID
function createAssocMetaDataRecord(tbID, tbCustomerID, canvasNo) {
  var canvasNo = String(canvasNo);
  var tbID = String(tbID);
  var tbMDID = tbID; // store this in the tbMetaData store in field tbMDID to create relationship between tb and md tables
  var tbMDNameValue = "auto-created for tbID cross ref: " + tbMDID;

  //var tbMDObject = { tbMDID: tbMDID, tbMDName: tbMDNameValue, tbMDCustomerID: tbCustomerID };
  var tbMDObject = {
    tbMDGate: 0,
    tbMDHealthOverall: 0,
    tbMDHealthScope: 0,
    tbMDHealthCost: 0,
    tbMDHealthIssues: 0,
    tbMDHealthRisk: 0,
    tbMDHealthSchedule: 0,
    tbMDHealthHours: 0,
    tbMDInvestmentCategory: 0,
    tbMDInvestmentInitiative: 0,
    tbMDInvestmentObjective: 0,
    tbMDInvestmentStrategy: 0,
    tbMDROMEstimate: 0,
    tbMDPortfolio: 0,
    tbMDProgram: 0,
    tbMDProgActivityAlignment: 0,
    tbMDSize: 0,
    tbMDStageApprover: 0,
    tbMDWrittenBy: 0,
    tbMDBusinessAdvisor: 0,
    tbMDBusinessOwner: 0,
    tbMDDeliveryManager: 0,
    tbMDOrgManager: 0,
    tbMDPriorityStrategic: 0,
    tbMDSponsoringDepartment: 0,
    tbMDPrimaryContact: 0,
    tbMDBenefitCostRatio: 0,
    tbMDContactNumber: 0,
    tbMDResponsibleTeam: 0,
    tbMDRiskVsSizeAndComplexity: 0,
    tbMDEcnomicValueAdded: 0,
    tbMDEstimationClass: 0,
    tbMDInternalRateOfReturn: 0,
    tbMDSprintName: 0,
    tbMDSunkCosts: 0,
    tbMDSyncNotes: 0,
    tbMDNotesProject: 0,
    tbMDContractNumber: 0,
    tbMDNetPresentValue: 0,
    tbMDOpportunityCost: 0,
    tbMDPaybackPeriod: 0,
    tbMDPrimaryLineOfBusiness: 0,
    tbMDBackgroundInfo: 0,
    tbMDCapabilitiesNeeded: 0,
    tbMDConsequence: 0,
    tbMDExpectedBenfits: 0,
    tbMDProblemOpportunity: 0,
    tbMDConstraintsAssumptions: 0,
    tbMDCostBenefitAnalysis: 0,
    tbMDExecutiveSummary: 0,
    tbMDSeniorLevelCommittment: 0,
    tbMDStakeholderDescription: 0,
    tbMDNotesWorkflow: 0,
    tbMDOther5: 0,
    tbMDOther6: 0,
    tbMDOther7: 0,
    tbMDOther8: 0,
    tbMDOther9: 0,
    tbMDOther10: 0,
    tbMDOther11: 0,
    tbMDOther12: 0,
    tbMDID: tbMDID,
    tbMDName: tbMDNameValue,
    tbMDCustomerID: tbCustomerID,
    tbMDAzureID: 0,
    tbMDNameShort: 0,
    tbMDProjectNumber: 0,
    tbMDDescription: 0,
    tbMDNotes: 0,
    tbMDExtLink1: 0,
    tbMDExtSystemID1: 0,
    tbMDSortOrder: 0,
    tbMDtbLastModified: 0,
    tbMDPriority: 0,
    tbMDStatus: 0,
    tbMDState: 0,
    tbMDSeverity: 0,
    tbMDStage: 0,
    tbMDPhase: 0,
    tbMDCategory: 0,
    tbMDHealth: 0,
    tbMDResponsibility: 0,
    tbMDDepartment: 0,
    tbMDExSponsor: 0,
    tbMDPM: 0,
    tbMDProjectType: 0,
    tbMDShowIn: 0,
    tbMDYesNoSelector: 0,
    tbMDProduct: 0,
    tbMDContact: 0,
    tbMDWBS: 0,
    tbMDWeighting: 0,
    tbMDLocation: 0,
    tbMDPrimarySkill: 0,
    tbMDPrimaryRole: 0,
    tbMDOther2: 0,
    tbMDOther3: 0,
    tbMDOther4: 0,
    canvasNo: 1
  };

  //var store = db.transaction(["tbMetaData"], "readwrite").objectStore("tbMetaData");
  openIDBGetStore("tbMetaData", "readwrite", function (store) {
    var request = store.add(tbMDObject);
    request.onsuccess = function () {
      // // note.innerHTML += '<li>success, added metadata record: ' + tbMDID + 'from CRUD.js</li>';
      // note.innerHTML += '<li>successfully added ' + tbName + '</li>';
      var tbMetaDataTableID = request.result;
      tbMetaDataTableID = String(tbMetaDataTableID);
    };
    request.onerror = function () {
      // note.innerHTML += '<li>failed to add metadata record: ' + tbMDID + 'from CRUD.js</li>';
    };
  }); // end openIDBGetStore
}



// update one field, any tale
export function updateOneFieldUniversal(
  storeName,
  indexName,
  tbid,
  fieldName,
  newVal
) {
  console.log("updateOneFieldUniversal - tbid " + tbid);
  openIDBGetStore(storeName, "readwrite", function (store) {
    // var request = store.get(key);
    var index = store.index(indexName);
    var request = index.get(tbid);
    // var request = store.get(tbid);
    request.onerror = function (event) {
      alert("No matching record found, or other error");
    };
    request.onsuccess = function (event) {
      var data = request.result;

      data[fieldName] = newVal;

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function (event) {
        alert("there was error updating data");
      };
      requestUpdate.onsuccess = function (event) {
        console.log(
          "succes updating fieldNamed: " +
          fieldName +
          " on tbDocID: " +
          data.tbDocID
        );
      };
    };
  });
}

// dont need index name because we get the item by idb built in key
export function setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal) {
  console.log("setTbidBasedOnKeyUniversal - uid " + uid);

  openIDBGetStore(storeName, "readwrite", function (store) {
    var request = store.get(Number(uid));
    request.onerror = function (event) {
      alert("No matching record found, or other error");
    };
    request.onsuccess = function (event) {
      var data = request.result;

      data[fieldName] = newVal;

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function (event) {
        alert("there was error updating data");
      };
      requestUpdate.onsuccess = function (event) {
        console.log(
          "succes updating fieldNamed: " +
          fieldName +
          " on tbDocID: " +
          data.tbDocID
        );
      };
    };
  });
}



export function updateOneMetadataField(tbMDID, fieldName, theText) {
  var tbMDID = String(tbMDID);
  openIDBGetStore("tbMetaData", "readwrite", function (store) {
    console.log("updateOneMetadataField tbMDID " + tbMDID);

    // var request = store.get(key);
    var index = store.index("tbMDID");
    var request = index.get(tbMDID);

    request.onerror = function (event) {
      alert("No matching record found, or other error");
    };
    request.onsuccess = function (event) {
      var data = request.result;

      // data['tbMDDescription'] = "newValue1;"
      data[fieldName] = theText;
      var requestUpdate = store.put(data);

      requestUpdate.onerror = function (event) {
        alert("there was error updating data");
      };
      requestUpdate.onsuccess = function (event) {
        console.log("successfully Updated " + data.tbMDID);
      };
    };
  });
}

function updateOneDocumentField(tbDocID, fieldName1, newValue1) {
  //alert("key " + key)
  console.log("tbDocID " + tbDocID);
  var tbDocID = String(tbDocID);
  var newValue1 = String(newValue1);

  openIDBGetStore("tbDocuments", "readwrite", function (store) {
    // var request = store.get(key);
    var index = store.index("tbDocID");
    var request = index.get(tbDocID);

    request.onerror = function (event) {
      alert("No matching record found, or other error");
    };
    request.onsuccess = function (event) {

      var data = request.result;

      data[fieldName1] = newValue1;

      //   console.log("tbDocID  " + data.tbDocID)

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function (event) {
        alert("there was error updating data");
      };
      requestUpdate.onsuccess = function (event) {
        //  // note.innerHTML += '<li>successfully Updated </li>';
        console.log(
          "succes updating fieldNamed: " +
          fieldName1 +
          " on tbDocID: " +
          data.tbDocID
        );
      };
    };
  });
}



// Copyright © 2018 Timebars Ltd.  All rights reserved.
