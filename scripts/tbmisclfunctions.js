import { reloadPage, getAdminPanelData, openIDBGetStore } from './tbdatabase';


// ******  materialize miscl functions for sidenav and modal etc   ******

document.addEventListener('DOMContentLoaded', function () {
    var elem = document.getElementById("slideOutLeft");
    var instance = M.Sidenav.init(elem, {
        inDuration: 500,
        outDuration: 350,
        edge: 'left' //or right
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var elem = document.getElementById("slideOutRight");
    var instance = M.Sidenav.init(elem, {
        inDuration: 650,
        outDuration: 350,
        edge: 'right' //or right
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var elem = document.getElementById("slideOutAdminPanel");
    var instance = M.Sidenav.init(elem, {
        inDuration: 350,
        outDuration: 350,
        edge: 'left' //or right
    });
});



document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {
        container: 'body',
        format: 'dd-mmm-yy'
    });
});


// init modal
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
});


// prevent move up or down - once the sole parent is drawn, want to prevent move up or down just drag left to right
function preventDragUpOrDown() {
    newDraggableID = "#key" + tbID
    $(newDraggableID).draggable({
        axis: "x"
    });

}

