InlineEditor.create(document.querySelector('.editor'), {

    toolbar: {
        items: [
            'heading',
            '|',
            'bold',
            'italic',
            'fontSize',
            'fontColor',
            'link',
            'bulletedList',
            'numberedList',
            '|',
            '|',
            'undo',
            'redo',
            'insertTable',
            'alignment'
        ]
    },
    language: 'en',
    table: {
        contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells',
            'tableCellProperties'
        ]
    },
    licenseKey: '',

})
    .then(editor => {
        window.editor = editor;
        //   let latestText = window.editorStakeholderDescription.getData()
        //    console.error('latestText! ' + latestText);
        //   console.error('edataConsequence ! ' + edataConsequence);

    })
    .catch(error => {
        console.error('Oops, something gone wrong!');
        console.error('Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:');
        console.warn('Build id: 1ceqoqaw1geb-3zp2kw9ycazv');
        console.error(error);
    });