// my main (only) js file linked to the html page
import $ from "jquery";

import XLSX from "xlsx";

import './app-assets/js/plugins';
import './app-assets/js/vendors.min';

/* import CSS files */
import "materialize-css/dist/css/materialize.css";
import "materialize-css/dist/js/materialize.js";
import "./css/app.css";
import "./css/materalizeoverride.css";

import {
  displaySuccessMessageToast,
  displayFailureMessageToast,
  clearObjectStore,
  reloadPage,
  clearProjectDataTbBlMD,
  exportAllData,
  exportAllDataAsCSV,
  populateMandatoryData,
  openDB,
  openIDBGetStore,
  deleteDB,
  populateDemoData,
  importJsonDataUniversal
} from "./scripts/tbdatabase";

import { populateMetadataFormMD, initEditorMdAdvUniversal } from "./scripts/FOCDMetadataForm"

// ay delete later
import moment from "moment";



import { } from "./scripts/tbmisclfunctions"; // houses sidenav, paralax etc js code for M
import { } from './scripts/focdresources';

// Data Management forms scripts
import { } from "./scripts/focdcommon";

import { getTimebars, getMetadatas, initDtMetadatas } from "./scripts/focdmetadata";
import { } from "./scripts/focdtags";
import { } from "./scripts/focdmetadata";
import { } from "./scripts/focddocuments";
import { } from "./scripts/focdconfigdata";
import { } from "./scripts/tbreportsgraphs";
import { } from "./scripts/FOCDMetadataForm"; // popup on bars metadata form
import { } from "./scripts/tbdatabackuprestore";




/* Initialize the databse */
openDB();

/* Initialize the DM Lists */
getMetadatas()
setTimeout(() => {
  console.log("md page loaded")
  initDtMetadatas()

  // show main inv planner intake form
  document.getElementById("mdFormAdvL1").style.display = "block";
  var tbid = "101"
  populateMetadataFormMD(tbid)
  // set the form data-tbid (this is tMDID pushed when it opens)
  var myform = document.getElementById('mdFormAdvL1')
  var myform = myform.setAttribute('data-tbid', tbid)

}, 150);




// main menu bar init
document
  .getElementById("btnPopulateDemoData")
  .addEventListener("click", populateDemoData);

document
  .getElementById("btnclearObjectStores")
  .addEventListener("click", clearProjectDataTbBlMD);


$(document).on('click', '.btnExportAll', function () {
  exportAllDataAsCSV()

});

/* Launch DM Lists */
$(document).on('click', '.btnViewDMLists', function (e) {
  document.getElementById("launchTimebarsForm").style.display = "none";
  document.getElementById("divDataManagementArea").style.display = "block";
  document.getElementById("ipSupportPage").style.display = "none";
  document.getElementById("mdFormAdvL1").style.display = "none";
});

/* home and default page when app opens */
$(document).on('click', '.launchTbHomePage', function (e) {
  document.getElementById("launchTimebarsForm").style.display = "none";
  document.getElementById("divDataManagementArea").style.display = "none";
  document.getElementById("ipSupportPage").style.display = "none";
  document.getElementById("mdFormAdvL1").style.display = "block"; /* mod */
});


$(document).on('click', '.launchIpSupportPage', function (e) {
  document.getElementById("launchTimebarsForm").style.display = "none";
  document.getElementById("divDataManagementArea").style.display = "none";
  document.getElementById("ipSupportPage").style.display = "block";
  document.getElementById("mdFormAdvL1").style.display = "none";
});


